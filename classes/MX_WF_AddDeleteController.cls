/**
* ------------------------------------------------------------------------------
* @Nombre: MX_WF_AddDeleteController
* @Autor: Sandra Ventura García
* @Proyecto: Workflow Campañas
* @Descripción : Clase controller para componentes MX_WF_TablaDinamicaPadre
* ------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ------------------------------------------------------------------------------
* 1.0           10/11/2019     Sandra Ventura García	       Creación
* ------------------------------------------------------------------------------
*/
public with sharing class MX_WF_AddDeleteController {
  /**
  * @description: construnctor sin argumentos
  * @author Sandra Ventura
  */
    @TestVisible
    private MX_WF_AddDeleteController() {}
  /**
  * @description: Obtiene lista de temas
  * @author Sandra Ventura
  */
@AuraEnabled
	public static List<MX_WF_Tema__c> getTemas(Id precordId) {
     try {
          final Id IdTaskforce = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: precordId].MX_WF_Taskforce__c;
         return [Select Id, Name, MX_WF_Tiempo_Minutos__c, MX_WF_Hora_inicio_Tema__c, MX_WF_Presentador__c, MX_WF_Soporte__c FROM MX_WF_Tema__c WHERE MX_WF_Taskforce__c=: IdTaskforce ORDER BY MX_WF_Hora_inicio_Tema__c ASC];

     } catch(Exception e) {
			throw new AuraHandledException(System.Label.MX_WF_ErrorTaskforce+ e);
         }
    }
  /**
  * @description: Actualiza e inserta lista de temas
  * @author Sandra Ventura
  */   
   @AuraEnabled
    public static boolean saveTema(List<MX_WF_Tema__c> listTema, Id precordId) {
        try {
           final Id idTaskforce = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: precordId].MX_WF_Taskforce__c;
           final Integer num = listTema.size();
           Time vtiempo = listTema[0].MX_WF_Hora_inicio_Tema__c;
           integer vminuts = 0; 
           Time newTime = listTema[0].MX_WF_Hora_inicio_Tema__c;
           final String[] idtemas = New String[] {};
            
           final List<MX_WF_Tema__c> ltemas = new List<MX_WF_Tema__c>();
            for(Integer i=0;i<num;i++) {
           
            final MX_WF_Tema__c temas = new MX_WF_Tema__c(Name= listTema[i].Name,
                                                          Id = listTema[i].Id,
                                                          MX_WF_Hora_inicio_Tema__c= vtiempo,
                                                          MX_WF_Tiempo_Minutos__c= listTema[i].MX_WF_Tiempo_Minutos__c,
                                                          MX_WF_Presentador__c= listTema[i].MX_WF_Presentador__c,
                                                          MX_WF_Soporte__c= listTema[i].MX_WF_Soporte__c,
                                                          MX_WF_Taskforce__c=idTaskforce);
                
                vminuts = integer.valueof(listTema[i].MX_WF_Tiempo_Minutos__c); 
                newTime = vtiempo.addMinutes(vminuts);
                vtiempo = newTime;
                system.debug(vtiempo);
             idtemas.add(temas.Id);   
                
             ltemas.add(temas);
        }
         final List<MX_WF_Tema__c> listtemas =[SELECT Id FROM MX_WF_Tema__c WHERE MX_WF_Taskforce__c=: idTaskforce AND Id not in : idtemas]; 
        delete listtemas;
        upsert ltemas id;
        return true;
        } catch (Exception e) {
            throw new AuraHandledException(System.Label.MX_WF_ErrorTaskforce+ e);
        }
    }
  /**
  * @description: Obtiene lista de invitados
  * @author Sandra Ventura
  */
@AuraEnabled
	public static List<MX_WF_Invitados_Taskforce__c> getInvitados(Id precordId) {
     try {
          final Id IdTaskforce = [SELECT MX_WF_Taskforce__c FROM MX_WF_Minuta_Taskforce__c WHERE Id=: precordId].MX_WF_Taskforce__c;
         return [Select Id, MX_WF_Asistio__c, MX_WF_Invitado__c, MX_WF_Correo_electr_nico__c, MX_WF_Nombre_Completo_txt__c FROM MX_WF_Invitados_Taskforce__c WHERE Taskforce__c=: IdTaskforce ORDER BY MX_WF_Invitado__c ASC];

	     } catch(Exception e) {
			throw new AuraHandledException(System.Label.MX_WF_ErrorTaskforce+ e);
         }
    }
  /**
  * @description: Actualiza la asistencia de los invitados
  * @author Sandra Ventura
  */
@AuraEnabled
    public static boolean saveInvitado(List<MX_WF_Invitados_Taskforce__c> listInvitado) {
          try {
            final Integer num = listInvitado.size();
            final List<MX_WF_Invitados_Taskforce__c> invit = new List<MX_WF_Invitados_Taskforce__c>();
            for(Integer i=0;i<num;i++) {
             final MX_WF_Invitados_Taskforce__c asistente = new MX_WF_Invitados_Taskforce__c(Id= listInvitado[i].Id,
                                                                                             MX_WF_Asistio__c=ListInvitado[i].MX_WF_Asistio__c);
           invit.add(asistente);
        }
        update invit;
        return true;
        } catch (Exception e) {
            throw new AuraHandledException(System.Label.MX_WF_ErrorTaskforce+ e);
        }
    }
}