/**-------------------------------------------------------------------------
* Nombre: MX_WB_TM_Cotizador_cls
* Autor Ing. Daniel Goncalves
* Proyecto: MW WB TM - BBVA Bancomer
* Descripción : Clase Controladora del componente de Cotizador de Telemarketing

* --------------------------------------------------------------------------
* Versión       Fecha           Autor                   Desripción
* --------------------------------------------------------------------------
* 1.0           14/01/2019      Ing. Daniel Goncalves   Creación
* 1.1           15/01/2019      Ing. Daniel Goncalves   Refactorización
* 1.2           25/01/2019      Ing. Daniel Goncalves   Cambio a OpportunityLineItem
* 1.3           28/01/2019      Ing. Daniel Goncalves   Se agrega elección del Producto
* 1.4           31/01/2019      Ing. Daniel Goncalves   Borrado de OpportunityLineItem sin folio
* 1.5           11/02/2019      Ing. Daniel Goncalves   Refactorización Sonar
* 1.6           20/02/2019      Ing. Daniel Goncalves   Cambio de Origen__c a LeadSource
* --------------------------------------------------------------------------
**/
public with sharing class MX_WB_TM_Cotizador_cls {

    /*Construtor para Singleton*/
    private MX_WB_TM_Cotizador_cls() {} //NOSONAR
    /** Metadata Personalizada para URL Seguro Moto Bancomer **/
    static final String STR_ML_SMB = 'URLMSB';
    /** Metadata Personalizada para URL Seguro Fronterizo **/
    static final String STR_ML_SF = 'URLSFN';
    /** Metadata Personalizada para URL Auto Seguro Bancomer **/
    static final String STR_ML_ASD = 'URLASB';
    /** String para Datos de Usuario **/
    static String strDatosUsuario;
    /** String para Fecha **/
    static String strFechaMilisApex;
    /** String para Firma **/
    static String strFirmaMain;
    /** Inicialización **/
    static {
        strDatosUsuario = '';
        strFechaMilisApex = '';
        strFirmaMain = '';
    }

/**
* Retorna la URL necesaria para mostrar el cotizador
* @param Id idOpporWanted ID Oportunidad de la que se requiere cotizar
* @return String con la URL
**/
    @AuraEnabled
    public static String crearURL ( String quoteId ) {
        String strMasterLabel;
        String finalURL;
        try {
            final Quote quoteData = [Select Id,Email, Phone,BillingPostalCode,QuoteToName, Opportunity.Origen__c, Opportunity.LeadSource, Status, BillingAddress, ShippingPostalCode, MX_SB_VTS_Folio_Cotizacion__c, MX_SB_VTS_Familia_Productos__c, MX_SB_VTS_Familia_Productos__r.Name, QuoteToAddress, AccountId, OpportunityId from Quote where Id =:quoteId];
            final Account accountData = [ SELECT Id, Name,BillingPostalCode, LastName, PersonMailingPostalCode,ShippingPostalCode, PersonMobilePhone, PersonEmail, Genero__c, PersonBirthdate FROM Account WHERE ID =: quoteData.AccountId];
            if (quoteData.MX_SB_VTS_Familia_Productos__r.Name == System.Label.MX_SB_VTS_FamiliaASD) {
                final QuoteLineItem quoliData = [Select Product2Id, MX_WB_Folio_Cotizacion__c, MX_WB_nombreAsegurado__c, MX_WB_emailAsegurado__c from QuoteLineItem where QuoteId =: quoteData.Id];
                strMasterLabel = MX_WB_TM_Cotizador_cls.getMasterLabelValidation(quoliData.Product2Id);
                if ( String.isNotBlank( strMasterLabel ) ) {
                    finalURL = MX_WB_TM_Cotizador_cls.crearURLSeguro (accountData, quoteData, createUrlPrefix(), strMasterLabel );
                }
            } else {
                finalURL = '';
            }
        } catch ( QueryException quEx ) {
            throw new AuraHandledException(System.Label.MX_WB_ERROR+quEx);
        }
        return finalURL;
    }

/**
* Retorna la URL necesaria para mostrar el cotizador, basado en datos de cotización y el producto
* @param OpportunityLineItem objOpLi Objeto de OpportunityLineItem con información necesaria para URL
* @param string strUrlBegin Contiene cadena de inicio de la URL
* @param string strMasterLabel Etiqueta del producto deseado
* @return String con la URL
**/
    private static String crearURLSeguro( Account accountData, Quote quoteData, String strUrlBegin, String strMasterLabel ) {
        String urlSeg;
        String postalCode = '';
        postalCode = String.isNotBlank(String.valueOf(quoteData.BillingPostalCode)) ? String.valueOf(quoteData.BillingPostalCode) : postalCode;
        postalCode = String.isNotBlank(String.valueOf(accountData.BillingPostalCode)) ?  String.valueOf(accountData.BillingPostalCode) : postalCode;
        final boolean blFolio = String.isBlank(quoteData.MX_SB_VTS_Folio_Cotizacion__c) ? false : true;
        final URL_Auto_Seguro_Bancomer__mdt objMSB = [ SELECT DeveloperName, URL__c, tipoUsoVehiculoCot__c FROM URL_Auto_Seguro_Bancomer__mdt WHERE MasterLabel =: strMasterLabel];
        if ( blFolio ) {
            urlSeg = objMSB.URL__c + strUrlBegin + '&folioSalto='+ quoteData.MX_SB_VTS_Folio_Cotizacion__c;
        } else {
            final String strTelfAseg = String.isNotBlank(quoteData.Phone) ? quoteData.Phone :'';
            String strOrigen = '';
            strOrigen = String.isNotBlank(quoteData.Opportunity.LeadSource) ? quoteData.Opportunity.LeadSource : strOrigen;
            strOrigen = String.isNotBlank(quoteData.Opportunity.Origen__c) ? quoteData.Opportunity.Origen__c : strOrigen;
            urlSeg = objMSB.URL__c + strUrlBegin + '&nombreCot=' + quoteData.QuoteToName +
                '&emailCot=' + quoteData.Email + '&telefonoCot=' + strTelfAseg + '&sexoCot=' + accountData.Genero__c +
                '&codigoPostalCot=' + postalCode + '&fechaDeNacimientoCot=' + accountData.PersonBirthdate + '&idClienteCot=' + accountData.Id +
                '&idOportunidadComercialCot=' + quoteData.OpportunityId + '&origenOportunidadCot=' + strOrigen + '&tipoUsoVehiculoCot=' + objMSB.tipoUsoVehiculoCot__c;
        }
        return urlSeg;
    }

/**
* Retorna la lista de productos disponibles para cotizar
* @param ID idOppor Oportunidad a traer productos
* @return List<String> con los productos disponibles
**/
    @AuraEnabled
    public static Map<String, Object> fetchProductsLst ( ID idOppor ) {
        final Map<String, Object> response = new Map<String, Object>();
        try {
            final List<Product2> lstAllProd = [Select Id, IsActive, Name, MX_WB_FamiliaProductos__c from Product2 where IsActive = true];
            final List<Product2> lstAllProdIn = [Select Id, IsActive, Name, MX_WB_FamiliaProductos__c from Product2 where IsActive = true AND MX_WB_FamiliaProductos__r.Name =: System.Label.MX_SB_VTS_FamiliaASD];
            List<Product2> lstAllProdOut = new List<Product2>();
            final Opportunity oppData = [ SELECT CampaignId, Campaign.MX_SB_VTS_Familia_Producto__c, Campaign.MX_SB_VTS_FamiliaProducto_Proveedor__r.MX_SB_VTS_Familia_de_productos__c, MX_SB_VTS_OppCase__c, MX_SB_VTS_OppCase__r.ProductId, MX_SB_VTS_OppCase__r.Product.MX_WB_FamiliaProductos__c, Origen__c FROM Opportunity WHERE Id =: idOppor ];
            if ( String.isBlank( oppData.MX_SB_VTS_OppCase__c ) || String.isNotBlank( oppData.CampaignId ) && String.isNotBlank( oppData.Campaign.MX_WB_FamiliaProductos__c)) {
                lstAllProdOut = [Select Id, IsActive, Name, MX_WB_FamiliaProductos__c from Product2 where IsActive = true AND MX_WB_FamiliaProductos__c =: oppData.Campaign.MX_SB_VTS_Familia_Producto__c];
            } else if(String.isNotBlank( oppData.MX_SB_VTS_OppCase__c ) && String.isNotBlank( oppData.MX_SB_VTS_OppCase__r.ProductId) && oppData.Origen__c == System.Label.MX_SB_VTS_SAC_LBL) {
                lstAllProdOut = [Select Id, IsActive, Name, MX_WB_FamiliaProductos__c from Product2 where IsActive = true AND MX_WB_FamiliaProductos__c =: oppData.MX_SB_VTS_OppCase__r.Product.MX_WB_FamiliaProductos__c];
            }
            response.put('lstProdOut', lstAllProdOut);
            response.put('lstProdIn', lstAllProdIn);
            response.put('lstAllProd', lstAllProd);
        } catch ( QueryException quEx ) {
            throw new AuraHandledException(System.Label.MX_WB_ERROR+quEx);
        }
        return response;
    }

/**
* Asigna producto a la oportunidad
* @param ID idOpporWanted Oportunidad a asignar producto
* @param String strProdName Nombre Producto a agregar
* @return boolean si pudo hacer el cambio o no
**/
    @AuraEnabled
    public static boolean addProduct ( ID idOpporWanted, String strProdName ) {
        try {
            final Product2 prodVal = [SELECT Id,MX_WB_FamiliaProductos__c,Name FROM Product2 WHERE Id =: strProdName];
            if ( String.isNotBlank( prodVal.Id ) && String.isNotBlank( prodVal.MX_WB_FamiliaProductos__c )) {
                final Quote presupuesto = new Quote();
                final QuoteLineItem quoli = new QuoteLineItem();
                Boolean hasPriceBook = true;
                presupuesto.OpportunityId = idOpporWanted;
                presupuesto.MX_SB_VTS_Familia_Productos__c = prodVal.MX_WB_FamiliaProductos__c;
                presupuesto.Status = 'Creada';
                for ( PricebookEntry pbe : [select Id,Pricebook2Id FROM PricebookEntry where Product2Id =: strProdName ] ) {
                    quoli.PricebookEntryId = pbe.Id;
                    presupuesto.Pricebook2Id = pbe.Pricebook2Id;
                    hasPriceBook = false;
                }
                if (hasPriceBook) {
                    final PricebookEntry pbe = MX_WB_OppoLineItemUtil_cls.crearPricebookEntry(strProdName, 1.00);
                    insert pbe;
                    presupuesto.Pricebook2Id = pbe.Pricebook2Id;
                    quoli.PriceBookEntryId = pbe.Id;
                }
                final Opportunity oppData = [Select Id, Account.PersonEmail, Account.FirstName,Account.LastName, Account.ApellidoMaterno__c, Account.BillingPostalCode,Account.PersonMobilePhone from Opportunity where Id =: idOpporWanted];
                presupuesto.Name = prodVal.Name;
                presupuesto.MX_SB_VTS_Nombre_Contrante__c = oppData.Account.FirstName;
                presupuesto.Email = oppData.Account.PersonEmail;
                presupuesto.Phone = oppData.Account.PersonMobilePhone;
                presupuesto.QuoteToName = oppData.Account.FirstName+ ' '+ oppData.Account.LastName + (String.isNotBlank(oppData.Account.ApellidoMaterno__c) ? (' '+oppData.Account.ApellidoMaterno__c) :'');
                presupuesto.BillingPostalCode = oppData.Account.BillingPostalCode;
                insert presupuesto;
                
                quoli.QuoteId = presupuesto.Id;
                quoli.UnitPrice = 1.00;
                quoli.Quantity = 1;
                
                insert quoli;
                oppData.MX_WB_Producto__c=prodVal.Id;
                oppData.Producto__c=prodVal.Name;
                update oppData;
                
            }
        } catch ( QueryException quEx ) {
            throw new AuraHandledException ( System.Label.MX_WB_LG_ErrorBack + ' - Actualizando Cuenta ' + quEx);
        }
        return true;
    }

    /**
     * addProductInbound Agregar productos Inbound
     * @param  idOpporWanted Oportunidad buscada
     * @param  strProdName   Nombre del producto a buscar
     * @return               Boolean si se encuentran resultados
     */
    @AuraEnabled
    public static boolean addProductInbound ( Id idOpporWanted, String strProdName ) {
        Boolean responseBool = false;
        try {
            String correctName = correctName(strProdName);
            final Product2 prodVal = [SELECT Id,MX_WB_FamiliaProductos__c,Name FROM Product2 WHERE Name =: correctName];
            responseBool = addProduct(idOpporWanted, prodVal.Id);
        } catch(QueryException quex) {
            throw new AuraHandledException(Label.MX_WB_LG_ErrorBack + ' ' + quex);//NOSONAR
        }
        return responseBool;
    }
    
/**
* Elabora el prefijo del URL con datos del Usuario y Firma Encriptada en MD5
* @return String prefijo de URL generado
**/
    public static String createUrlPrefix () {
        try {
            String strUsuario, strCanal, strSubCanal, strAgencia, strServicio, strUrlBegin;
            final User objUser = [ SELECT Id, Usuario__c, Canal__c, Sub_Canal__c, Agencia__c, Servicio__c FROM User WHERE Id =: UserInfo.getUserId() ];
            strFechaMilisApex = Utilities.fnGeneradorFechaMilis();
            strUsuario = String.isNotBlank( objUser.Usuario__c ) ? objUser.Usuario__c : '';
            strCanal = String.isNotBlank( objUser.Canal__c ) ? objUser.Canal__c : '';
            strSubCanal = String.isNotBlank( objUser.Sub_Canal__c ) ? objUser.Sub_Canal__c : '';
            strAgencia = String.isNotBlank( objUser.Agencia__c ) ? objUser.Agencia__c : '';
            strServicio = String.isNotBlank( objUser.Servicio__c ) ? objUser.Servicio__c : '';
            strFirmaMain = Utilities.fnGeneradorMD5 ( strFechaMilisApex, strUsuario, strCanal, strSubCanal, strAgencia, strServicio );
            strDatosUsuario = '&usuario=' + strUsuario + '&servicio=' + strServicio + '&agencia=' + strAgencia + '&canal=' + strCanal + '&subcanal=' + strSubCanal;
            strUrlBegin = '?' + 'fechaMilis=' + strFechaMilisApex + strDatosUsuario + '&firma=' + strFirmaMain;
            return strUrlBegin;
        } catch ( QueryException quEx ) {
            throw new AuraHandledException(System.Label.MX_WB_ERROR+quEx);
        }
    }

/**
* Valida y crea el OpportunityLineItem y PricebookEntry
* @param ID idOppor Oportunidad a asignar producto
* @return boolean si pudo hacer el cambio o no
**/
    public static String getMasterLabelValidation (ID idProd) {
        String strMasterLabel = '';
        String strProdName = [ SELECT Name FROM Product2 WHERE Id =: idProd ].Name;
        if ( String.isNotBlank( strProdName ) ) {
            strProdName = strProdName.touppercase( System.Label.MX_WB_CL_LocaleString );
            switch on strProdName {
                when 'SEGURO DE MOTO BANCOMER' {
                    strMasterLabel = STR_ML_SMB;
                }
                when 'SEGURO FRONTERIZO' {
                    strMasterLabel = STR_ML_SF;
                }
                when 'SEGURO DE AUTO FRONTERIZO BANCOMER' {
                    strMasterLabel = STR_ML_SF;
                }
                when 'SEGURO DE AUTO NACIONALIZADO' {
                    strMasterLabel = STR_ML_SF;
                }
                when 'AUTO SEGURO DINÁMICO' {
                    strMasterLabel = STR_ML_ASD;
                }
                when 'AUTO SEGURO DINAMICO' {
                    strMasterLabel = STR_ML_ASD;
                }
            }
        }
        return strMasterLabel;
    }

    /**
     * initDataQuotes Inicializa componente de cotizaciones
     * @param  oppVals valores de oportunidades
     * @return         valores de inicio
     */
    @AuraEnabled
    public static Map<String, Object> initDataQuotes(String oppVals) {
        final Map<String, Object> response = new Map<String, Object>();
        try {
            final Type oppType = Type.forName('Opportunity');
            final Opportunity oppData = (Opportunity)JSON.deserialize(oppVals, oppType);
            final Account accData = [Select Id, Name from Account where Id =: oppData.AccountId];
            final List<Contract> lstcontract = [Select Id, Name, MX_WB_Producto__c from Contract where AccountId =: oppData.AccountId];
            final List<Quote> lstQuotes = [Select Id,OpportunityId,MX_SB_VTS_Familia_Productos__c from Quote where OpportunityId =: oppData.Id ORDER BY CreatedDate DESC];
            final List<Case> lstCase = [Select Id,EstadoSistema__c,Status, ProductId, Product.Name, Product.MX_WB_FamiliaProductos__c from Case where Id =: oppData.MX_SB_VTS_OppCase__c];
            final String recordOutBound = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Outbound').getRecordTypeId();
            final String recordInBound = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ASD').getRecordTypeId();
            response.put('accData', accData);
            response.put('lstcontract', lstcontract);
            response.put('lstCase', lstCase);
            response.put('lstQuotes', lstQuotes);
            response.put('recordOutBound', recordOutBound);
            response.put('recordInBound', recordInBound);
        } catch (QueryException qEx) {
            throw new AuraHandledException ( System.Label.MX_WB_LG_ErrorBack + qEx +qEx.getLineNumber());
        }
        return response;
    }

    /**
    * corretName description
    * @param  producto producto description
    * @return          return description
    */
    public static String correctName (String producto) {
        String finalName = producto;
        finalName = correctASD(finalName);
        finalName = correctFrontNa(finalName);
        finalName = finalName.contains(Label.MX_SB_VTS_MOTO_LBL) ? Label.MX_SB_VTS_SEGURO_MOTO_LBL:producto;
        return finalName;
    }

    /**
    * correctASD description
    * @param  producto producto description
    * @return          correctASDN Valor correcto ASD
    */
    public static String correctASD(String producto) {
        String correctASDN = producto.toUpperCase();
        correctASDN = correctASDN.contains(Label.MX_SB_VTS_DINAMICO_LBL) ? Label.MX_SB_VTS_PRODUCTO_AUTO_DINAMICO_LBL : producto;
        correctASDN = correctASDN.contains(Label.MX_SB_VTS_DINAMICO2_LBL) ? Label.MX_SB_VTS_PRODUCTO_AUTO_DINAMICO_LBL : producto;
        return correctASDN;
    }

    /**
    * correctFrontNa description
    * @param  producto producto description
    * @return          correctFront Valor correcto Fronterizo
    */
    public static String correctFrontNa(String producto) {
        String correctFront = producto.toUpperCase();
        correctFront = correctFront.contains(Label.MX_SB_VTS_FRONTERIZO_LBL) ? Label.MX_SB_VTS_AUTO_FRONTERIZO_LBL : producto;
        correctFront = correctFront.contains(Label.MX_SB_VTS_NACIONALIZADO_LBL) ? Label.MX_SB_VTS_AUTO_NACIONALIZADO_LBL : producto;
        return correctFront;
    }
}