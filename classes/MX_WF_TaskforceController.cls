/**
* ------------------------------------------------------------------------------
* @Nombre: MX_WF_TaskforceController
* @Autor: Sandra Ventura García
* @Proyecto: Workflow Campañas
* @Descripción : Apex controller para componentes MX_WF_CmpInvitados, MX_WF_CmpEnvioEmail
* ------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ------------------------------------------------------------------------------
* 1.0           01/10/2019     Sandra Ventura García	         Desarrollo
* ------------------------------------------------------------------------------
*/
public with sharing class MX_WF_TaskforceController {
  /**
  * @description: construnctor sin argumentos
  * @author Sandra Ventura
  */
    @TestVisible
    private MX_WF_TaskforceController() {}
  /**
  * @description: Obtiene lista de invitados recurrentes
  * @author Sandra Ventura
  */
@AuraEnabled
	public static List<Contact> getInvitadosRecurrentes(String rtContact) {
     try {
          final Id recTypeUser = [SELECT Id FROM RecordType WHERE DeveloperName =: rtContact AND sObjectType = 'Contact'].Id;
          return [Select id, Name, Email, MX_RTE_Direccion_General__c, GBL_WF_Nombre_Completo__c FROM Contact WHERE MX_WF_Recurrente_taskforce__c='Sí'AND recordtypeid=:recTypeUser ORDER BY Name ASC];
      	 } catch(Exception e) {
			throw new AuraHandledException(System.Label.MX_WF_ErrorTaskforce+ e);
         }
    }
  /**
  * @description: Envía plantilla de minuta a invitados
  * @author Sandra Ventura
  */
@AuraEnabled
     public static void sendTemplatedEmail(String whatId) {

    try {
          final Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

        final String[] adressObjId = New String[] {};
        final Id userId = UserInfo.getUserId();
        final List<MX_WF_Invitados_Taskforce__c> listInv = [SELECT 	MX_WF_Correo_electr_nico__c FROM MX_WF_Invitados_Taskforce__c  
                                                            WHERE Taskforce__c IN (SELECT MX_WF_Taskforce__c FROM MX_WF_Minuta_Taskforce__c WHERE Id=:whatId)];
             for (MX_WF_Invitados_Taskforce__c inv : listInv) {
                 adressObjId.add( inv.MX_WF_Correo_electr_nico__c);
             }
             final Id templateId = [select id, name from EmailTemplate where developername = : 'MX_WF_Envio_de_minuta'].id;

            email.setToAddresses(adressObjId);
            email.setTargetObjectId(userId);
            email.setWhatId(whatId);
            email.setTemplateId(templateId);
            email.saveAsActivity = false;

            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        } catch (Exception e) {System.debug(e.getMessage());
                                 throw new AuraHandledException(System.Label.MX_WF_ErrorTaskforce + e);}
    }
  /**
  * @description: Ingresa invitados
  * @author Sandra Ventura
  */
 @AuraEnabled
    public static void insertInv( String[] listInvitados, String idEvent) {
      try {
      final Id IdTaskforce = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: idEvent].MX_WF_Taskforce__c;
      final Id idMinuta = [SELECT Id, Name FROM MX_WF_Minuta_Taskforce__c WHERE MX_WF_Taskforce__c=: IdTaskforce].Id;    
      final List<MX_WF_Invitados_Taskforce__c> listInvit = new List<MX_WF_Invitados_Taskforce__c>();

      final List<Contact> invselec = [Select Id, Name FROM Contact WHERE id=: listInvitados];

        for(Contact invtask: invselec) {

           final MX_WF_Invitados_Taskforce__c invitf = new MX_WF_Invitados_Taskforce__c(MX_WF_Invitado__c = invtask.Id,
                                                                                        MX_WF_Minuta__c = idMinuta,
                                                                                        Taskforce__c=IdTaskforce);
            listInvit.add(invitf);
        }
           insert listInvit;
        } catch (Exception e) {
           throw new AuraHandledException(System.Label.MX_WF_ErrorTaskforce + e);
        }
    }
}