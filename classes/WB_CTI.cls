/**
* Avanxo Colombia
* @author       NN
* Project:      WIBE
* Description:    Clase que implementa metodos para realizar la conexion desde SFDC hacia CTI.
*
* Changes (Version)
* -------------------------------------
*     No.   Date      Author          Description
*     ----- ----------    --------------------  ---------------
* @version  2.0   2016-10-05    Manuel Medina (MM)    Ajuste relacionado con JIRA SAN1-29:
*                             - Validar el producto de la oportunidad comercial y/o cotización para
                              conmutar la combinación de usuario y contraseña que será enviado al CTI.
* @version 2.1      23/02/2018      BEEVA Karen Sanchez (KB) Agregar dos nuevos productos (user,password) para el envio a CTI para Autos UDI y FyL
* @version 2.2      24/08/2018      BEEVA Karen Sanchez (KB) Agregar un nuevo producto (user,password) para el envio a CTI para wiberizate

* @version 3.0      13/11/2018      Minsait Maria Gandarillas (MGGT) Se agrega como input el id intermediario y validación para envio a CTI
********************************************************************************************************************************************/
public with sharing class WB_CTI {

  @future(callout=true)
  public static void ftProcesaSol(String sIdOpp, String sFolioCotizacion, String sIdProducto
  , String sIdOwnerId, String sNameCte, String sTelefono, String sObjeto, Integer iTipo) {
    String sTelefonoFinal;
    System.debug('EN WB_CTI.ftProcesaSol: ' + sTelefono + ' sObjeto: ' + sObjeto + ' iTipo: ' + iTipo);

    //Es una cotización
    if (sObjeto != 'Lead') {
      //Si es del DF
      if (sTelefono.startsWith('55'))
      sTelefonoFinal = '044' + sTelefono;
      //Si es Foraneo
      if (!sTelefono.startsWith('55'))
      sTelefonoFinal = '045' + sTelefono;
    }//Fin si sObjeto != 'Lead'

    //Es un candidato
    if (sObjeto == 'Lead')
      sTelefonoFinal = sTelefono;

    System.debug('EN WB_CTI.ftProcesaSol: ' + sTelefonoFinal);
    //Ya tienes los datos para el envio al WS crea el XML con GenerarXML
    final String sNombre = sNameCte != null ? sNameCte : '' ;
    final String sFolio = sFolioCotizacion == null ? '' : sFolioCotizacion;
    System.debug('EN WB_CTI.ftProcesaSol ANTES DE CREAR EL XML: ' + sTelefonoFinal + ' ' + sNombre + ' ' + sFolio + ' ' + sIdOpp + ' ' + sIdOwnerId + ' ' + sIdProducto);

    if(sIdProducto == label.SAN163_WSReferenceProduct ||
      sIdProducto == label.wsProductoFronterizo ||
      sIdProducto == label.wsProductoSMU ||
      sIdProducto == label.wsProductoAutoUDI ||
      sIdProducto == label.wsProductoSeguroDeAuto||
      sIdProducto == label.wsProductoASD ||
      sIdProducto == label.wsProductoMSD ||
      sIdProducto == label.wsProductoFSD ||
      sIdProducto == label.wsProductoNSD ||
      iTipo == 0 ) {

      System.debug('Entro en if Producto...');
      final String sXML = WB_CTI.GenerarXML(sTelefonoFinal, sNombre, sIdProducto == null ? '' : sIdProducto, sIdOpp, sIdOwnerId, sFolio, iTipo);
        System.debug('EN WB_CTI.ftProcesaSol sXML: ' + sXML);
        WB_CTI.reqSolicitud(sXML, sIdOpp, sObjeto);

    }
  }

  public static void ProcesaSol(String sIdOpp, String sFolioCotizacion, String sIdProducto
  , String sIdOwnerId, String sNameCte, String sTelefono, String sObjeto, Integer iTipo) {
    String sTelefonoFinal;
    System.debug('EN WB_CTI.procesaSol: ' + sTelefono + ' sObjeto: ' + sObjeto + ' iTipo: ' + iTipo);

    //Es una cotización
    if (sObjeto != 'Lead') {
      //Si es del DF
      if (sTelefono.startsWith('55'))
      sTelefonoFinal = '044' + sTelefono;
      //Si es Foraneo
      if (!sTelefono.startsWith('55'))
      sTelefonoFinal = '045' + sTelefono;
    }//Fin si sObjeto != 'Lead'

    //Es un candidato
    if (sObjeto == 'Lead')
      sTelefonoFinal = sTelefono;

    System.debug('EN WB_CTI.procesaSol: ' + sTelefonoFinal);
    //Ya tienes los datos para el envio al WS crea el XML con GenerarXML
    final String sNombre = sNameCte != null ? sNameCte : '' ;
    final String sFolio = sFolioCotizacion == null ? '' : sFolioCotizacion;
    System.debug('EN WB_CTI.procesaSol ANTES DE CREAR EL XML: ' + sTelefonoFinal + ' ' + sNombre + ' ' + sFolio + ' ' + sIdOpp + ' ' + sIdOwnerId + ' ' + sIdProducto);


    if(sIdProducto == label.SAN163_WSReferenceProduct ||
      sIdProducto == label.wsProductoFronterizo ||
      sIdProducto == label.wsProductoSMU ||
      sIdProducto == label.wsProductoAutoUDI ||
      sIdProducto == label.wsProductoSeguroDeAuto||
      sIdProducto == label.wsProductoASD ||
      sIdProducto == label.wsProductoMSD ||
      sIdProducto == label.wsProductoFSD ||
      sIdProducto == label.wsProductoNSD ||
      iTipo == 0 ) {
      System.debug('Entro en if Producto procesaSol...');
      final String sXML = WB_CTI.GenerarXML(sTelefonoFinal, sNombre, sIdProducto == null ? '' : sIdProducto, sIdOpp, sIdOwnerId, sFolio, iTipo);
      System.debug('EN WB_CTI.procesaSol sXML: ' + sXML);
      WB_CTI.reqSolicitud(sXML, sIdOpp, sObjeto);}

  }

  public static String GenerarXML(String sTelefono, String sNombre, String sProducto, String sIdProspecto, String sAgente, String sFolio, Integer iTipo) {

    final String soapXSI = 'http://www.w3.org/2001/XMLSchema-instance';
    final String soapXSD = 'http://www.w3.org/2001/XMLSchema';
    final String soapENV = 'http://schemas.xmlsoap.org/soap/envelope/';
    final String soapWS = Label.soapWS;

    final dom.Document doc = new dom.Document();
    final dom.Xmlnode envelope = doc.createRootElement('Envelope', soapENV, 'soapenv');
    final dom.XmlNode header = envelope.addChildElement('Header', soapENV, 'soapenv');
    final dom.XmlNode body = envelope.addChildElement('Body', soapENV, 'soapenv');

    envelope.setNamespace('xsi', soapXSI);
    envelope.setNamespace('xsd', soapXSD);
    envelope.setNamespace('soapenv', soapENV);
    envelope.setNamespace('ws', soapWS);

    final dom.XmlNode setCall = body.addChildElement('setCall', soapWS, null);
    setCall.setAttribute('soapenv:encodingStyle','http://schemas.xmlsoap.org/soap/encoding/');
    System.debug('EN WB_CTI.GenerarXML iTipo: ' + iTipo + ' sProducto: ' + sProducto);
    if (iTipo == 0 || Test.isRunningTest()) {
            if (String.isNotBlank(sProducto) && sProducto.equals(System.Label.wsProductoASD)) { //CALLME BACK ASD
                final dom.XmlNode nUser = setCall.addChildElement( 'user', null, null ).addTextNode( Label.wsUserASD );
                final dom.XmlNode nPass = setCall.addChildElement( 'pass', null, null).addTextNode( Label.wsPwdASD );
            } else if (String.isNotBlank(sProducto) && (sProducto.equals(System.Label.wsProductoAutoUDI))) {
                System.debug('Entro a WB_CTI.GenerarXML UDI....');
                final dom.XmlNode nUser = setCall.addChildElement( 'user', null, null ).addTextNode( Label.wsUserAutoUDI);
                final dom.XmlNode nPass = setCall.addChildElement( 'pass', null, null).addTextNode( Label.wsPwdAutoUDI);
        } else { //CALLME BACK WIBE*/
          final dom.XmlNode nUser = setCall.addChildElement('user',null,null).addTextNode(Label.wsUser);
          final dom.XmlNode nPass = setCall.addChildElement('pass',null,null).addTextNode(Label.wsPwd);
            }
    }
    if (iTipo == 1 || Test.isRunningTest()) {
      /* BEGIN - Manuel Medina - Conmutacion de usuario y contraseña para tipo de producto Chofer Privado; valor almacenado en etiqueta SAN163_WSReferenceProduct - 05102016 */
      if( String.isNotBlank( sProducto ) && sProducto.equals( System.Label.SAN163_WSReferenceProduct ) ) { //TRACKING CHEFER PRIVADO
        final dom.XmlNode nUser = setCall.addChildElement( 'user', null, null ).addTextNode( Label.SAN163_WSUser );
        final dom.XmlNode nPass = setCall.addChildElement( 'pass', null, null).addTextNode( Label.SAN163_WSPassword );

      }else if (String.isNotBlank(sProducto) &&
               sProducto.equals(System.Label.wsProductoASD) ||
               sProducto.equals(System.Label.wsProductoMSD) ||
               sProducto.equals(System.Label.wsProductoFSD) ||
               sProducto.equals(System.Label.wsProductoNSD) ) { //TRACKING  ASD
                final dom.XmlNode nUser = setCall.addChildElement( 'user', null, null ).addTextNode( Label.wsUserTrackASD);
                final dom.XmlNode nPass = setCall.addChildElement( 'pass', null, null).addTextNode( Label.wsPaswTrackASD);

            }else if (String.isNotBlank(sProducto) && (sProducto.equals(System.Label.wsProductoSMU)
              || sProducto.equals(System.Label.wsProductoSM) )) { //Seguro de Moto y Seguro de Moto UDI  //MOTOS
        System.debug('EN WB_CTI.GenerarXML sProducto0: ' + sProducto);
                final dom.XmlNode nUser = setCall.addChildElement( 'user', null, null ).addTextNode( Label.wsUserSMU );
                final dom.XmlNode nPass = setCall.addChildElement( 'pass', null, null).addTextNode( Label.wsPwdSMU );
             /*KB: Agregar dos nuevos productos (user,password) para el envio a CTI 23/02/2018*/
        //-----------------------------------------------------------

        } else if (String.isNotBlank(sProducto) && (sProducto.equals(System.Label.wsProductoFronterizo)
              || sProducto.equals(System.Label.wsProductoLegalizado) )) {
                System.debug('Entro a WB_CTI.GenerarXML Fronterizo Legalizado....');
                final dom.XmlNode nUser = setCall.addChildElement( 'user', null, null ).addTextNode( Label.wsUserFyL );
                final dom.XmlNode nPass = setCall.addChildElement( 'pass', null, null).addTextNode( Label.wsPwdFyL );
                }else if (String.isNotBlank(sProducto) && (sProducto.equals(System.Label.wsProductoAutoUDI))) {
                System.debug('Entro a WB_CTI.GenerarXML UDI....');
                final dom.XmlNode nUser = setCall.addChildElement( 'user', null, null ).addTextNode( Label.wsUserAutoUDI);
                final dom.XmlNode nPass = setCall.addChildElement( 'pass', null, null).addTextNode( Label.wsPwdAutoUDI);
                //-----------------------------------------------------------

                 /*KB: Agregar producto wiberizate (user,password) para el envio a CTI 24/08/2018*/
        //-----------------------------------------------------------
        } else if (String.isNotBlank(sProducto) && (sProducto.equals(System.Label.wsProductoWiberizate))) {
            System.debug('Entro a WB_CTI.GenerarXML wiberizate....');
            final dom.XmlNode nUser = setCall.addChildElement( 'user', null, null ).addTextNode( Label.wsUserwiberizate );
            final dom.XmlNode nPass = setCall.addChildElement( 'pass', null, null).addTextNode( Label.wsPwdwiberizate );
        //-----------------------------------------------------------
        } else { //TRACKING PARTICULAR
        final dom.XmlNode nUser = setCall.addChildElement( 'user', null, null ).addTextNode( Label.wsUserTW );
        final dom.XmlNode nPass = setCall.addChildElement( 'pass', null, null ).addTextNode( Label.wsPwdWT );
      }
      /* END - Manuel Medina - Conmutacion de usuario y contraseña para tipo de producto Chofer Privado; valor almacenado en etiqueta SAN129_WSReferenceProduct - 05102016 */
    }
    //System.debug('EN WB_CTI.GenerarXML nUser1: ' + nUser + ' nPass1: ' + nPass);
    final dom.XmlNode nPhone = setCall.addChildElement('phone',null,null).addTextNode(sTelefono);
    final dom.XmlNode nName = setCall.addChildElement('name',null,null).addTextNode(sNombre);
    final dom.XmlNode nProduct = setCall.addChildElement('product',null,null).addTextNode(sProducto);
    final dom.XmlNode nLeadId = setCall.addChildElement('leadId',null,null).addTextNode(sIdProspecto);
    final dom.XmlNode nAgent = setCall.addChildElement('agent',null,null).addTextNode(sAgente);
    final dom.XmlNode nCallType = setCall.addChildElement('callType',null,null).addTextNode('ANYONE');
    final dom.XmlNode nFolio = setCall.addChildElement('folio',null,null).addTextNode(sFolio);

    system.debug('XML: ' + Doc.toXmlString());
    return Doc.toXmlString();
  }

  public static void reqSolicitud(String doc, String IdObjeto, String sObjeto) {
    System.debug('EN WB_CTI.reqSolicitud: ' + doc);
    System.debug('EN WB_CTI.reqSolicitud: ' + IdObjeto);
    System.debug('EN WB_CTI.reqSolicitud: ' + sObjeto);

    final String endpoint = Label.wsEndPointWibe00; //ANTES: http://201.148.35.186/ws/dialer.php ACT: http://201.148.35.186/ws/

    final HttpRequest request = new HttpRequest();
    HttpResponse response = new HttpResponse();
    final Http http = new Http();

    request.setEndpoint(endpoint);
    request.setHeader('SOAPAction' , Label.WsCTIAction);
    request.setMethod('POST');
    request.setHeader('Accept-Encoding', 'gzip,deflate');
    request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
    request.setHeader('Host', 'vcip.com.mx');
    request.setHeader('Connection', 'Keep-Alive');
    request.setHeader('User-Agent', 'Apache-HttpClient/4.1.1 (java 1.5)');
    request.setTimeout(120000);
    request.setBody(doc);

    System.debug('EN WB_CTI.reqSolicitud doc: ' + doc);

    //Si no es una prueba mandalo a WIBE
    String sXml = '';
    if (!Test.isRunningTest()) {
      response = http.send(request);
      sXml = response.getBody();
      System.debug('El response es: ' + response);
    }

    final list<string> lstNodos = new list<string>{'<return>'};
    final list<string> lstNodosF = new list<string>{'</return>'};
    String sRespuesta = '';
    Integer iInicio = 0;
    Integer iFin = 0;
    String sXmlx = '';

    sRespuesta = sXml;
    if(Test.isRunningTest())
      sXml = sTestSolRespuesta();

    System.debug('EN WB_CTI.reqSolicitud sRespuesta: ' + sRespuesta);
    if (sXml.contains('OK') || test.isRunningTest()) {
      if (test.isRunningTest()) sXml = sTestSolRespuesta();
      final string rXml = sXml.replace('<return xsi:type="xsd:string">','<return>');
      System.debug('XmlReplace: ' + rXml);
      sXml = rXml;
      Integer iNodo = 0;
      for (String e : lstNodos) {
        iInicio = sXml.IndexOf(e,0);
        iFin = sXml.IndexOf(lstNodosF[iNodo],0);
        iFin = iFin + lstNodosF[iNodo].length();
        if (sXml.length() >0 && iInicio > 0 && iFin > 0) {
          sXmlx = sXml.substring(iInicio, iFin);
          system.debug('ValXML' + sXmlx);
          if (sXmlx.length() > 0) {
            if(e == '<return>')
            sRespuesta = parse(sXmlx);
          }
        }
        iNodo++;
      }
      //system.debug('Respueseta: ' + sRespuesta);
    }

    If (Test.isRunningTest())
    sXml = sTestSolRespuestaError();

    if (sXml.contains('ERROR') || test.isRunningTest()) { //Error.
      if (test.isRunningTest()) sXml = sTestSolRespuestaError();
      final string rXml = sXml.replace('<return xsi:type="xsd:string">','<return>');
      sXml = rXml;
      System.debug('XmlReplace: ' + rXML);
      final String Titulo = '';
      final String Codigo = '';
      final String IDMensaje = '';
      final String Mensaje = '';
      final String Texto = '';

      Integer iNodo = 0;
      for (String e : lstNodos) {
        iInicio = sXml.IndexOf(e,0);
        iFin = sXml.IndexOf(lstNodosF[iNodo],0);
        iFin = iFin + lstNodosF[iNodo].length();
        if (sXml.length() >0 && iInicio > 0 && iFin > 0) {
          sXmlx = sXml.substring(iInicio, iFin);
          system.debug('ValXML' + sXmlx);
          if (sXmlx.length() > 0) {
            if(e == '<return>')
            sRespuesta = parse(sXmlx);
          }
        }
        iNodo++;
      }
      //system.debug('Respueseta: ' + sRespuesta);
    }

    System.debug('EN WB_CTI.reqSolicitud sRespuesta: ' + sRespuesta);
    //Actualizar un objeto en caso de ser necesario.
    if (sObjeto == 'Lead') {
      final Lead entLead = new Lead();
      entLead.Id = IdObjeto;
      entLead.xmlRespuesta__c = sRespuesta;
      entLead.xmlEnvio__c = doc;
      entLead.EnviarCTI__c = true;
      if (!Test.isRunningTest())
      update entLead;
      System.debug('EN WB_CTI.reqSolicitud entLead: ' + entLead);
    }

    if (sObjeto == 'Opportunity') {
      final Opportunity entOpp = new Opportunity();
      entOpp.Id = IdObjeto;
      entOpp.xmlRespuesta__c = sRespuesta;
      entOpp.xmlEnvio__c = doc;
      entOpp.EnviarCTI__c = true;
      System.debug('EN WB_CTI.reqSolicitud entOpp: ' + entOpp);

      //Actaliza la Opp entOpp
      if (!Test.isRunningTest()) update entOpp;

      //Consulta los datos de la Opp y crea la taree si FechaHoracontacto__c
      for (Opportunity objOpp : [Select id, Name, FechaHoracontacto__c From Opportunity
        Where id = :entOpp.id And FechaHoracontacto__c != null]) {
        //WB_CrearTarea_cls.fnCrearTareaOportunidad(entOpp, null );
      }
      System.debug('EN WB_CTI.reqSolicitud entOpp: ' + entOpp);
    }//Fin si sObjeto == 'Opportunity'

    // Get the base URL.
    final String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
    System.debug('Base URL: ' + sfdcBaseURL );

    // Get the URL for the current request.
    final String currentRequestURL = URL.getCurrentRequestUrl().toExternalForm();
    System.debug('Current request URL: ' + currentRequestURL);

    // Get some parts of the base URL.
    System.debug('Host: ' + URL.getSalesforceBaseUrl().getHost());
    System.debug('Protocol: ' + URL.getSalesforceBaseUrl().getProtocol());

  }

  public static String parse(String toParse) {
    String r = '';
    final DOM.Document doc = new DOM.Document();
    try {
      doc.load(toParse);
      final DOM.XMLNode root = doc.getRootElement();
      return walkThrough(root);
    } catch (System.XMLException e) {  // invalid XML
      r = e.getMessage();
    }
    system.debug('ParseRes: ' + r);
    return r;
  }

  public static String walkThrough(DOM.XMLNode node) {
    String result = '\n';
    if (node.getNodeType() == DOM.XMLNodeType.COMMENT) {
      return 'Comment (' +  node.getText() + ')';
    }

    if (node.getNodeType() == DOM.XMLNodeType.TEXT) {
      return 'Text: ' + node.getText() + ' ';
    }

    if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
      //result += 'Element: ' + node.getName();
      if (node.getText() != '') {
        //result += ', Valor=' + node.getText();
        result += node.getText();
      }

      if (node.getAttributeCount() > 0) {
        for (Integer i = 0; i< node.getAttributeCount(); i++ ) {
          result += ', attribute #' + i + ':' + node.getAttributeKeyAt(i) + '=' + node.getAttributeValue(node.getAttributeKeyAt(i), node.getAttributeKeyNsAt(i));
          result += ', text=' + node.getText();
        }
      }
      for (Dom.XMLNode child: node.getChildElements()) {
        result += walkThrough(child);
      }
      return result;
    }
    return '';  //should never reach here
  }

  public static String sTestSolRespuesta() {
    final String Res =   '<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://vcip.com.mx/ws/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">' +
      '<SOAP-ENV:Body>' +
        '<ns1:setCallResponse>' +
          '<return xsi:type="xsd:string">OK</return>' +
        '</ns1:setCallResponse>' +
      '</SOAP-ENV:Body>' +
    '</SOAP-ENV:Envelope>';
    return Res;
  }

  public static String sTestSolRespuestaError() {
    final String Res =   '<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://vcip.com.mx/ws/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">' +
      '<SOAP-ENV:Body>' +
        '<ns1:setCallResponse>' +
          '<return xsi:type="xsd:string">ERROR: El telefono debe de componerse de 12 o 13 digitos</return>' +
        '</ns1:setCallResponse>' +
      '</SOAP-ENV:Body>' +
    '</SOAP-ENV:Envelope>';
    return Res;
  }

    public static boolean manualShareRead(Id recordId, Id userOrGroupId) {

        String sId = '';
        if(!Test.isRunningTest()) {
          for(AccountShare a :  [Select Id, AccountId From AccountShare Where AccountId =: recordId and AccountAccessLevel != 'All' limit 1]) {
              sId = a.Id;
          }
      }

        final AccountShare Shr  = new AccountShare();
        Shr.AccountId = recordId;
        Shr.UserOrGroupId = userOrGroupId;
        Shr.AccountAccessLevel = 'Read';
        Shr.OpportunityAccessLevel = 'none';
        Shr.CaseAccessLevel = 'none';
        //Shr.RowCause = Schema.AccountShare.RowCause.Manual;

        Database.SaveResult sr;
        if(sId == '' || Test.isRunningTest()) {
            if(!Test.isRunningTest()) sr = Database.insert(Shr,false);
        }
        if(sId != '' || Test.isRunningTest()) {
            Shr.Id = sId;
            if(!Test.isRunningTest()) sr = Database.update(shr,false);
        }

        if(sr.isSuccess() || Test.isRunningTest()) {
            System.debug('SharingMessageError: El registro fue compartido satisfactoriamente.');
            if (!Test.isRunningTest()) return true;
        }
        if(sr.isSuccess() == false || Test.isRunningTest()) {
            final Database.Error err = sr.getErrors()[0];
            System.debug('SharingMessageError: -->' + err.getMessage());
            if (!Test.isRunningTest())return false;
       }
       return false;
   }

}