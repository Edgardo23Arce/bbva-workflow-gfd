/****************************************************************************************************
author: Javier Ortiz Flores
company: Indra
Description: controlador After Update para proceso Outbound,
donde se crea un contrato por opportunityLineItem asociado a Oportunidad

Information about changes (versions)
-------------------------------------
Number    Dates           Author                       Description
------    --------        --------------------------   -----------
1.0       01-Ene-2019     Javier Ortiz Flores          Creación de la Clase
1.1       11-Feb-2019     Eduardo Hernández            Log de ultima modificación - Se agregan y quitan System debugs para clase Test
1.2   6/5/2019 17:16:30   Eduardo Hernández Cuamatzi   Cambio de modelo y adecuación para multi producto
1.2.1       13/05/2019     Arsenio Perez               Corrección lógica de consulta.
1.2.2       22/05/2019     Eduardo Hernández           Se agrega consulta anidada para recuperar Leads de campaña activa
****************************************************************************************************/
public without sharing class MX_WB_contratosOpp_cls {
    /** ID tipo de registro **/    
    public static final Id IdRecTypOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_RecordTypeOutOpp).getRecordTypeId();
	
    /**
     * creaContrato Creacion de contrato
     * @param	lstOLI Una lista de la oportunidad
     * @param	valuesOpp Valores de la oportunidad
     * @param	valuesProduct	Valores del producto
     * @param	clippNums	Cadena String
     */
    public static void creaContrato(List<OpportunityLineItem> lstOLI, Map<Id, Opportunity> valuesOpp, Map<Id, Product2> valuesProduct, Set<String> clippNums, List<Lead> leadsOutbound) {
        final List<Contract> listContratos = new List<Contract>();
        final Map<Id, Contract> valuesContract = new Map<Id, Contract>([Select Name,MX_WB_noPoliza__c from Contract where Name IN :clippNums]);
        for(OpportunityLineItem iteraOLI : lstOLI) {
            if(!checkContrato(iteraOLI.MX_WB_noPoliza__c, valuesContract)) {
                final Contract contratoNew = generaContrato(iteraOLI, valuesOpp, valuesProduct);
                if(listContratos.contains(contratoNew)==false){
                    listContratos.add(contratoNew);
                }
                
                if (valuesOpp.get(iteraOLI.OpportunityId).RecordType.DeveloperName.equals(System.Label.MX_SB_VTS_RecordTypeOutOpp)) {
                    createTask(valuesOpp.get(iteraOLI.OpportunityId), leadsOutbound);
                }
            }
        }
        if(!listContratos.isEmpty()) {
            insert listContratos;
            closeOppor(listContratos,valuesOpp);

        }
    }
    /**
    * @Method: Close to Opportunity 
    * @Param: listContratos and valuesOpp
    * @Description: 
    */
    private static void closeOppor(List<Contract> listContratos, Map<Id, Opportunity> valuesOpp){
        final List<Opportunity> OppstoUpdate = new List<Opportunity>();
        Map<Id, Contract> MapCont = new Map<Id,Contract>();
        for(Contract temp: listContratos){
            MapCont.put(temp.MX_WB_Oportunidad__c, temp);
        }  
        for(Opportunity opptem: valuesOpp.values()){
            if( String.isNotEmpty(MapCont.get(opptem.Id).Id) && 
                String.isNotEmpty(opptem.Owner.Profile.Name) && 
                opptem.Owner.Profile.Name == system.label.MX_SB_VTS_ProfileAdmin){
                    opptem.StageName=system.label.MX_SB_VTS_CLOSEDWON_LBL;
                    OppstoUpdate.add(opptem);
            }
        }
        if(OppstoUpdate.isEmpty()==false){
            update OppstoUpdate;
        }
    }
    
    /**
     * createTask Creacion de tarea
     * @param	iteraOpp Una iteración de la oportunidad
     */
    public static void createTask(opportunity iteraOpp, List<Lead> leadsOutbound) {
        final set<id> idLeads = new set<id>();
        for(Lead iteraLead : leadsOutbound) {
            if(String.isNotBlank(iteraLead.ConvertedAccountId) && iteraLead.ConvertedAccountId.equals(iteraOpp.AccountId)) {
                idLeads.add(iteraLead.Id);
            }
        }

        if(!idLeads.isEmpty()) {
            for(id iteraId : idLeads) {
                MX_WB_regresosCall_cls.procesaCall(iteraId, 'ASD', iteraOpp.Id);
            }
        }
    }
    
    /**
     * checkContrato Revisión de contrato
     * @param	clippNum	Una cadena
     * @param	valuesContract	valores del contrato
     * @return	existe Valor boolean
     */
    public static boolean checkContrato(String clippNum, Map<Id, Contract> valuesContract) {
        boolean existe = false;
        for(contract iteraContrato : valuesContract.values()) {
            if(iteraContrato.MX_WB_noPoliza__c.equals(clippNum)) {
                existe = true;
            }
        }
        return existe;
    }
	
    /**
     * generaContrato Generación de contrato
     * @param	iteraOppLineItm	Un articulo de la oportunidad
     * @param	valuesOpp	valores de la oportunidad
     * @param	valuesProduct	valores del producto
     * @return	contratoNew Nuevo contrato
     */
    public static contract generaContrato(OpportunityLineItem iteraOppLineItm, Map<Id, Opportunity> valuesOpp, Map<Id, Product2> valuesProduct) {
        Contract contratoNew = new Contract();
        contratoNew.AccountId = valuesOpp.get(iteraOppLineItm.OpportunityId).AccountId;
        contratoNew.MX_WB_Oportunidad__c = iteraOppLineItm.OpportunityId;
        switch on valuesProduct.get(iteraOppLineItm.Product2Id).MX_WB_FamiliaProductos__r.Name {
            when  'ASD' {
               contratoNew= fillContractASD(iteraOppLineItm, contratoNew);       
            }
            when  'Hogar' {
               contratoNew= fillContractASD(iteraOppLineItm, contratoNew);       
            }
        }
        return contratoNew;
    }
	
    /**
     * generaContrato Generación de contrato
     * @param	iteraOppLineItm	Un articulo de la oportunidad
     * @param	contratoNew	Nuevo contrato
     */
    public static Contract fillContractASD(OpportunityLineItem iteraOppLineItm, Contract contratoNew) {
        contratoNew.MX_WB_Sexo__c = validEmptyField(iteraOppLineItm.MX_WB_Sexo__c);
        contratoNew.MX_WB_edad_de_conductor_adicional__c =  validEmptyField(iteraOppLineItm.MX_WB_edad_de_conductor_adicional__c);
        contratoNew.MX_WB_EsContratante__c = validEmptyField(iteraOppLineItm.MX_WB_EsContratante__c);
        contratoNew.MX_WB_nombreAsegurado__c = validEmptyField(iteraOppLineItm.MX_WB_nombreAsegurado__c);
        contratoNew.MX_WB_apellidoMaternoAsegurado__c = validEmptyField(iteraOppLineItm.MX_WB_apellidoMaternoAsegurado__c);
        contratoNew.MX_WB_apellidoPaternoAsegurado__c = validEmptyField(iteraOppLineItm.MX_WB_apellidoPaternoAsegurado__c);
        contratoNew.MX_WB_emailAsegurado__c = validEmptyField(iteraOppLineItm.MX_WB_emailAsegurado__c);
        contratoNew.MX_WB_celularAsegurado__c = iteraOppLineItm.MX_WB_celularAsegurado__c;
        contratoNew.MX_WB_telefonoAsegurado__c = iteraOppLineItm.MX_WB_telefonoAsegurado__c;
        contratoNew.MX_WB_noPoliza__c = iteraOppLineItm.MX_WB_noPoliza__c;
        contratoNew.Name = iteraOppLineItm.MX_WB_noPoliza__c;
        contratoNew.MX_WB_Descuento_con_cupones__c = validEmptyField(iteraOppLineItm.MX_WB_Descuento_con_cupones__c);
        contratoNew.StartDate = iteraOppLineItm.MX_WB_fechaEmision__c;
        contratoNew.MX_WB_Producto__c = validEmptyField(iteraOppLineItm.Product2Id);
        contratoNew.status = System.Label.MX_SB_VTS_ContractStatus;
        contratoNew.MX_WB_cuenta_con_dispositivo_satelital__c = validEmptyField(iteraOppLineItm.MX_WB_cuenta_con_dispositivo_satelital__c);
        contratoNew.MX_WB_SaleACarretera__c = validEmptyField(iteraOppLineItm.MX_WB_SaleACarretera__c);
        contratoNew.MX_WB_Alguien_mas_conduce__c = validEmptyField(iteraOppLineItm.MX_WB_Alguien_mas_conduce__c);
        contratoNew.MX_WB_comentariosPersonalizados__c = validEmptyField(iteraOppLineItm.MX_WB_comentariosPersonalizados__c);
        contratoNew.MX_WB_existen_conductores_adicionales__c = validEmptyField(iteraOppLineItm.MX_WB_existen_conductores_adicionales__c);
        contratoNew.MX_WB_numeroSerie__c = validEmptyField(iteraOppLineItm.MX_WB_numeroSerie__c);
        contratoNew.MX_WB_Modelo__c = validEmptyField(iteraOppLineItm.MX_WB_Modelo__c);
        contratoNew.MX_WB_placas__c = validEmptyField(iteraOppLineItm.MX_WB_placas__c);
        contratoNew.MX_WB_origen__c = validEmptyField(iteraOppLineItm.MX_WB_origen__c);
        contratoNew.MX_WB_subMarca__c = validEmptyField(iteraOppLineItm.MX_WB_subMarca__c);
        contratoNew.MX_WB_Marca__c = validEmptyField(iteraOppLineItm.MX_WB_Marca__c);
        contratoNew.MX_WB_Tipo_de_auto__c = validEmptyField(iteraOppLineItm.MX_WB_Tipo_de_auto__c);
        contratoNew.MX_SB_VTS_Precio_Anual__c = String.valueOf(iteraOppLineItm.TotalPrice);
        contratoNew.Id = null;
        return contratoNew;
    }
	
    /**
     * validEmptyField Valida campo
     * @param	evaluateValue	Valida valor
     * @return	validField	Retorna campo validado
     */
    public static String validEmptyField(String evaluateValue) {
        String validField = '';
        if (String.isNotBlank(evaluateValue) && String.isNotEmpty(evaluateValue)) {
            validField = evaluateValue;
        }
        return validField ;
    }
}