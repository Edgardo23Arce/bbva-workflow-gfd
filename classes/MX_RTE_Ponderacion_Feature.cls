/* ------------------------------------------------------------------------------
* @Nombre: MX_RTE_Ponderacion_Feature
* @Autor: Marco Ivan Mejia
* @Proyecto: Workflow GFD/RTE
* @Descripción : Apex controller para componente MX_RTE_CalcularPonderacion
* ------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ------------------------------------------------------------------------------
* 1.0           01/10/2019     Marco Ivan Mejia	                Desarrollo
* ------------------------------------------------------------------------------
*/
public class MX_RTE_Ponderacion_Feature {
      /**
  * @description: Obtiene lista de Features relacionadas a la iniciativa que se pretende editar.
  * @author Marco Ivan Mejia
  */    
     @AuraEnabled
    public static List<Case> getFeatures(){
        
        List<Case> FeatureReturn_list = [SELECT  Subject, MX_RTE_Ponderacion__c  
                                               FROM Case ORDER BY Subject ASC];
        
        
        return FeatureReturn_list;
    }

}