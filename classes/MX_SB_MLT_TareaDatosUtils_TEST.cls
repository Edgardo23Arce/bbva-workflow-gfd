/*
----------------------------------------------------------
* Nombre: MX_SB_MLT_TareaDatosUtils_TEST
* Autor Oscar Martínez
* Proyecto: Siniestros - BBVA Bancomer
* Descripción : Clase que prueba los Methods de la clase MX_SB_MLT_TareaDatosUtils_Cls
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Desripción<p />
* --------------------------------------------------------------------------------
* 1.0           08/05/2019     Oscar Martínez                       Creación
* 1.1           15/05/2019     Oscar Martínez                       Se corrijen issues de sonar.
* 1.2           15/05/2019     Oscar Martínez                       Cambio de Profile a CustomLabel
* --------------------------------------------------------------------------------
*/
@isTest
public  class MX_SB_MLT_TareaDatosUtils_TEST {
    @testSetup static void setup() {
        final User testUser = MX_WB_TestData_cls.crearUsuario ( 'TestLastName', Label.MX_SB_VTS_ProfileAdmin );
        insert testUser;
        final Account accRec = MX_WB_TestData_cls.crearCuenta ( 'LastName', 'MX_WB_rt_PAcc_Telemarketing' );
        accRec.OwnerId = testUser.Id;
        accRec.Correo_Electronico__c = 'prueba@wibe.com';
        insert accRec;
        final Opportunity oppRec = MX_WB_TestData_cls.crearOportunidad ( 'Test', accRec.Id, testUser.Id, 'MX_WB_RT_Telemarketing' );
        oppRec.FolioCotizacion__c = null;
        oppRec.TelefonoCliente__c = '1234567890';
        oppRec.NumerodePoliza__c='000101';
        insert oppRec;
        final Contract contrato = new Contract();
        contrato.MX_WB_noPoliza__c = oppRec.NumerodePoliza__c;
        contrato.MX_WB_Oportunidad__c = oppRec.Id;
        contrato.AccountId = accRec.Id;
        insert contrato;
    }
    static testMethod void queryObjects() {
        final Account accRec = MX_WB_TestData_cls.crearCuenta ( 'LastName', 'MX_WB_rt_PAcc_Telemarketing' );
        insert accRec;
        final Case caso = new Case(Status='new',Origin='phone',Priority='Medium',AccountId=accRec.Id,MX_SB_MLT_URLLocation__c='99.3123123,-19.45345');
        insert caso;
        final List<Case> lstResponse = MX_SB_MLT_TareaDatosUtils_Cls.obtieneCase(caso.Id);
        System.assert(lstResponse.size() > 0, 'Retorna dato');
    }
    static testMethod void queryObjectsDato() {
        final Account accRec = MX_WB_TestData_cls.crearCuenta ( 'LastName', 'MX_WB_rt_PAcc_Telemarketing' );
        insert accRec;
        Case caso = new Case(Status='new',Origin='phone',Priority='Medium',AccountId=accRec.Id,MX_SB_MLT_URLLocation__c='99.3123123,-19.45345');
        insert caso;
        final List<Case> lstResponse = MX_SB_MLT_TareaDatosUtils_Cls.obtieneCase(caso.Id);
        final Case casoResultado =  lstResponse.get(0);
        System.assertEquals(casoResultado.MX_SB_MLT_URLLocation__c, '99.3123123,-19.45345', 'Dato correcto');
    }
    static testMethod void validaDatoNull() {
        final String strNull = null;
        String strRespuesta ='';
        strRespuesta = MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio(strNull);
        System.assert(String.isEmpty(strRespuesta), 'Funciona dato nulo');
    }
    static testMethod void validaDatoVacio() {
        final String strVacio = '';
        String strRespuesta ='';
        strRespuesta = MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio(strVacio);
        System.assert(String.isEmpty(strRespuesta), 'Funciona dato nulo');
    }
    static testMethod void validaDato() {
        final String strDato = 'dato';
        String strRespuesta ='';
        strRespuesta = MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio(strDato);
        System.assertEquals(strDato, strRespuesta, 'Funciona dato'); 
    }
}