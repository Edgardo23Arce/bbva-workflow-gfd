/**-------------------------------------------------------------------------
* Nombre: MX_SB_VTS_rtwCotizacion_ext
* @author Julio Medellin
* Proyecto: MW SB VTS - BBVA 
* Descripción : Clase extención  de rtwCotizacion 
* --------------------------------------------------------------------------
*                         Fecha           Autor                   Descripción
* -------------------------------------------------------------------
* @version 1.0           02/05/2019      Julio Medellin           Divide metodos y propíedades de la clase rtwCotizacion
* --------------------------------------------------------------------------*/ 
public virtual with sharing class MX_SB_VTS_rtwCotizacion_ext {//NOSONAR
    /*Variabale cuentapara nulos*/
    public static final account accountnull  = null;
    /*Variabale para nulos*/
    public static final id idnull  = null;
    /*Variabale para vacios*/
    public static final string stringEmpty  = '';
    /*Variabale para retener datos del cliente*/
    public static Account contratante = new Account();//NOSONAR
    /*Variabale para retener id global*/ 
    public  static String Idglobalcontratante= '';//NOSONAR
    
    public MX_SB_VTS_rtwCotizacion_ext(){}
    
    private static void upsertCotizaA( reqCotizacion cotizacionret,String sIdContratanteret, Account cteContratanteret, Account aseguradoOriginalret, infoCompbeneficiario objinfoCompbeneficiarioret){
        Account aseguradoOriginal= aseguradoOriginalret;
        final reqCotizacion cotizacion = cotizacionret;
        String sIdContratante=sIdContratanteret;
        Account cteContratante=cteContratanteret; 
        final infoCompbeneficiario objinfoCompbeneficiario=objinfoCompbeneficiarioret;
        
        if ( String.isBlank( sIdContratante ) ) {
            for ( Account cliente : [SELECT id, Name, Correo_Electronico__c, isPersonAccount, PersonBirthdate, FirstName, LastName, PHONE,
                                     edad__c, Genero__c, RFC__c, Nacionalidad__c, Profesion__c, Colonia__c, BillingPostalCode,
                                     Numero_Exterior__c, Numero_Interior__c, AccountSource, BillingCity, BillingState, BillingCountry,
                                     BillingStreet, Delegacion__c FROM Account WHERE id =: cotizacion.cliente ] ) {
                                         sIdContratante = cliente.id;
                                         cteContratante = new Account( id = cliente.id );
                                         aseguradoOriginal = cliente;
                                     }
            cteContratante.FirstName = cotizacion.nombreDelContratante.length() > 40 ? cotizacion.nombreDelContratante.subString(0, 40) : cotizacion.nombreDelContratante;
            cteContratante.LastName = cotizacion.apellidoPaternoContratante + ' ' + cotizacion.apellidoMaternoContratante;
            cteContratante.PersonEmail = cotizacion.correoElectronicoContratante;
            cteContratante.Correo_Electronico__c = cotizacion.correoElectronicoContratante;
            cteContratante.Phone = cotizacion.telefonoCelularContratante;
            cotizacion.nombreDelContratante = aseguradoOriginal.FirstName;
            cotizacion.apellidoPaternoContratante = aseguradoOriginal.LastName;
            cotizacion.apellidoMaternoContratante = stringEmpty;
            cotizacion.correoElectronicoContratante = String.isNotBlank(aseguradoOriginal.Correo_Electronico__c) ? aseguradoOriginal.Correo_Electronico__c : stringEmpty;
            cotizacion.telefonoCelularContratante = aseguradoOriginal.Phone;
            objinfoCompbeneficiario = new infoCompbeneficiario();
            objinfoCompbeneficiario.fechaNacimiento = String.isNotBlank(String.valueOf( aseguradoOriginal.PersonBirthdate ) ) ? String.valueOf( aseguradoOriginal.PersonBirthdate ) : stringEmpty;
            objinfoCompbeneficiario.edad = String.isNotBlank(String.valueOf( aseguradoOriginal.edad__c ) ) ? String.valueOf( aseguradoOriginal.edad__c ) : stringEmpty;
            objinfoCompbeneficiario.sexoDelConductor = aseguradoOriginal.Genero__c;
            objinfoCompbeneficiario.rfc = aseguradoOriginal.RFC__c;                   
            objinfoCompbeneficiario.origen = aseguradoOriginal.AccountSource;
            objinfoCompbeneficiario.ciudad = aseguradoOriginal.BillingCity;
            objinfoCompbeneficiario.estado = aseguradoOriginal.BillingState;
            objinfoCompbeneficiario.pais = aseguradoOriginal.BillingCountry;
            objinfoCompbeneficiario.delegacion = aseguradoOriginal.Delegacion__c;
            objinfoCompbeneficiario.nacionalidad = aseguradoOriginal.Nacionalidad__c;
            objinfoCompbeneficiario.profesion = aseguradoOriginal.Profesion__c;
            objinfoCompbeneficiario.colonia = aseguradoOriginal.Colonia__c;
            objinfoCompbeneficiario.calleOAvenida = aseguradoOriginal.BillingStreet;
            objinfoCompbeneficiario.codigoPostal = aseguradoOriginal.BillingPostalCode;
            objinfoCompbeneficiario.numeroExterior = aseguradoOriginal.Numero_Exterior__c;
            objinfoCompbeneficiario.numeroInterior = aseguradoOriginal.Numero_Interior__c;
            update cteContratante;
            Idglobalcontratante=sIdContratante;
        }  
        
        
    }
    
    
    private static void upsertCotiza2( reqCotizacion cotizacionret,String sIdContratanteret, Account cteContratanteret, Account aseguradoOriginalret, infoCompbeneficiario objinfoCompbeneficiarioret,Boolean bActDatosCteret,Account Aseguradoret){
        Account aseguradoOriginal= aseguradoOriginalret;
        final reqCotizacion cotizacion = cotizacionret;
        String sIdContratante=sIdContratanteret;
        Account cteContratante=cteContratanteret; 
        infoCompbeneficiario objinfoCompbeneficiario=objinfoCompbeneficiarioret;
        Boolean bActDatosCte=bActDatosCteret;
        final Account Asegurado= Aseguradoret;
        
        for (Account cliente : [ SELECT id, Name, Correo_Electronico__c, isPersonAccount, PersonBirthdate, FirstName, LastName, PHONE, edad__c, Genero__c, 
                                RFC__c, Nacionalidad__c, Profesion__c, Colonia__c, BillingPostalCode, Numero_Exterior__c, Numero_Interior__c, AccountSource,
                                BillingCity, BillingState, BillingCountry, BillingStreet, Delegacion__c, ( SELECT apellidoMaternoContratante__c, apellidoPaternoContratante__c,
                                                                                                          nombreDelContratante__c, telefonoCelularContratante__c, correoElectronicoContratante__c, Estatus__c
                                                                                                          FROM Opportunities WHERE Estatus__c = : System.Label.MX_SB_VTS_STATUSEMITIDA_LBL ) FROM Account WHERE Correo_Electronico__c =: cotizacion.correoElectronicoContratante ]) {
                                                                                                              sIdContratante = cliente.id;
                                                                                                              contratante = cliente;
                                                                                                              cteContratante = new Account( id = cliente.id );
                                                                                                              Asegurado.id = cliente.id;
                                                                                                              bActDatosCte = cliente.Opportunities.isEmpty()==false?false:true;  
                                                                                                          }
        if ( String.isNotBlank( sIdContratante ) ) {
            cteContratante= getcteContratante(cteContratante,cotizacion,bActDatosCte);
            
            cteContratante.Phone = cotizacion.telefonoCelularContratante;
            Account delBene;
            for ( Account cliente : [ SELECT id, Name, Correo_Electronico__c, isPersonAccount, PersonBirthdate, FirstName, LastName, PHONE,
                                     edad__c, Genero__c, RFC__c, Nacionalidad__c, Profesion__c, Colonia__c, BillingPostalCode, 
                                     Numero_Exterior__c, Numero_Interior__c, AccountSource, BillingCity, BillingState, BillingCountry,
                                     BillingStreet, Delegacion__c, (SELECT ID FROM Opportunities)  FROM Account
                                     WHERE id =: cotizacion.cliente ] ) {
                                         aseguradoOriginal = cliente;                                         
                                         delBene = ( cliente.Opportunities.isEmpty() )?new Account( id = cotizacion.cliente):accountnull;
                                     }
            cotizacion.nombreDelContratante = aseguradoOriginal.FirstName;
            cotizacion.apellidoPaternoContratante = aseguradoOriginal.LastName;
            cotizacion.apellidoMaternoContratante = stringEmpty;
            cotizacion.correoElectronicoContratante = String.isNotBlank(aseguradoOriginal.Correo_Electronico__c) ? aseguradoOriginal.Correo_Electronico__c : stringEmpty;
            cotizacion.telefonoCelularContratante = aseguradoOriginal.Phone;
            objinfoCompbeneficiario = new infoCompbeneficiario();
            objinfoCompbeneficiario = getobjinfoCompbeneficiario(objinfoCompbeneficiario, aseguradoOriginal);
            update cteContratante;
            if ( delBene != null ) {
                delete delBene;
            }
        }
        Idglobalcontratante=sIdContratante;
        upsertCotizaA(cotizacion,sIdContratante,cteContratante,aseguradoOriginal,objinfoCompbeneficiario);       
        
    }
    private static account getcteContratante(account gcteContratante,reqCotizacion cotizacion, boolean bActDatosCte){
        final account cteContratante= gcteContratante;
        if(bActDatosCte || Test.isRunningTest()){
            cteContratante.FirstName = cotizacion.nombreDelContratante.length() > 40? cotizacion.nombreDelContratante.subString(0, 40) : cotizacion.nombreDelContratante;
            cteContratante.LastName = cotizacion.apellidoPaternoContratante + ' ' + cotizacion.apellidoMaternoContratante;
            cteContratante.PersonEmail = cotizacion.correoElectronicoContratante;
            cteContratante.Correo_Electronico__c = cotizacion.correoElectronicoContratante;
        }
        return cteContratante;
    }
    private static infoCompbeneficiario getobjinfoCompbeneficiario(infoCompbeneficiario infoCompbeneficiar,  Account aseguradoOriginal){
        final infoCompbeneficiario  objinfoCompbeneficiario=infoCompbeneficiar;
        objinfoCompbeneficiario.calleOAvenida = aseguradoOriginal.BillingStreet;
        objinfoCompbeneficiario.ciudad = aseguradoOriginal.BillingCity;
        objinfoCompbeneficiario.codigoPostal = aseguradoOriginal.BillingPostalCode;
        objinfoCompbeneficiario.colonia = aseguradoOriginal.Colonia__c;
        objinfoCompbeneficiario.delegacion = aseguradoOriginal.Delegacion__c;
        objinfoCompbeneficiario.edad = String.isNotBlank(String.valueOf(aseguradoOriginal.edad__c)) ? String.valueOf(aseguradoOriginal.edad__c) : stringEmpty;
        objinfoCompbeneficiario.estado = aseguradoOriginal.BillingState;
        objinfoCompbeneficiario.nacionalidad = aseguradoOriginal.Nacionalidad__c;
        objinfoCompbeneficiario.numeroExterior = aseguradoOriginal.Numero_Exterior__c;
        objinfoCompbeneficiario.numeroInterior = aseguradoOriginal.Numero_Interior__c;
        objinfoCompbeneficiario.origen = aseguradoOriginal.AccountSource;
        objinfoCompbeneficiario.pais = aseguradoOriginal.BillingCountry;
        objinfoCompbeneficiario.profesion = aseguradoOriginal.Profesion__c;
        objinfoCompbeneficiario.rfc = aseguradoOriginal.RFC__c;
        objinfoCompbeneficiario.sexoDelConductor = aseguradoOriginal.Genero__c;
        objinfoCompbeneficiario.fechaNacimiento = String.isNotBlank(String.valueOf(aseguradoOriginal.PersonBirthdate)) ? String.valueOf(aseguradoOriginal.PersonBirthdate) : stringEmpty;
        return objinfoCompbeneficiario;
    }
    
    private static void upsertCotiza3( reqCotizacion cotizacionret,String sIdContratanteret, Account cteContratanteret, Account aseguradoOriginalret, infoCompbeneficiario objinfoCompbeneficiarioret){
        Account aseguradoOriginal = aseguradoOriginalret;
        final reqCotizacion cotizacion = cotizacionret;
        String sIdContratante=sIdContratanteret;
        Account cteContratante=cteContratanteret;
        final infoCompbeneficiario objinfoCompbeneficiario=objinfoCompbeneficiarioret; 
        final String sEmailCliente;
        if ( cotizacion.estatus.Equals(System.Label.MX_SB_VTS_FORMALIZADA_LBL) ) {
            for (Account cliente : [SELECT id, Correo_Electronico__c FROM Account WHERE id =: cotizacion.cliente ] ) {
                sEmailCliente = cliente.Correo_Electronico__c;
            }
        }
        if ( String.isBlank( sEmailCliente ) ) {
            sIdContratante = cotizacion.cliente;
            for ( Account cliente : [ SELECT id, Name, Correo_Electronico__c, isPersonAccount, PersonBirthdate, FirstName, LastName, PHONE,
                                     edad__c, Genero__c, RFC__c, Nacionalidad__c, Profesion__c, Colonia__c, BillingPostalCode,
                                     Numero_Exterior__c, Numero_Interior__c, AccountSource, BillingCity, BillingState, BillingCountry,
                                     BillingStreet, Delegacion__c FROM Account WHERE id =: cotizacion.cliente ] ) {
                                         sIdContratante = cliente.id;
                                         cteContratante = new Account( id = cliente.id );
                                         aseguradoOriginal = cliente;
                                     }
        } else {
            for ( Account cliente : [ SELECT id, Name, Correo_Electronico__c, isPersonAccount, PersonBirthdate, FirstName, LastName, PHONE,
                                     edad__c, Genero__c, RFC__c, Nacionalidad__c, Profesion__c, Colonia__c, BillingPostalCode,
                                     Numero_Exterior__c, Numero_Interior__c, AccountSource, BillingCity, BillingState, BillingCountry,
                                     BillingStreet, Delegacion__c FROM Account WHERE Correo_Electronico__c =: sEmailCliente ] ) {
                                         sIdContratante = cliente.id;
                                         cteContratante = new Account( id = cliente.id );
                                         aseguradoOriginal = cliente;
                                     }
        }
        cotizacion.nombreDelContratante = aseguradoOriginal.FirstName;
        cotizacion.apellidoPaternoContratante = aseguradoOriginal.LastName;
        cotizacion.apellidoMaternoContratante = stringEmpty;
        cotizacion.correoElectronicoContratante = String.isNotBlank(aseguradoOriginal.Correo_Electronico__c) ? aseguradoOriginal.Correo_Electronico__c : stringEmpty;
        cotizacion.telefonoCelularContratante = aseguradoOriginal.Phone;
        objinfoCompbeneficiario = new infoCompbeneficiario();                
        objinfoCompbeneficiario.fechaNacimiento = String.isNotBlank(String.valueOf( aseguradoOriginal.PersonBirthdate ) ) ? String.valueOf( aseguradoOriginal.PersonBirthdate ) : stringEmpty;                
        objinfoCompbeneficiario.edad = String.isNotBlank(String.valueOf( aseguradoOriginal.edad__c ) ) ? String.valueOf( aseguradoOriginal.edad__c ) : stringEmpty;
        objinfoCompbeneficiario.sexoDelConductor = aseguradoOriginal.Genero__c;
        objinfoCompbeneficiario.estado = aseguradoOriginal.BillingState;
        objinfoCompbeneficiario.pais = aseguradoOriginal.BillingCountry;
        objinfoCompbeneficiario.delegacion = aseguradoOriginal.Delegacion__c;
        objinfoCompbeneficiario.rfc = aseguradoOriginal.RFC__c;
        objinfoCompbeneficiario.nacionalidad = aseguradoOriginal.Nacionalidad__c;
        objinfoCompbeneficiario.profesion = aseguradoOriginal.Profesion__c;               
        objinfoCompbeneficiario.colonia = aseguradoOriginal.Colonia__c;
        objinfoCompbeneficiario.calleOAvenida = aseguradoOriginal.BillingStreet;
        objinfoCompbeneficiario.codigoPostal = aseguradoOriginal.BillingPostalCode;
        objinfoCompbeneficiario.origen = aseguradoOriginal.AccountSource;
        objinfoCompbeneficiario.ciudad = aseguradoOriginal.BillingCity;
        objinfoCompbeneficiario.numeroExterior = aseguradoOriginal.Numero_Exterior__c;
        objinfoCompbeneficiario.numeroInterior = aseguradoOriginal.Numero_Interior__c;
        Idglobalcontratante=sIdContratante;
    }
    
    private static void orderIf(reqCotizacion cotizacion,List<resSFDC> lrSFDC,resSFDC rSFDC,String sIdContratante, Account cteContratante, Account aseguradoOriginal, infoCompbeneficiario objinfoCompbeneficiario,Boolean bActDatosCte,Account Asegurado){
        if ( System.label.MX_SB_VTS_NO_LBL.equals(cotizacion.laPersonaQueAdquiereLaPolizaEsElContratante) ) { 
            upsertCotiza2(cotizacion,sIdContratante,cteContratante,aseguradoOriginal,objinfoCompbeneficiario,bActDatosCte, Asegurado);  
        } 
        else if ( cotizacion.laPersonaQueAdquiereLaPolizaEsElContratante.Equals(System.Label.MX_SB_VTS_SI_LBL)) {       
            upsertCotiza3(cotizacion,sIdContratante,cteContratante,aseguradoOriginal,objinfoCompbeneficiario);
        }
    }
    
    public static List<resSFDC> upsertCotiza( reqCotizacion cotizacion){
        final List<resSFDC> lrSFDC = new List<resSFDC>(); 
        final resSFDC rSFDC = new resSFDC();
        rSFDC.error = System.Label.MX_SB_VTS_ERRORUpsert_LBL;
        String idOpp = null;
        String sMsn = '';
        Boolean blRta = true;
        Boolean bActDatosCte = true;
        String sIdContratante;
        final Account cteContratante = new Account();
        final Account Asegurado = new Account();
        final Account aseguradoOriginal = new Account();
        final infoCompbeneficiario objinfoCompbeneficiario;
        try {
            sMsn = System.Label.MX_SB_VTS_MSG1rtwCotizacion_LBL + cotizacion + '. ';
            orderIf(cotizacion,lrSFDC,rSFDC,sIdContratante,cteContratante,aseguradoOriginal,objinfoCompbeneficiario,bActDatosCte,Asegurado);
            sIdContratante=Idglobalcontratante;
            final List<Opportunity> lstOpp = [ SELECT Id, AccountId,Account.Name, LeadSource FROM Opportunity WHERE FolioCotizacion__c =: cotizacion.folioDeCotizacion OR id =: cotizacion.idOportunidadComercial ];
            idOpp = rtwCotizacion.fnUpsertOportunidad ( cotizacion, ( lstOpp.size() > 0 )?lstOpp[0].Id:idnull, sIdContratante, objinfoCompbeneficiario,cotizacion.producto);          
            if ( idOpp.contains( '-' ) ) {
                rSFDC.message = System.Label.MX_SB_VTS_ErrorCotizacion_LBL + idOpp.substring( idOpp.indexOf( '-' ) + 1, idOpp.length() );
                sMsn += rSFDC.message + ' : ' + idOpp.substring( idOpp.indexOf( '-' )+1, idOpp.length() );
                rSFDC.id = idOpp.substring( 0, idOpp.indexOf( '-' ) );
            } else {
                rSFDC.message = System.Label.MX_SB_VTS_CotizadorExito_LBL+idOpp;
                sMsn += rSFDC.message;
                rSFDC.id = idOpp;
            }
            lrSFDC.add( rSFDC );
        } catch( Exception ex ) {
            rSFDC.message = System.Label.MX_SB_VTS_MSGERROR2_LBL + ex.getMessage() + System.Label.MX_SB_VTS_CAUSA_LBL + ex.getCause() + System.Label.MX_SB_VTS_LINEA_LBL + ex.getLineNumber();
            sMsn += rSFDC.message;
            blRta = false;
            rSFDC.id = idnull;
            lrSFDC.add( rSFDC );
        } 
        WB_CrearLog_cls.fnCrearLog ( sMsn, System.Label.MX_SB_VTS_RTWCotizacion_LBL, blRta );
        return lrSFDC;
    }
    
    
    
    
    
    
}