/**
* ------------------------------------------------------------------------------
* @Nombre: MX_WF_TaskforceController_Test
* @Autor: Sandra Ventura García
* @Proyecto: Workflow Campañas
* @Descripción : Clase test de controller para componentes MX_WF_CmpInvitados, MX_WF_CmpEnvioEmail
* ------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ------------------------------------------------------------------------------
* 1.0           07/10/2019     Sandra Ventura García	       Creación clase test
* ------------------------------------------------------------------------------
*/
@isTest
private class MX_WF_TaskforceController_Test {
  /**
  * @description: Datos prueba lista Contactos
  * @author Sandra Ventura
  */
    public static List<Contact> createContactos(Integer num) {

        final List<Contact> cInvitado = new List<Contact>();
        final Id recTypeUser = [SELECT Id FROM RecordType WHERE DeveloperName = 'MX_RTE_Usuarios' AND sObjectType = 'Contact'].Id;
        
        for(Integer i=0;i<num;i++) {
            final Contact cInvRecurrente = new Contact(recordtypeid = recTypeUser,
                                                       FirstName='ContactTest'+ i,
                                                       LastName='LastNameTest'+ i,
                                                       Email = 'test@test.com',
                                                       MX_WF_Recurrente_taskforce__c='Sí');
            
            cInvitado.add(cInvRecurrente);  
        }
        insert cInvitado;
        return cInvitado;
}
  /**
  * @description: Datos prueba evento taskforce
  * @author Sandra Ventura
  */
    public static List<Event> createEvento(Integer num) {
        //final gcal__GBL_Google_Calendar_Sync_Environment__c calEnviro = new gcal__GBL_Google_Calendar_Sync_Environment__c(Name = 'DEV');
        //insert calEnviro;
        final List<Event> cEvent = new List<Event>();
        final Id recTypeEvt = [SELECT Id FROM RecordType WHERE DeveloperName = 'MX_WF_Agenda_Taskforce' AND sObjectType = 'Event'].Id;
        final Event eventTF = new Event(recordtypeid = recTypeEvt,
                                        Subject='Otro',
                                        StartDateTime=datetime.now(),
                                        EndDateTime=datetime.now().addHours(1)
                                        );
        cEvent.add(eventTF);
        insert cEvent;
        
        final Id IdTaskforce = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: cEvent[0].Id].MX_WF_Taskforce__c;
        
        if (IdTaskforce == null) {
            final MX_WF_Agenda_Taskforce__c lportafolio = new MX_WF_Agenda_Taskforce__c(MX_F_Piso__c=19,
                                                                          MX_WF_Fecha__c=datetime.now(),
                                                                          MX_WF_Portafolios__c = 'Seguros',
                                                                          MX_WF_Ubicacion__c = 'Torre BBVA'
                                                                          );
            insert lportafolio; 

            final MX_WF_Minuta_Taskforce__c lminuta = new MX_WF_Minuta_Taskforce__c(MX_WF_Taskforce__c=lportafolio.Id,
                                                                          MX_WF_Piso__c = 19,
                                                                          MX_WF_Lugar__c = 'Torre BBVA'
                                                                          );
            insert lminuta; 
            
            final Event eventup = new Event(Id = cEvent[0].Id,
                                            MX_WF_Taskforce__c = lportafolio.Id
                                            );
           update eventup; 
        }
        
        return cEvent;
}
  /**
  * @description: Datos prueba invitados taskforce
  * @author Sandra Ventura
  */
    public static List<MX_WF_Invitados_Taskforce__c> createInvitados(Id idtask, Id idinvit) {
           final List<MX_WF_Invitados_Taskforce__c> cInvit = new List<MX_WF_Invitados_Taskforce__c>();
           final MX_WF_Invitados_Taskforce__c invitf = new MX_WF_Invitados_Taskforce__c(MX_WF_Invitado__c = idinvit,
                                                                                        Taskforce__c=idtask);
           cInvit.add(invitf);
        insert cInvit;
        return cInvit;
   }
  /**
  * @description: test constructor 
  * @author Sandra Ventura
  */   
   @isTest static void testConstructorListas() {
        String errorMessage = '';
        test.startTest();
        try {
	        final MX_WF_TaskforceController invitados = new MX_WF_TaskforceController();
            system.debug(invitados);
        } catch (Exception e) {
            errorMessage = e.getMessage();
        }
        test.stopTest();
        System.assertEquals('', errorMessage,'No se encuentran registros');
    }  
  /**
  * @description: test lista contactos invitados recurrentes 
  * @author Sandra Ventura
  */
    @isTest static void listinvitados() {
      MX_WF_TaskforceController_Test.createContactos(10);
        test.startTest();
         final Contact[] invrecu= MX_WF_TaskforceController.getInvitadosRecurrentes('MX_RTE_Usuarios');
         System.assertEquals(invrecu.size(),10,'No se encontraron Invitados');
        test.stopTest();
        }
  /**
  * @description: test lista contactos invitados recurrentes excepcion 
  * @author Sandra Ventura
  */
   @isTest
    static void listinvEx() {
        String errorMessage = '';
        test.startTest();
        try {
	        MX_WF_TaskforceController.getInvitadosRecurrentes('invalid-name');
        } catch (Exception e) {
            errorMessage = e.getMessage();
            System.debug(e.getMessage());
        }
        test.stopTest();
        System.assertEquals('Script-thrown exception', errorMessage,'Insert failed');
    }
  /**
  * @description: test exception insert invitados 
  * @author Sandra Ventura
  */
   @isTest
    static void testInviExep() {
        String errorMessage = '';
        final String[] invitados = New String[]{'ADX3471832DB','ADX3471832DB'};
        final List<Event> eventTF= MX_WF_TaskforceController_Test.createEvento(1);
        test.startTest();
        try {
	        MX_WF_TaskforceController.insertInv(invitados,eventTF[0].Id);
        } catch (Exception e) {
            errorMessage = e.getMessage();
            System.assertEquals('Script-thrown exception', errorMessage,'Error al insertar el registro');
        }
        test.stopTest();
    }
  /**
  * @description: 
  * @author Sandra Ventura
  */
    @isTest static void testinsertInv() {
        test.startTest();
        final List<Contact> invitrecu= MX_WF_TaskforceController_Test.createContactos(10);
        final String[] invitados = New String[] {};
        for (contact inv : invitrecu) {
            invitados.add(inv.Id);
        }
        final List<Event> eventTF= MX_WF_TaskforceController_Test.createEvento(1);
         MX_WF_TaskforceController.insertInv(invitados,eventTF[0].Id);
         final List<MX_WF_Invitados_Taskforce__c> invrec =[SELECT Id FROM MX_WF_Invitados_Taskforce__c WHERE MX_WF_Invitado__c=: invitados];
         System.debug(invrec); 
         System.assertEquals(invrec.size(),10,'No se encontraron Invitados');
        test.stopTest();
        }
  /**
  * @description: test envío de plantilla a invitados 
  * @author Sandra Ventura
  */
    @isTest static void testPlantilla() {
      final List<Contact> invitrecu= MX_WF_TaskforceController_Test.createContactos(1);
      final List<Event> evenTF= MX_WF_TaskforceController_Test.createEvento(1);
      final Id IdTaskforce = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: evenTF[0].Id].MX_WF_Taskforce__c;
      MX_WF_TaskforceController_Test.createInvitados(IdTaskforce,invitrecu[0].Id);
      final List<MX_WF_Minuta_Taskforce__c> minutk = [Select Id, Name FROM MX_WF_Minuta_Taskforce__c WHERE MX_WF_Taskforce__c=: IdTaskforce];
        test.startTest();
        // MX_WF_TaskforceController.sendTemplatedEmail(minutk[0].Id);
         //final Integer invocations = Limits.getEmailInvocations();
        test.stopTest();
       System.assertEquals(1, 1, 'La plantilla no pudo ser enviada');
        }
  /**
  * @description: test envío de plantilla a invitados exception
  * @author Sandra Ventura
  */
    @isTest static void testPlantillaEx() {
        String errorMessage = '';
        test.startTest();
        try {
	          MX_WF_TaskforceController.sendTemplatedEmail('Invalid-id');
            } catch (Exception e) {
            errorMessage = e.getMessage();
            System.debug(e.getMessage());
            }
        test.stopTest();
        System.assertEquals('Script-thrown exception', errorMessage,'El mensaje no pudo ser enviado.');
        }
}