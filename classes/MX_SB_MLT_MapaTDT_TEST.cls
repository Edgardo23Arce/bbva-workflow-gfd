/*
----------------------------------------------------------
* Nombre: MX_SB_MLT_MapaTDT_TEST
* Autor Oscar Martínez
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Clase que prueba los Methods de la clase MX_SB_MLT_MapaTDT_Cls

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Desripción<p />
* --------------------------------------------------------------------------------
* 1.0           08/05/2019     Oscar Martínez                       Creación
* 1.1           15/05/2019     Daniel Goncalves Vivas               Corrección acentos y typos
* 1.2           16/05/2019     Saúl González Reséndiz               Corrección de comentarios sonar
* --------------------------------------------------------------------------------
*/

@isTest
private class MX_SB_MLT_MapaTDT_TEST {

    static testMethod void datosMapaNoVacia() {
        final Account accRec = MX_WB_TestData_cls.crearCuenta ( 'LastName', 'MX_WB_rt_PAcc_Telemarketing' );
        insert accRec;
        final Case caso = new Case(Status='New',Origin='Phone',Priority='Medium',AccountId=accRec.Id,MX_SB_MLT_URLLocation__c='19.305819,-99.2115422');
        insert caso;
        final List<Case> lstCase = MX_SB_MLT_MapaTDT_Cls.datosMapa(caso.Id);
        final String strStatus = lstCase.get(0).Origin;
        System.assertEquals(strStatus, 'Phone','lista de casos');
    }

    static testMethod void datosMapaCorrectos() {
        final Account accRec = MX_WB_TestData_cls.crearCuenta ( 'LastName', 'MX_WB_rt_PAcc_Telemarketing' );
        insert accRec;
        final Case caso = new Case(Status='new',Origin='phone',Priority='Medium',AccountId=accRec.Id,MX_SB_MLT_URLLocation__c='19.4413849,-99.1861152');
        insert caso;
        final List<Case> lstCase = MX_SB_MLT_MapaTDT_Cls.datosMapa(caso.Id);
        System.assertEquals(lstCase.get(0).MX_SB_MLT_URLLocation__c,'19.4413849,-99.1861152','geolocalización correcta');
       
    }

    static testMethod void datosMapaException() {
        final Account accRec = MX_WB_TestData_cls.crearCuenta ( 'LastName', 'MX_WB_rt_PAcc_Telemarketing' );
        insert accRec;
        final Case caso = new Case(Status='new',Origin='phone',Priority='Medium',AccountId=accRec.Id,MX_SB_MLT_URLLocation__c='99.3123123,-19.45345');
        insert caso;
        try {
            MX_SB_MLT_MapaTDT_Cls.datosMapa(accRec.Id);
        } catch(Exception  e) {
            System.assertEquals('Script-thrown exception', e.getMessage(),'Excepción capturada.');
        }
    }
}