/**
* ------------------------------------------------------------------------------
* @Nombre: MX_WF_TaskforceUtility_DataTest
* @Autor: Sandra Ventura García
* @Proyecto: Workflow Campañas
* @Descripción : clase para generar datos de prueba workflow campañas - taskforce
* ------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ------------------------------------------------------------------------------
* 1.0           07/10/2019     Sandra Ventura García	       Creación de clase
* ------------------------------------------------------------------------------
*/
@isTest
public class MX_WF_Utility_DataTest {
  /**
  * @description: Datos prueba lista Contactos
  * @author Sandra Ventura
  */
    public static List<Contact> createContactos(Integer num) {

        final List<Contact> cInvitado = new List<Contact>();
        final Id recTypeUser = [SELECT Id FROM RecordType WHERE DeveloperName = 'MX_RTE_Usuarios' AND sObjectType = 'Contact'].Id;
        
        for(Integer i=0;i<num;i++) {
            final Contact cInvRecurrente = new Contact(recordtypeid = recTypeUser,
                                                       FirstName='ContactTest'+ i,
                                                       LastName='LastNameTest'+ i,
                                                       Email = 'test@test.com',
                                                       MX_WF_Recurrente_taskforce__c='Sí');
            
            cInvitado.add(cInvRecurrente);  
        }
        insert cInvitado;
        return cInvitado;
}
  /**
  * @description: Datos prueba evento taskforce
  * @author Sandra Ventura
  */
    public static Event createEvento(Integer num) {
       //final gcal__GBL_Google_Calendar_Sync_Environment__c calEnviro = new gcal__GBL_Google_Calendar_Sync_Environment__c(Name = 'DEV');
       //insert calEnviro;
        final Id recTypeEvt = [SELECT Id FROM RecordType WHERE DeveloperName = 'MX_WF_Agenda_Taskforce' AND sObjectType = 'Event'].Id;
		    Event eventTF              = new Event();
            eventTF.recordtypeid       = recTypeEvt;
            eventTF.MX_WF_Tipo_de_evento__c='Portafolio';
            eventTF.MX_WF_Portafolios__c ='Seguros';
		    eventTF.StartDateTime      = datetime.now().adddays(num);
            eventTF.EndDateTime        = datetime.now().adddays(num).addHours(num);
            eventTF.Subject='Otros';
		    eventTF.MX_WF_Ubicacion__c = 'Torre BBVA';
            eventTF.MX_WF_Piso__c      = 19;
        insert eventTF;
    	return eventTF;
    }
  /**
  * @description: Datos prueba invitados taskforce
  * @author Sandra Ventura
  */
    public static List<MX_WF_Invitados_Taskforce__c> createInvitados(Id idtask, Id idinvit) {
           final List<MX_WF_Invitados_Taskforce__c> cInvit = new List<MX_WF_Invitados_Taskforce__c>();
           final MX_WF_Invitados_Taskforce__c invitf = new MX_WF_Invitados_Taskforce__c(MX_WF_Invitado__c = idinvit,
                                                                                        Taskforce__c=idtask);
           cInvit.add(invitf);
        insert cInvit;
        return cInvit;
   }
  /**
  * @description: Datos prueba lista temas
  * @author Sandra Ventura
  */
    public static List<MX_WF_Tema__c> createTemas(Integer num, Id idportafolio) {

        final List<MX_WF_Tema__c> ctema = new List<MX_WF_Tema__c>();
        final Time initime = Time.newInstance(8, 30, 0, 0);
        for(Integer i=0;i<num;i++) {
            final MX_WF_Tema__c temalist = new MX_WF_Tema__c(Name = 'Tema '+i,
                                                             MX_WF_Tiempo_Minutos__c = 30,
                                                             MX_WF_Hora_inicio_Tema__c= initime.addMinutes(30),
                                                             MX_WF_Taskforce__c = idportafolio
                                                             );
            
            ctema.add(temalist);
        }
        insert ctema;
        return ctema;
}
}