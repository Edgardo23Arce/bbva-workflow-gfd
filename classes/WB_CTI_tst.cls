@isTest (SeeAllData=true)
private class WB_CTI_tst {

    @isTest(seeAllData = true)
	static void test_method_one() {

		Test.startTest();

		    //String VaRtCaseInfo = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Información').getRecordTypeId();
		    //String VaRtOppVenta = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Ventas Wibe').getRecordTypeId();
			final String VaRtOppVenta = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ASD').getRecordTypeId();

			final String IdAgente = UserInfo.getUserId();

			final Opportunity entOpp = new Opportunity();

		    //Crea el cliente
			final List<Account> lCliente = TestCreaObjetos_cls.CreaClienteC(1, null);
			//La lista para los Opportunity
			final List<Opportunity> lOportunidad = TestCreaObjetos_cls.CreaOportunidad(1, lCliente.get(0).id, VaRtOppVenta
				, 'Emitida');

			final Lead entLead = new Lead();
				entLead.FirstName = 'Test';
				entLead.LastName = 'Test';
				entLead.Apellido_Materno__c = 'Auto wibe';
				entLead.Producto_Interes__c = '';
				entLead.Status = 'Abierto';
				entLead.LeadSource = 'Call me back';
				entLead.Priority__c = '0';
				entLead.Hora_contacto__c = Datetime.now();
				entLead.TelefonoCelular__c = '5555555555';
        		entLead.Email = 'test@test.com';
			Insert entLead;
			entLead.Priority__c = '1';
			update entLead;
			entLead.Priority__c = '0';
			update entLead;

			final String sXML = WB_CTI.GenerarXML('5555555555', 'Wibe Test', '', String.valueOf(entLead.Id), String.valueOf(IdAgente),'000',0);
			WB_CTI.GenerarXML('5555555555', 'Wibe Test', '', String.valueOf(entLead.Id), String.valueOf(IdAgente),'000',1);
			WB_CTI.reqSolicitud(sXML, entLead.Id,'Lead');

        	//Llama al metodo de GenerarXML con otros parametros
			WB_CTI.GenerarXML('5555555555', 'Wibe Test', 'Auto Seguro Dinamico', String.valueOf(entLead.Id), String.valueOf(IdAgente),'000',0);

        	//Llama al metodo de GenerarXML con otros parametros
			WB_CTI.GenerarXML('5555555555', 'Wibe Test', 'Chofer Privado', String.valueOf(entLead.Id), String.valueOf(IdAgente),'000',1);

        	//Llama al metodo de GenerarXML con otros parametros
			WB_CTI.GenerarXML('5555555555', 'Wibe Test', 'Auto Seguro Dinamico', String.valueOf(entLead.Id), String.valueOf(IdAgente),'000',1);

        	//Llama al metodo de GenerarXML con otros parametros
			WB_CTI.GenerarXML('5555555555', 'Wibe Test', 'Seguro de Moto UDI', String.valueOf(entLead.Id), String.valueOf(IdAgente),'000',1);

			//Llama al metodo de GenerarXML con otros parametros
			WB_CTI.GenerarXML('5555555555', 'Wibe Test', 'AUTOS UDI', String.valueOf(entLead.Id), String.valueOf(IdAgente),'000',1);

            //Llama al metodo de GenerarXML con otros parametros
			WB_CTI.GenerarXML('5555555555', 'Wibe Test', 'Seguro Fronterizo', String.valueOf(entLead.Id), String.valueOf(IdAgente),'000',1);


			//Llama a ProcesaSol
			WB_CTI.ftProcesaSol(lOportunidad.get(0).id, lOportunidad.get(0).FolioCotizacion__c, 'Seguro de Auto', IdAgente,
				'Prueba', '5512345678', 'Opportunity', 1);

			//Llama a ProcesaSol
			WB_CTI.ProcesaSol(lOportunidad.get(0).id, lOportunidad.get(0).FolioCotizacion__c, 'Seguro de Auto', IdAgente,
				'Prueba', '5512345678', 'Opportunity', 1);

        	//WB_CTI.manualShareRead(lOportunidad.get(0).id, lOportunidad.get(0).id);
		system.assertNotEquals(lCliente, null);
		Test.stopTest();
	}

}