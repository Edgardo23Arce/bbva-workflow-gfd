/*
----------------------------------------------------------
* Nombre: MX_SB_MLT_MapaTDT_Cls
* Autor Oscar Martínez
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Clase que recupera los datos para mostrar el mapa en la tarea de datos.

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   			Desripción<p />
* --------------------------------------------------------------------------------
* 1.0           25/04/2019     Oscar Martínez           		   	Creación
* --------------------------------------------------------------------------------
*/

public with sharing class MX_SB_MLT_MapaTDT_Cls {
    
private MX_SB_MLT_MapaTDT_Cls () {}
     
     @AuraEnabled
     public static List<Case> datosMapa (String idCase) {
     	List<case> datosCase =  new List<Case>();
          try {
          	datosCase = MX_SB_MLT_TareaDatosUtils_Cls.obtieneCase(idCase);
          } catch ( CustomException ce ) {
               throw new AuraHandledException ( System.Label.MX_WB_LG_ErrorBack + ce);
          }
          return datosCase;
     }
}