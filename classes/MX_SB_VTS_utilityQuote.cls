/**-------------------------------------------------------------------------
* Name: Utility Quote Class
* @author Julio Medellin
* Proyect: MW SB VTS - BBVA
* Description : Utility class for the Quotes
* --------------------------------------------------------------------------
*                         Date           Author                   Description
* -------------------------------------------------------------------
* @version 1.0           May/22/2019      Julio Medellin            Header
* @version 1.1           May/27/2019      Jaime Terrats         Add new method to get pricebook entry id
* @version 1.1.1         Jun/06/2019      Eduardo Hernández     Fix para cotización Pricebook2Id
*--------------------------------------------------------------------------------**/

/**
* @description: update quote for synchronization
*/
public without sharing class MX_SB_VTS_utilityQuote {
    private  MX_SB_VTS_utilityQuote(){}//NOSONAR

    public static string prepareQuoteSync(reqCotizacion requestQuoting) {
        //variable to specifiy if found an opportunity
        string sOpportunityId;
        //variable to specifiy if update an opportunity
        boolean updateOpp=false;
        Quote quoting = new Quote();
        for(Quote quot : [SELECT Id, OpportunityId, Opportunity.LeadSource, Opportunity.Account.Name FROM QUOTE WHERE MX_SB_VTS_Folio_Cotizacion__c =: requestQuoting.folioDeCotizacion]) {
            quoting = quot;
            sOpportunityId = quot.OpportunityId;
            updateOpp = true;
        }
        if(updateOpp) {
           Opportunity opp =[SELECT Id,SyncedQuoteId FROM Opportunity WHERE Id=:sOpportunityId];
           opp.SyncedQuoteId=quoting.Id;
		   List<sObject> updObjects = new List<sObject>();
           updObjects.add(opp);
           updObjects.add(quoting);
           update updObjects;
        }

        return  sOpportunityId;
    }

    /*
    * Method to sync quote line item with opportunity line item
    * @param datosCotizacion
    */
	public static string prepareQuoteHogarSync(MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosCotizacion datosCotizacion) {
        //variable to specifiy if found an opportunity
        string sOpportunityId;
        //variable to specifiy if update an opportunity
        boolean updateOpp=false;
        Quote quoting = new Quote();
        for(Quote quot : [SELECT Id, OpportunityId, Opportunity.LeadSource, Opportunity.Account.Name
                          FROM QUOTE WHERE MX_SB_VTS_Folio_Cotizacion__c =: datosCotizacion.datosIniciales.folioCotizacion]) {
            quoting = quot;
            sOpportunityId = quot.OpportunityId;

            updateOpp = true;
        }
        if(updateOpp) {
           Opportunity opp =[SELECT Id,SyncedQuoteId FROM Opportunity WHERE Id=:sOpportunityId];
           opp.SyncedQuoteId=quoting.Id;
           update opp;
           update quoting;
        }
        return  sOpportunityId;
    }

    /**
     * getPricebookEntry Obtine el precio
     * @param	presupuesto	Busca el presupuesto
     * @param	quoli	Id del quoli
     * @param	datosCotizacion	Datos de la cotización
     * @return	PriceBookEntryId	Regresa Id
    */
    public static Id getPricebookEntry(Quote presupuesto, QuoteLineItem quoli, List<String> datosCotizacion) {
        Boolean hasPriceBook = true;
        Id pbe1;
        try {
            for ( PricebookEntry pbe : [select Id,Pricebook2Id FROM PricebookEntry where Product2Id =: datosCotizacion[0] ] ) {
                quoli.PricebookEntryId = pbe.Id;
                presupuesto.Pricebook2Id = pbe.Pricebook2Id;
                pbe1 = pbe.Id;
                hasPriceBook = false;
            }
        } catch(QueryException qEx) {
            throw new QueryException('Error: ' + qEx);
        }

        if(hasPriceBook && String.isNotBlank(datosCotizacion[1])) {
            final Product2 product = [select ID, Name from Product2 where Id =: datosCotizacion[0]];
            final PricebookEntry pbe = MX_WB_OppoLineItemUtil_cls.crearPricebookEntry(product.ID, Decimal.valueOf(datosCotizacion[1]));
            try {
                Database.upsert(pbe);
            	quoli.PriceBookEntryId = pbe.Id;
                presupuesto.Pricebook2Id = pbe.Pricebook2Id;
                pbe1 = pbe.Id;
            } catch(DmlException dmlEx) {
                throw new DmlException('Fail: '+ dmlEx);
            }
            quoli.PriceBookEntryId = pbe.Id;
        }
        final Quote quotePB = [select Id, Pricebook2Id from Quote where Id =: presupuesto.Id ];
        quotePB.Pricebook2Id = presupuesto.Pricebook2Id;
        try {
            Database.update(quotePB);
        } catch(DmlException dmlEx) {
            throw new DmlException('Failed: '+ dmlEx);
        }
        return pbe1;
    }

    /*
    * Returns a boolean value to validate input
    * @params value: incoming string from api
    */
    public static Boolean checkBooleanValues(Boolean value) {
        return String.isNotBlank(String.valueOf(value)) ? true : false;
    }
}