/**
* ------------------------------------------------------------------------------
* @Nombre: MX_WF_AddDeleteController
* @Autor: Sandra Ventura García
* @Proyecto: Workflow Campañas
* @Descripción : Clase test de controller para componentes MX_WF_TablaDinamicaPadre
* ------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ------------------------------------------------------------------------------
* 1.0           10/11/2019     Sandra Ventura García	       Creación
* ------------------------------------------------------------------------------
*/
@isTest
public with sharing class MX_WF_AddDeleteController_Test {
  /**
  * @description: Datos prueba evento taskforce
  * @author Sandra Ventura
  */
    public static List<Event> createEvento(Integer num) {
        //final gcal__GBL_Google_Calendar_Sync_Environment__c calEnviro = new gcal__GBL_Google_Calendar_Sync_Environment__c(Name = 'DEV');
        //insert calEnviro;
        final List<Event> cEvent = new List<Event>();
        final Id recTypeEvt = [SELECT Id FROM RecordType WHERE DeveloperName = 'MX_WF_Agenda_Taskforce' AND sObjectType = 'Event'].Id;
        final Event eventTF = new Event(recordtypeid = recTypeEvt,
                                        Subject='Taskforce',
                                        StartDateTime=datetime.now(),
                                        EndDateTime=datetime.now().addHours(1)
                                        );
        cEvent.add(eventTF);
        insert cEvent;
        
        final Id IdTaskforce = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: cEvent[0].Id].MX_WF_Taskforce__c;        
        if (IdTaskforce == null) {
            final MX_WF_Agenda_Taskforce__c lportafolio = new MX_WF_Agenda_Taskforce__c(MX_F_Piso__c=19,
                                                                          MX_WF_Fecha__c=datetime.now(),
                                                                          MX_WF_Portafolios__c = 'Seguros',
                                                                          MX_WF_Ubicacion__c = 'Torre BBVA'
                                                                          );
            insert lportafolio;
            final MX_WF_Minuta_Taskforce__c lminuta = new MX_WF_Minuta_Taskforce__c(MX_WF_Taskforce__c=lportafolio.Id,
                                                                          MX_WF_Piso__c = 19,
                                                                          MX_WF_Lugar__c = 'Torre BBVA'
                                                                          );
            insert lminuta;
            final Event eventup = new Event(Id = cEvent[0].Id,
                                            MX_WF_Taskforce__c = lportafolio.Id
                                            );
           update eventup;
        }
        return cEvent;
}
  /**
  * @description: Datos prueba lista temas
  * @author Sandra Ventura
  */
    public static List<MX_WF_Tema__c> createTemas(Integer num, Id idportafolio) {

        final List<MX_WF_Tema__c> ctema = new List<MX_WF_Tema__c>();
        final Time initime = Time.newInstance(8, 30, 0, 0);
        for(Integer i=0;i<num;i++) {
            final MX_WF_Tema__c temalist = new MX_WF_Tema__c(Name = 'Tema '+i,
                                                             MX_WF_Tiempo_Minutos__c = 30,
                                                             MX_WF_Hora_inicio_Tema__c= initime.addMinutes(30),
                                                             MX_WF_Taskforce__c = idportafolio
                                                             );
            
            ctema.add(temalist);
        }
        insert ctema;
        return ctema;
}
  /**
  * @description: Datos prueba invitados taskforce
  * @author Sandra Ventura
  */
    public static List<MX_WF_Invitados_Taskforce__c> createInvitados(Id idtask) {
        
        final List<Contact> cInvitado = new List<Contact>();
        final Id recTypeUser = [SELECT Id FROM RecordType WHERE DeveloperName = 'MX_RTE_Usuarios' AND sObjectType = 'Contact'].Id;
        
        for(Integer i=0;i<5;i++) {
            final Contact cInvRecurrente = new Contact(recordtypeid = recTypeUser,
                                                       FirstName='ContactTest'+ i,
                                                       LastName='LastNameTest'+ i,
                                                       Email = 'test@test.com',
                                                       MX_WF_Recurrente_taskforce__c='Sí');
            
            cInvitado.add(cInvRecurrente);  
        }
        insert cInvitado;
        
           final List<MX_WF_Invitados_Taskforce__c> cInvit = new List<MX_WF_Invitados_Taskforce__c>();
           final MX_WF_Invitados_Taskforce__c invitf = new MX_WF_Invitados_Taskforce__c(MX_WF_Invitado__c = cInvitado[0].Id,
                                                                                        Taskforce__c=idtask);
           cInvit.add(invitf);
        insert cInvit;
        return cInvit;
   }
/**
  * @description: test constructor 
  * @author Sandra Ventura
  */   
   @isTest static void testConstructortemas() {
        String errorM = '';
        test.startTest();
        try {
	        final MX_WF_AddDeleteController temas = new MX_WF_AddDeleteController();
            system.debug(temas);
        } catch (Exception e) { errorM = e.getMessage(); }
        test.stopTest();
        System.assertEquals('', errorM,'No se encuentran registros');
    }
  /**
  * @description: test lista temas 
  * @author Sandra Ventura
  */
    @isTest static void listtemas() {
      final List<Event> eventTF= MX_WF_AddDeleteController_Test.createEvento(1);
        final Id IdTaskforce = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: eventTF[0].Id].MX_WF_Taskforce__c;
        MX_WF_AddDeleteController_Test.createTemas(10,IdTaskforce);
        test.startTest();
         final MX_WF_Tema__c[] listtem= MX_WF_AddDeleteController.getTemas(eventTF[0].Id);
         System.assertEquals(listtem.size(),10,'No se encontraron Temas');
        test.stopTest();
        }
  /**
  * @description: test lista contactos invitados recurrentes excepcion 
  * @author Sandra Ventura
  */
   @isTest
    static void listtemasEx() {
        String errorMessage = '';
        final List<Event> eventTF= MX_WF_AddDeleteController_Test.createEvento(1);
        final Id IdTaskforce = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: eventTF[0].Id].MX_WF_Taskforce__c;
        test.startTest();
        try {
	        MX_WF_AddDeleteController.getTemas(IdTaskforce);
        } catch (Exception e) {
            errorMessage = e.getMessage();
            System.debug(e.getMessage());
        }
        test.stopTest();
        System.assertEquals('Script-thrown exception', errorMessage,'Insert failed');
    }
  /**
  * @description: test lista temas 
  * @author Sandra Ventura
  */
    @isTest static void testsavetema() {
      final list<Event> eventTF= MX_WF_AddDeleteController_Test.createEvento(1);
      final Id idportafolio = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: eventTF[0].Id].MX_WF_Taskforce__c;
      final List<MX_WF_Tema__c> ctema = new List<MX_WF_Tema__c>();
      final Time initime = Time.newInstance(8, 30, 0, 0);
        for(Integer i=0;i<5;i++) {
            final MX_WF_Tema__c temalist = new MX_WF_Tema__c(Name = 'Tema '+i,
                                                             MX_WF_Tiempo_Minutos__c = 20,
                                                             MX_WF_Hora_inicio_Tema__c= initime.addMinutes(15),
                                                             MX_WF_Taskforce__c = idportafolio
                                                             );
            
            ctema.add(temalist);
        }
        test.startTest();
         final boolean listtem= MX_WF_AddDeleteController.saveTema(ctema, eventTF[0].Id);
         System.assert(listtem,'Save tema test');
        test.stopTest();
    }
  /**
  * @description: test lista temas 
  * @author Sandra Ventura
  */
    @isTest static void listinvitado() {
      final List<Event> evenTF= MX_WF_AddDeleteController_Test.createEvento(1);
      final Id IdTaskforce = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: evenTF[0].Id].MX_WF_Taskforce__c;
      final Id IdMinuta = [SELECT Id FROM MX_WF_Minuta_Taskforce__c WHERE MX_WF_Taskforce__c=: IdTaskforce].id;
      MX_WF_AddDeleteController_Test.createInvitados(IdTaskforce);
        test.startTest();
         final MX_WF_Invitados_Taskforce__c[] listtem= MX_WF_AddDeleteController.getInvitados(IdMinuta);
         System.assertEquals(listtem.size(),1,'No se encontraron Invitados');
        test.stopTest();
        }
  /**
  * @description: test lista invitados recurrentes asistencia 
  * @author Sandra Ventura
  */
    @isTest static void listinvitadoEx() {
      String errorMessage = '';
      final List<Event> evenTF= MX_WF_AddDeleteController_Test.createEvento(1);
        final Id IdTaskforce = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: evenTF[0].Id].MX_WF_Taskforce__c;
      MX_WF_AddDeleteController_Test.createInvitados(IdTaskforce);
        test.startTest();
        try {
             MX_WF_AddDeleteController.getInvitados(IdTaskforce);
        } catch (Exception e) {
            errorMessage = e.getMessage();
            System.debug(e.getMessage());
        }
        test.stopTest();
        System.assertEquals('Script-thrown exception', errorMessage,'List error');
    }
  /**
  * @description: test actualiza asistencia de invitados 
  * @author Sandra Ventura
  */
    @isTest static void testsaveInvitado() {
      final List<Event> evenTF= MX_WF_AddDeleteController_Test.createEvento(1);
      final Id IdTaskforce = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: evenTF[0].Id].MX_WF_Taskforce__c;
      final List<MX_WF_Invitados_Taskforce__c> listinv = MX_WF_AddDeleteController_Test.createInvitados(IdTaskforce);
      test.startTest();
       final boolean invrecurr = MX_WF_AddDeleteController.saveInvitado(listinv);
      test.stopTest();
       System.assert(invrecurr, 'Actualización asistencia');
        }
}