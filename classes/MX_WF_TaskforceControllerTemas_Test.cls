/**
* ------------------------------------------------------------------------------
* @Nombre: MX_WF_TaskforceController_Test
* @Autor: Sandra Ventura García
* @Proyecto: Workflow Campañas
* @Descripción : Clase test de controller para componentes MX_WF_CmpInvitados, MX_WF_CmpEnvioEmail
* ------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ------------------------------------------------------------------------------
* 1.0           07/10/2019     Sandra Ventura García	       Creación clase test
* ------------------------------------------------------------------------------
*/
@isTest
public class MX_WF_TaskforceControllerTemas_Test {
  /**
  * @description: test constructor 
  * @author Sandra Ventura
  */

   @isTest static void testConstructorListas() {
        String errorMessage = '';
        test.startTest();
        try {
	        final MX_WF_TaskforceController invitados = new MX_WF_TaskforceController();
            system.debug(invitados);
        } catch (Exception e) {
            errorMessage = e.getMessage();
        }
        test.stopTest();
        System.assertEquals('', errorMessage,'No se encuentran registros');
    }
  /**
  * @description: test lista contactos invitados recurrentes excepcion 
  * @author Sandra Ventura
  */
   @isTest
    static void listinvEx() {
        String errorMessage = '';
        test.startTest();
        try {
	        MX_WF_TaskforceController.getInvitadosRecurrentes('invalid-name');
        } catch (Exception e) {
            errorMessage = e.getMessage();
            System.debug(e.getMessage());
        }
        test.stopTest();
        System.assertEquals('Script-thrown exception', errorMessage,'Insert failed');
    }
  /**
  * @description: test exception insert invitados 
  * @author Sandra Ventura
  */
   @isTest
    static void testInviExep() {
        String errorMessage = '';
        final String[] invitados = New String[]{'ADX3471832DB','ADX3471832DB'};
        final Event eventTF= MX_WF_Utility_DataTest.createEvento(1);
        test.startTest();
        try {
	        MX_WF_TaskforceController.insertInv(invitados,eventTF.Id);
        } catch (Exception e) {
            errorMessage = e.getMessage();
            System.assertEquals(' Script-thrown exception', errorMessage,'Error al insertar el registro');
        }
        test.stopTest();
    }
  /**
  * @description: test envío de plantilla a invitados exception
  * @author Sandra Ventura
  */
    @isTest static void testPlantillaEx() {
        String errorMessage = '';
        test.startTest();
        try {
	          MX_WF_TaskforceController.sendTemplatedEmail('Invalid-id');
            } catch (Exception e) {
            errorMessage = e.getMessage();
            System.debug(e.getMessage());
            }
        test.stopTest();
        System.assertEquals('Script-thrown exception', errorMessage,'El mensaje no pudo ser enviado');
        }
  /**
  * @description: test lista contactos invitados recurrentes excepcion 
  * @author Sandra Ventura
  */
   @isTest
    static void listtemasEx() {
        String errorMessage = '';
        final Event eventTF= MX_WF_Utility_DataTest.createEvento(1);
        final Id IdTaskforce = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: eventTF.Id].MX_WF_Taskforce__c;
        test.startTest();
        try {
	        MX_WF_TaskforceController.getTemas(IdTaskforce);
        } catch (Exception e) {
            errorMessage = e.getMessage();
            System.debug(e.getMessage());
        }
        test.stopTest();
        System.assertEquals('Script-thrown exception', errorMessage,'Insert failed');
    }
  /**
  * @description: test lista temas 
  * @author Sandra Ventura
  */
    @isTest static void listinvitadoEx() {
      String errorMessage = '';
      final List<Contact> invitrecu= MX_WF_Utility_DataTest.createContactos(1);
      final Event evenTF= MX_WF_Utility_DataTest.createEvento(1);
      final Id IdTaskforce = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: evenTF.Id].MX_WF_Taskforce__c;
      MX_WF_Utility_DataTest.createInvitados(IdTaskforce,invitrecu[0].Id);
        test.startTest();
        try {
         final MX_WF_Invitados_Taskforce__c[] listtem= MX_WF_TaskforceController.getInvitados(IdTaskforce);
        } catch (Exception e) {
            errorMessage = e.getMessage();
            System.debug(e.getMessage());
        }
        test.stopTest();
        System.assertEquals('Script-thrown exception', errorMessage,'List error');
    }
}