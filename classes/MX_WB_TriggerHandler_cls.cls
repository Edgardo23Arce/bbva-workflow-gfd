/**-------------------------------------------------------------------------
 * Nombre: MX_WB_TriggerHandler_cls
 * Autor Ing. Karen Belem Sanchez
 * Proyecto: MW WB TeleMarketing - BBVA Bancomer
 * Descripción : Clase TriggerHandler
 * --------------------------------------------------------------------------
 * Versión 		Fecha			Autor					Descripción
 * -------------------------------------------------------------------
 * 1.0			28/11/2018		Karen Belem Sanchez 	Creación
 * 1.1			13/02/2019		Eduardo Hernández 	 	Se agrega para desactivar Lead en AfterUpdate
 * 1.1			15/02/2019		Eduardo Hernández 	 	Correción para afterUpdate, se elimna la lista de triggers Old
 * --------------------------------------------------------------------------
 */
public class MX_WB_TriggerHandler_cls  extends TriggerHandler {
    List<Campaign> tgrNewCampaign = (list<Campaign>)(Trigger.new);

    /*
    @beforeInsert event override en la Clase TriggerHandler
    Logica Encargada de los Eventos BeforeInsert */
    protected override void beforeInsert() {
        MX_WB_CampaniasNoDuplicadas.CampaniasNoDuplicadasCreaAct(tgrNewCampaign);
    }

    /*
	@afterUpdate event override en la Clase TriggerHandler
	Logica Encargada de los Eventos afterUpdate*/
	protected override void afterUpdate() {
        MX_WB_CampaniasNoDuplicadas.CampaniasNoDuplicadasCreaAct(tgrNewCampaign);
    }

}