/**
* Avanxo Colombia
* @author           Francisco Javier
* Project:          WIBE
* Description:      Clase que implementa metodos para realizar la conexion desde SFDC hacia CTI.
*
* Changes (Version)
* -------------------------------------
*     No.   Date      Author          Description
*     ----- ----------    --------------------  ---------------
* @version  1.0   2019-03-07    Francisco García García (FG)    Creación
********************************************************************************************************************************************/
public class MX_WB_CTI_cls {

    static final String sObjetoOpp, sObjetoLead;

    static{
        sObjetoOpp ='Opportunity';
        sObjetoLead = 'Lead';
    }

    /*
    *Método: método a futuro que sirve para extraer los resultados del request del servicio
    *28-01-2019
    *Karen Belem Sanchez Ruiz*/
    @future(callout=true)
    public static void ftProcesaSol(String sIdOpp, String sFolioCotizacion, String sIdProducto, String sIdOwnerId, String sNameCte, String sTelefono, String sObjeto, Integer iTipo, String sTelefono2, String sTelefono3) {
        String sXML, tel1, tel2, tel3;
        final String sNombre = String.isNotBlank(sNameCte) ? sNameCte : '' ;
        final String sFolio = String.isNotBlank(sFolioCotizacion) ? sFolioCotizacion : '' ;
        List<String> lstTelefonos = new List<String>();

        lstTelefonos = validacionTelefono(sObjeto,sTelefono,sTelefono2,sTelefono3);
        tel1 = lstTelefonos[0];
        tel2 = lstTelefonos[1];
        tel3 = lstTelefonos[2];

        sXML = MX_WB_CTI_cls.GenerarXML(tel1, sNombre, sIdProducto == null ? '' : sIdProducto, sIdOpp, sIdOwnerId, sFolio, iTipo, tel2,tel3);
        if(String.isNotBlank(sXML)) {
            MX_WB_CTI_cls.reqSolicitud(sXML, sIdOpp, sObjeto);
        }
    }

    public static List<String> ProcesaSol(String sIdOpp, String sFolioCotizacion, String sIdProducto, String sIdOwnerId, String sNameCte, String sTelefono, String sObjeto, Integer iTipo , String sTelefono2, String sTelefono3) {
        String sXML, tel1, tel2, tel3;
        final String sNombre = String.isNotBlank(sNameCte) ? sNameCte : '' ;
        final String sFolio = String.isNotBlank(sFolioCotizacion) ? sFolioCotizacion : '';
        List<String> lstTelefonos = new List<String>();
        List<String> sRequest = new List<String>();

        lstTelefonos = validacionTelefono(sObjeto,String.isNotBlank(sTelefono) ? sTelefono : '',String.isNotBlank(sTelefono2) ? sTelefono2 : '',String.isNotBlank(sTelefono3) ? sTelefono3 : '');
        tel1 = lstTelefonos[0];
        tel2 = lstTelefonos[1];
        tel3 = lstTelefonos[2];

        sXML = MX_WB_CTI_cls.GenerarXML(tel1, sNombre, String.isNotBlank(sIdProducto) ? sIdProducto : '', sIdOpp, sIdOwnerId, sFolio, iTipo, tel2,tel3);
        if(String.isNotBlank(sXML)) {
            sRequest = MX_WB_CTI_cls.reqSolicitud(sXML, sIdOpp, sObjeto);
        }
        return sRequest;
    }

    /*Método: Valida el teléfono para asignar lada
    *28-01-2019
    *Karen Belem Sanchez Ruiz*/
    public static List<String> validacionTelefono(String sObjeto, String sTelefono, String sTelefono2, String sTelefono3) {
        final List<String> telefonos = new List<String>();

        telefonos.add( (String.isNotBlank(sTelefono) && sTelefono.startsWith('55'))  ? '044' + sTelefono   : '045' + sTelefono );
        telefonos.add( (String.isNotBlank(sTelefono2) && sTelefono2.startsWith('55')) ? '044' + sTelefono2  : '045' + sTelefono2 );
        telefonos.add( (String.isNotBlank(sTelefono3) && sTelefono3.startsWith('55')) ? '044' + sTelefono3  : '045' + sTelefono3 );

        return telefonos;
    }

    /*Método: Se genera el XML
    *28-01-2019
    *Karen Belem Sanchez Ruiz*/
    public static String GenerarXML(String sTelefono,String sNombre,String sProducto, String sIdProspecto, String sAgente, String sFolio, Integer iTipo, String sTelefono2, String sTelefono3) {
        MX_WB_CredencialesCTI__c credenciales = new MX_WB_CredencialesCTI__c();
        final dom.Document doc = new dom.Document();

        final String soapXSI = 'http://www.w3.org/2001/XMLSchema-instance';
        final String soapXSD = 'http://www.w3.org/2001/XMLSchema';
        final String soapENV = 'http://schemas.xmlsoap.org/soap/envelope/';
        final String soapWS = Label.soapWS;

        final dom.Xmlnode envelope = doc.createRootElement('Envelope', soapENV, 'soapenv');
        envelope.addChildElement('Header', soapENV, 'soapenv');
        final dom.XmlNode body = envelope.addChildElement('Body', soapENV, 'soapenv');

        envelope.setNamespace('xsi', soapXSI);
        envelope.setNamespace('xsd', soapXSD);
        envelope.setNamespace('soapenv', soapENV);
        envelope.setNamespace('ws', soapWS);

        final dom.XmlNode setCall = body.addChildElement('setCall', soapWS, null);
        setCall.setAttribute('soapenv:encodingStyle','http://schemas.xmlsoap.org/soap/encoding/');
        credenciales = CredencialCTI(sProducto,iTipo);
        setCall.addChildElement('user',null,null).addTextNode(credenciales.MX_WB_Usuario__c);
        setCall.addChildElement('pass',null,null).addTextNode(credenciales.MX_WB_Contrasenia__c);
        setCall.addChildElement('phone',null,null).addTextNode(sTelefono);
        setCall.addChildElement('name',null,null).addTextNode(sNombre);
        setCall.addChildElement('product',null,null).addTextNode(String.isBlank(sProducto) ? label.wsProductoSeguroDeAuto :sProducto);
        setCall.addChildElement('leadId',null,null).addTextNode(sIdProspecto);
        setCall.addChildElement('agent',null,null).addTextNode(sAgente);
        setCall.addChildElement('callType',null,null).addTextNode('ANYONE');
        setCall.addChildElement('folio',null,null).addTextNode(sFolio);
        setCall.addChildElement('phone',null,null).addTextNode(sTelefono2);
        setCall.addChildElement('phone',null,null).addTextNode(sTelefono3);

        return Doc.toXmlString();
    }

    /*
    *Método: sirve para extraer las credenciales para hacer el envío a CTI
    *28-01-2019
    *Karen Belem Sanchez Ruiz */
    public static MX_WB_CredencialesCTI__c CredencialCTI(String nombreLista,Integer iTipo) {
        final String sName = nombreLista + iTipo;
        final MX_WB_CredencialesCTI__c ListCTI = MX_WB_CredencialesCTI__c.getValues(sName);
        return ListCTI;
    }


    /*Método: Repsuesta del servicio y actualización del lead u oportunidad
    *28-01-2019
    *Karen Belem Sanchez Ruiz*/
    public static List<String> reqSolicitud(String doc, String IdObjeto, String sObjeto) {
        String endpoint = Label.wsEndPointWibe00;
        String sXml = '';
        String sRespuesta = '';
        final List<String> lstRequest = new List<String>();

        final HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        final Http http = new Http();

        request.setEndpoint(endpoint);
        request.setHeader('SOAPAction' , 'WsCTIAction');
        request.setMethod('POST');
        request.setHeader('Accept-Encoding', 'gzip,deflate');
        request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        request.setHeader('Host', 'vcip.com.mx');
        request.setHeader('Connection', 'Keep-Alive');
        request.setHeader('User-Agent', 'Apache-HttpClient/4.1.1 (java 1.5)');
        request.setTimeout(120000);
        request.setBody(doc);

        response = http.send(request);
        sXml = response.getBody();
        final list<string> lstNodos = new list<string>{'<return>'};
        final list<string> lstNodosF = new list<string>{'</return>'};
        sRespuesta = sXml;

        if (sXml.contains('OK') ) {
            sRespuesta = sRespuestaOK(sXml,lstNodos,lstNodosF);
        } else {
            sRespuesta = sRespuestaERROR(sXml,lstNodos,lstNodosF);
        }
        URL.getSalesforceBaseUrl().toExternalForm();

        URL.getCurrentRequestUrl().toExternalForm();

        lstRequest.add(sRespuesta);
        lstRequest.add(doc);
        return lstRequest;
    }

    /*Método: Entrega la cadena de respuesta OK
    *28-01-2019
    *Karen Belem Sanchez Ruiz*/
    public static String sRespuestaOK(String sXml, list<string> lstNodos, list<string> lstNodosF) {
        Integer iInicio = 0;
        Integer iFin = 0;
        String sXmlx = '';
        String sRespuesta = '';

        string rXml = sXml.replace('<return xsi:type="xsd:string">','<return>');

        Integer iNodo = 0;
        for (String e : lstNodos) {
            iInicio = rXml.IndexOf(e,0);
            iFin = rXml.IndexOf(lstNodosF[iNodo],0);
            iFin = iFin + lstNodosF[iNodo].length();

            if (rXml.length() >0 && iInicio > 0 && iFin > 0) {
                sXmlx = rXml.substring(iInicio, iFin);
                if (sXmlx.length() > 0 && e == '<return>') {
                    sRespuesta = parse(sXmlx);
                }
            }
            iNodo++;
        }
        return sRespuesta;
    }

    /*Método: Entrega la cadena de respuesta OK
    *28-01-2019
    *Karen Belem Sanchez Ruiz*/
    public static String sRespuestaERROR(String sXml, list<string> lstNodos, list<string> lstNodosF) {
        Integer iInicio = 0;
        Integer iFin = 0;
        String sXmlx = '';
        final string rXml = sXml.replace('<return xsi:type="xsd:string">','<return>');
        Integer iNodo = 0;
        String sRespuesta = '';

        for (String e : lstNodos) {
            iInicio = rXml.IndexOf(e,0);
            iFin = rXml.IndexOf(lstNodosF[iNodo],0);
            iFin = iFin + lstNodosF[iNodo].length();
            if (rXml.length() >0 && iInicio > 0 && iFin > 0) {
                sXmlx = rXml.substring(iInicio, iFin);
                if (sXmlx.length() > 0) {
                    if(e == '<return>') {
                        sRespuesta = parse(sXmlx);
                    }
                }
            }
            iNodo++;
        }
        return sRespuesta;
    }

    /*Método: Analiza los argumentos en xml y los carga en un documento
    *28-01-2019
    *Karen Belem Sanchez Ruiz*/
    public static String parse(String toParse) {
        String r = '';
        final DOM.Document doc = new DOM.Document();
        try {
            doc.load(toParse);
            final DOM.XMLNode root = doc.getRootElement();
            return walkThrough(root);
        } catch (System.XMLException e) {
            r = e.getMessage();
        }
        return r;
    }

    public static String walkThrough(DOM.XMLNode node) {
        String result = '\n';
        if (node.getNodeType() == DOM.XMLNodeType.COMMENT) {
            return 'Comment (' +  node.getText() + ')';
        }

        if (node.getNodeType() == DOM.XMLNodeType.TEXT) {
            return 'Text: ' + node.getText() + ' ';
        }

        if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
            if (node.getText() != '') {
                result += node.getText();
            }

            if (node.getAttributeCount() > 0) {
                for (Integer i = 0; i< node.getAttributeCount(); i++ ) {
                    result += ', attribute #' + i + ':' + node.getAttributeKeyAt(i) + '=' + node.getAttributeValue(node.getAttributeKeyAt(i), node.getAttributeKeyNsAt(i));
                    result += ', text=' + node.getText();
                }
            }
            for (Dom.XMLNode child: node.getChildElements()) {
                result += walkThrough(child);
            }
            return result;
        }
        return '';
    }
}