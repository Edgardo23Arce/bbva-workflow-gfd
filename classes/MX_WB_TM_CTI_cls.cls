/*
* Nombre: MX_WB_TM_CTI_cls
* @author Karen Sanchez (KB)
* Proyecto: MW WB Tlmkt - BBVA Bancomer
* Descripción : Clase que implementa metodos para realizar la conexión desde SFDC hacia CTI.

* --------------------------------------------------------------------------
*                         Fecha           Autor                   Desripción
* -------------------------------------------------------------------
* @version 1.0           28/01/2019      Karen Sanchez            Creación
* @version 2.0           11/03/2019      Karen Sanchez            Modificación: En la variable de tipo string sRespuesta se agrega para que solo almacene solo la respuesta
*                                                                 que se obtiene del envio a CTI, también se modifican consultas para quitar issues de complejidad  ciclomática
*/

public with sharing class MX_WB_TM_CTI_cls {
    /*Constante para el objeto de leads y opportunities*/
    static final String SOBJECTOOPP, SOBJECTOLEAD;
    public static final String soapXSI = MX_WB_GenerarXML__c.getInstance().soapXSI__c;
    public static final String soapXSD = MX_WB_GenerarXML__c.getInstance().soapXSD__c;
    public static final String soapENV = MX_WB_GenerarXML__c.getInstance().soapENV__c;
    public static final String soapWS = MX_WB_GenerarXML__c.getInstance().soapWS__c;
    public static final String soapEncoding = MX_WB_GenerarXML__c.getInstance().soapEncoding__c;

    static {
        SOBJECTOOPP ='Opportunity';
        SOBJECTOLEAD = 'Lead';
    }


    /*
*Método: método a futuro que sirve para extraer los resultados del request del servicio
*28-01-2019
*Karen Belem Sanchez Ruiz*/
    @future(callout=true)
    public static void ftProcesaSol(
        String sIdOpp, String sFolioCotizacion, String sIdProducto, String sIdOwnerId, String sNameCte, String sTelefono, String sObjeto, Integer iTipo,
        String sTelefono2, String sTelefono3) {
            String  sXML, tel1, tel2, tel3;
            Boolean bProductoFamilia = false;
            String sNombre = sNameCte != null ? sNameCte : '' ;
            String sFolio = sFolioCotizacion == null ? '' : sFolioCotizacion;
            List<String> lstTelefonos = new List<String>();

           

            lstTelefonos = validacionTelefono(sObjeto,sTelefono,sTelefono2,sTelefono3);
            tel1 = lstTelefonos[1];
            tel2 = lstTelefonos[2];
            tel3 = lstTelefonos[3];

            if(iTipo == 0) {
                bProductoFamilia = true;
            }
            if(bProductoFamilia) {
                sXML = MX_WB_TM_CTI_cls.GenerarXML(tel1, sNombre, sIdProducto == null ? '' : sIdProducto, sIdOpp, sIdOwnerId, sFolio, iTipo, tel2,tel3);
                if(String.isNotBlank(sXML)) {
                    MX_WB_TM_CTI_cls.reqSolicitud(sXML, sIdOpp, sObjeto);
                }
            }
        }

    public static List<String> ProcesaSol(
        String sIdOpp, String sFolioCotizacion, String sIdProducto,
        String sIdOwnerId, String sNameCte, String sTelefono,
        String sObjeto, Integer iTipo , String sTelefono2, String sTelefono3) {
            String  sXML, tel1, tel2, tel3;
            String sNombre = sNameCte != null ? sNameCte : '' ;
            String sFolio = sFolioCotizacion == null ? '' : sFolioCotizacion;
            List<String> lstTelefonos = new List<String>();
            List<String> sRequest = new List<String>();

            String Telefono  = sTelefono  != null ? sTelefono  : '';
            String Telefono2 = sTelefono2 != null ? sTelefono2 : '';
            String Telefono3 = sTelefono3 != null ? sTelefono3 : '';

            lstTelefonos = validacionTelefono(sObjeto,Telefono,Telefono2,Telefono3);
            tel1 = lstTelefonos[1];
            tel2 = lstTelefonos[2];
            tel3 = lstTelefonos[3];

            sXML = MX_WB_TM_CTI_cls.GenerarXML(tel1, sNombre, sIdProducto == null ? '' : sIdProducto, sIdOpp, sIdOwnerId, sFolio, iTipo, tel2,tel3);
            if(String.isNotBlank(sXML)) {
                sRequest =  MX_WB_TM_CTI_cls.reqSolicitud(sXML, sIdOpp, sObjeto);
            }
            return sRequest;
        }

    /*Método: Valida el teléfono para asignar lada
*28-01-2019
*Karen Belem Sanchez Ruiz*/
    public static List<String> validacionTelefono(String sObjeto, String sTelefono, String sTelefono2, String sTelefono3) {
        List<String> telefonos = new List<String>();

        String Telefono  = String.isNotBlank(sTelefono)  && sTelefono.startsWith('55')  ? '044' + sTelefono   : '045' + sTelefono;
        String Telefono2 = String.isNotBlank(sTelefono2) && sTelefono2.startsWith('55') ? '044' + sTelefono2  : '045' + sTelefono2;
        String Telefono3 = String.isNotBlank(sTelefono3) && sTelefono3.startsWith('55') ? '044' + sTelefono3  : '045' + sTelefono3;

        telefonos = new List<String> {sObjeto,Telefono,Telefono2,Telefono3};
            return telefonos;
    }


    /*Método: Se genera el XML
*28-01-2019
*Karen Belem Sanchez Ruiz*/
    public static String GenerarXML(String sTelefono,String sNombre,String sProducto,
                                    String sIdProspecto, String sAgente, String sFolio,
                                    Integer iTipo, String sTelefono2, String sTelefono3) {
                                        MX_WB_CredencialesCTI__c credenciales = new MX_WB_CredencialesCTI__c();
                                        dom.Document doc = new dom.Document();
                                        

                                        String soapXSI = 'http://www.w3.org/2001/XMLSchema-instance';
                                        String soapXSD = 'http://www.w3.org/2001/XMLSchema';
                                        String soapENV = 'http://schemas.xmlsoap.org/soap/envelope/';
                                        String soapWS = Label.soapWS;

                                        dom.Xmlnode envelope = doc.createRootElement('Envelope', soapENV, 'soapenv');
                                        dom.XmlNode header = envelope.addChildElement('Header', soapENV, 'soapenv');
                                        dom.XmlNode body = envelope.addChildElement('Body', soapENV, 'soapenv');

                                        envelope.setNamespace('xsi', soapXSI);
                                        envelope.setNamespace('xsd', soapXSD);
                                        envelope.setNamespace('soapenv', soapENV);
                                        envelope.setNamespace('ws', soapWS);

                                        dom.XmlNode setCall = body.addChildElement('setCall', soapWS, null);
                                        setCall.setAttribute('soapenv:encodingStyle','http://schemas.xmlsoap.org/soap/encoding/');

                                        credenciales = CredencialCTI('ASD');
                                        setCall.addChildElement('user',null,null).addTextNode(credenciales.MX_WB_Usuario__c);
                                        dom.XmlNode nPass = setCall.addChildElement('pass',null,null).addTextNode(credenciales.MX_WB_Contrasenia__c);
                                        dom.XmlNode nPhone = setCall.addChildElement('phone',null,null).addTextNode(sTelefono);
                                        dom.XmlNode nName = setCall.addChildElement('name',null,null).addTextNode(sNombre);
                                        dom.XmlNode nProduct = setCall.addChildElement('product',null,null).addTextNode(sProducto);
                                        dom.XmlNode nLeadId = setCall.addChildElement('leadId',null,null).addTextNode(sIdProspecto);
                                        dom.XmlNode nAgent = setCall.addChildElement('agent',null,null).addTextNode(sAgente);
                                        dom.XmlNode nCallType = setCall.addChildElement('callType',null,null).addTextNode('ANYONE');
                                        dom.XmlNode nFolio = setCall.addChildElement('folio',null,null).addTextNode(sFolio);
                                        dom.XmlNode nPhone2 = setCall.addChildElement('phone',null,null).addTextNode(sTelefono2);
                                        dom.XmlNode nPhone3 = setCall.addChildElement('phone',null,null).addTextNode(sTelefono3);

                                        return Doc.toXmlString();
                                    }

    /*
*Método: sirve para extraer las credenciales para hacer el envío a CTI
*28-01-2019
*Karen Belem Sanchez Ruiz */
    public static MX_WB_CredencialesCTI__c CredencialCTI(String nombreLista) {
        MX_WB_CredencialesCTI__c ListCTI = MX_WB_CredencialesCTI__c.getValues(nombreLista);
        return ListCTI;
    }


    /*Método: Repsuesta del servicio y actualización del lead u oportunidad
*28-01-2019
*Karen Belem Sanchez Ruiz*/
    public static List<String> reqSolicitud(String doc, String IdObjeto, String sObjeto) {
        String endpoint = Label.wsEndPointWibe00;
        String sXml = '';
        String sRespuesta = '';


        List<String> lstRequest = new List<String>();

        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http http = new Http();

        request.setEndpoint(endpoint);
        request.setHeader('SOAPAction' , Label.WsCTIAction);
        request.setMethod('POST');
        request.setHeader('Accept-Encoding', 'gzip,deflate');
        request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        request.setHeader('Host', 'vcip.com.mx');
        request.setHeader('Connection', 'Keep-Alive');
        request.setHeader('User-Agent', 'Apache-HttpClient/4.1.1 (java 1.5)');
        request.setTimeout(120000);
        request.setBody(doc);

        response = http.send(request);
        sXml = response.getBody();

        list<string> lstNodos = new list<string> {'<return>'};
            list<string> lstNodosF = new list<string> {'</return>'};
                sRespuesta = sXml;

        if (sXml.contains('OK') ) {
            sRespuesta = sRespuestaOK(sRespuesta,sXml,lstNodos,lstNodosF);
        }else {
            sRespuesta = sRespuestaERROR(sRespuesta,sXml,lstNodos,lstNodosF);
        }
        String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();

        String currentRequestURL = URL.getCurrentRequestUrl().toExternalForm();

        lstRequest.add(sObjeto);
        lstRequest.add(sRespuesta);
        lstRequest.add(doc);
        lstRequest.add(idObjeto);
        return lstRequest;
    }

    /*Método: Entrega la cadena de respuesta OK
*28-01-2019
*Karen Belem Sanchez Ruiz*/
    public static String sRespuestaOK(String sRespuesta, String sXml, list<string> lstNodos, list<string> lstNodosF) {
        Integer iInicio = 0;
        Integer iFin = 0;
        String sXmlx = '';
        string srespuesta2 = sRespuesta;
        string rXml = sXml.replace('<return xsi:type="xsd:string">','<return>');
        String xmlReplace = rXml;
        Integer iNodo = 0;
        for (String e : lstNodos) {
            iInicio = xmlReplace.IndexOf(e,0);
            iFin = xmlReplace.IndexOf(lstNodosF[iNodo],0);
            iFin = iFin + lstNodosF[iNodo].length();

            if (xmlReplace.length() >0 && iInicio > 0 && iFin > 0) {
                sXmlx = xmlReplace.substring(iInicio, iFin);
                if (sXmlx.length() > 0) {
                    if(e == '<return>') {
                        srespuesta2 = parse(sXmlx);
                    }
                }
            }
            iNodo++;
        }
        return srespuesta2;
    }

    /*Método: Entrega la cadena de respuesta ERROR
*28-01-2019
*Karen Belem Sanchez Ruiz*/
    public static String sRespuestaERROR(String sRespuesta, String sXml, list<string> lstNodos, list<string> lstNodosF) {
        Integer iInicio = 0;
        Integer iFin = 0;
        String sXmlx = '';
        string rXml = sXml.replace('<return xsi:type="xsd:string">','<return>');
        String xmlReplace = rXml;
        
        Integer iNodo = 0;
        String respuesta2;

        for (String e : lstNodos) {
            iInicio = xmlReplace.IndexOf(e,0);
            iFin = xmlReplace.IndexOf(lstNodosF[iNodo],0);
            iFin = iFin + lstNodosF[iNodo].length();
            if (xmlReplace.length() >0 && iInicio > 0 && iFin > 0) {
                sXmlx = xmlReplace.substring(iInicio, iFin);
                if (sXmlx.length() > 0) {
                    if(e == '<return>') {
                        respuesta2 = parse(sXmlx);
                    }
                }
            }
            iNodo++;
        }
        return respuesta2;
    }

    /*Método: Analiza los argumentos en xml y los carga en un documento
*28-01-2019
*Karen Belem Sanchez Ruiz*/
    public static String parse(String toParse) {
        String r = '';
        DOM.Document doc = new DOM.Document();
        try {
            doc.load(toParse);
            DOM.XMLNode root = doc.getRootElement();
            return walkThrough(root);
        } catch (System.XMLException e) {
            r = e.getMessage();
        }
        return r;
    }

    /*Método: Retorna el tipo de nodo
*28-01-2019
*Karen Belem Sanchez Ruiz*/
    public static String walkThrough(DOM.XMLNode node) {
        String result = '\n';
        if (node.getNodeType() == DOM.XMLNodeType.COMMENT) {
            return 'Comment (' +  node.getText() + ')';
        }

        if (node.getNodeType() == DOM.XMLNodeType.TEXT) {
            return 'Text: ' + node.getText() + ' ';
        }

        if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
            if (node.getText() != '') {
                result += node.getText();
            }

            if (node.getAttributeCount() > 0) {
                for (Integer i = 0; i< node.getAttributeCount(); i++ ) {
                    result += ', attribute #' + i + ':' + node.getAttributeKeyAt(i) + '=' + node.getAttributeValue(node.getAttributeKeyAt(i), node.getAttributeKeyNsAt(i));
                    result += ', text=' + node.getText();
                }
            }
            for (Dom.XMLNode child: node.getChildElements()) {
                result += walkThrough(child);
            }
            return result;
        }
        return '';
    }
}