/*
 *  @author: Jaime Terrats
 *  @description: Opportunity Trigger handler
 *  @version: 1.0
 * 
 */

public without sharing class MX_SB_VTS_OpportunityTriggerHandler extends TriggerHandler {
	private List<Opportunity> newOppList;
    
    public MX_SB_VTS_OpportunityTriggerHandler() {
        this.newOppList = Trigger.new;
    }
    
    /*
	 * Process before update logic 
	 */
    protected override void beforeUpdate() {
        MX_SB_VTS_CierreOpp.validaCierre(newOppList);
    }
}