/*
*
* @author Jaime Terrats
* @description Unit tests for controller
*
*           No  |     Date     |     Author      |    Description
* @version  1.0    04/10/2019     Jaime Terrats     Create Unit Test
*           1.1     21/05/2019      Arsenio Perez   Code smell
*/
@isTest
private class MX_SB_VTS_QuoteFinder_Test {
    /**Constante numerico ***/
    private static final string NUMEROCONSTANTE = '9999'; 
    @TestSetup
    static void makeData() {
        final User tUser = MX_WB_TestData_cls.crearUsuario('testUser qf', System.Label.MX_SB_VTS_ProfileAdmin);
        insert tUser;

        System.runAs(tUser) {
            MX_WB_TestData_cls.createStandardPriceBook2();

            final Product2 producto = MX_WB_TestData_cls.productNew('Hogar');
            producto.isActive = true;
            insert producto;

            final PricebookEntry pbe = MX_WB_TestData_cls.priceBookEntryNew(producto.Id);
            Insert pbe;

            final Account acc = MX_WB_TestData_cls.crearCuenta('test qf', 'PersonAccount');
            insert acc;

            final Opportunity opp = MX_WB_TestData_cls.crearOportunidad('testOpp fq', acc.Id, tUser.Id, 'ASD');
            opp.StageName = 'Objeciones';
            opp.Producto__c = 'Hogar';
            final Opportunity opp2 = MX_WB_TestData_cls.crearOportunidad('test2 qf', acc.Id, tUser.Id, 'ASD');
            opp2.StageName = 'Objeciones';
            opp2.Producto__c = 'Hogar';
           	final List<Opportunity> oppTests = new List<Opportunity>();
            oppTests.add(opp);
            oppTests.add(opp2);
            insert oppTests;


            final OpportunityLineItem oli = MX_WB_TestData_cls.oppLineItmNew(opp.Id, pbe.Id, producto.Id, 1, 1);
            insert oli;

            final Quote quo = MX_WB_TestData_cls.crearQuote(opp.Id, '9999 1 OppTest findQuote', NUMEROCONSTANTE);
            insert quo;
        }
    }

    @isTest
    static void testFindQuote() {
        Quote fQuote = new Quote();
        Test.startTest();
        fquote = MX_SB_VTS_QuoteFinder.findQuote(NUMEROCONSTANTE);
        Test.stopTest();
        System.assertNotEquals(fQuote, null, 'Se encontro quote');
    }

    @isTest
    static void testUpdateQuote() {
        Quote updQuote = new Quote();
        Boolean flag;
        Test.startTest();
        updQuote = MX_SB_VTS_QuoteFinder.findQuote(NUMEROCONSTANTE);
        updQuote.MX_SB_VTS_Motivos_de_no_venta__c = 'Cliente molesto';
        flag = MX_SB_VTS_QuoteFinder.updateQuote(updQuote);
        Test.stopTest();
        System.assert(flag, 'Se actualizo la cotizacion');
    }

    @isTest
    static void testLast5Quotes() {
        List<Quote> last5Quotes = new List<Quote>();
        final Opportunity opp = [Select Id from Opportunity where Name =: 'testOpp fq'];
        Test.startTest();
        last5Quotes = MX_SB_VTS_QuoteFinder.getLast5Quotes(opp.Id);
        Test.stopTest();
        System.assertEquals('9999 1 OppTest findQuote', last5Quotes[0].Name, 'Los nombres coinciden');
    }

    @isTest
    static void failQuoteSearch() {
        Test.startTest();
        try {
            MX_SB_VTS_QuoteFinder.findQuote('8888');
            throw new AuraHandledException(system.label.MX_SB_VTS_Found);
        } catch(AuraHandledException aEx) {
            final Boolean expectedExcep =  aEx.getMessage().contains('Script-thrown') ? true : false;
            System.assert(expectedExcep, 'Error Esperado');
        }
        Test.stopTest();
    }

    @isTest
    static void failGet5Quotes() {
        final String oppId = [Select Id from Opportunity where Name =: 'test2 qf'].Id;
        Test.startTest();
        try {
            MX_SB_VTS_QuoteFinder.getLast5Quotes(oppId);
            throw new AuraHandledException(system.label.MX_SB_VTS_Found);
        } catch(AuraHandledException aEx) {
            final Boolean expectedExcep =  aEx.getMessage().contains('Script-thrown') ? true : false;
            System.assert(expectedExcep, 'Error Esperado');
        }
        Test.stopTest();
    }

    @isTest
    static void failUpdate() {
        Quote updQuote = new Quote();
        Boolean flag;
        Test.startTest();
        try {
            updQuote = MX_SB_VTS_QuoteFinder.findQuote(NUMEROCONSTANTE);
            updQuote.MX_SB_VTS_Motivos_de_no_venta__c = 'Lorem Ipsum';
            flag = MX_SB_VTS_QuoteFinder.updateQuote(updQuote);
        } catch(AuraHandledException aEx) {
            System.assertEquals(flag, null, 'Error al actualizar');
        }
        Test.stopTest();
    }
}