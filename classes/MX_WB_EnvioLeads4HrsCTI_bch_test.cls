@isTest
public class MX_WB_EnvioLeads4HrsCTI_bch_test {
	@testSetup
    static void initData() {
        final String equivocado = 'Tel. Equivocado';
        final MX_WB_FamiliaProducto__c testFamilia = MX_WB_TestData_cls.createProductsFamily('Test Familia');
        insert testFamilia;
        final Campaign testCampaign = MX_WB_TestData_cls.createCampania('Test Camp');
        testCampaign.MX_WB_FamiliaProductos__c = testFamilia.Id;
        insert testCampaign;
        final Lead testLead = MX_WB_TestData_cls.createLead('Test Data');
        testLead.MX_WB_ph_Telefono1__c = '5551040002';
        testLead.MX_WB_ph_Telefono2__c = '5551040003';
        testLead.MX_WB_ph_Telefono3__c = '5551040004';
        insert testLead;

        final Lead lostLead = MX_WB_TestData_cls.createLead('Test Lost');
        lostLead.MX_WB_ph_Telefono1__c = '5551040002';
        lostLead.MX_WB_ph_Telefono2__c = '5551040003';
        lostLead.MX_WB_ph_Telefono3__c = '5551040004';
        insert lostLead;

        final Task tarea1 = new Task();
        tarea1.Telefono__c = '5551040002';
        tarea1.Motivos_CONTACTO__c = equivocado;
        tarea1.WhoId = lostLead.Id;
        insert tarea1;
        final Task tarea2 = new Task();
        tarea2.Telefono__c = '5551040003';
        tarea2.Motivos_CONTACTO__c = equivocado;
        tarea2.WhoId = lostLead.Id;
        insert tarea2;
        final Task tarea3 = new Task();
        tarea3.Telefono__c = '5551040004';
        tarea3.Motivos_CONTACTO__c = equivocado;
        tarea3.WhoId = lostLead.Id;
        insert tarea3;

        //final CampaignMember testMember = MX_WB_TestData_cls.createCampaignMember(testLead.Id, testCampaign.Id);
        final MX_WB_CredencialesCTI__c credenciales = new MX_WB_CredencialesCTI__c();
        credenciales.MX_WB_Usuario__c ='TestUsuario';
        credenciales.MX_WB_Contrasenia__c ='TestPass';
        credenciales.Name ='ASD';
        insert credenciales;

        final MX_WB_MotivosNoContacto__c testMotivoNo = new MX_WB_MotivosNoContacto__c();
        testMotivoNo.Name=equivocado;
        testMotivoNo.MX_WB_MotivoNoContacto__c=equivocado;
        insert testMotivoNo;
    }

    testMethod static void batchTest() {
        Test.setMock(HttpCalloutMock.class, new MX_WB_WSCall_Mock());
        Test.StartTest();
        final MX_WB_EnvioLeads4HrsCTI_bch bh1 = new MX_WB_EnvioLeads4HrsCTI_bch('SELECT Id, Folio_Cotizacion__c, OwnerId, FirstName, (SELECT Id, Motivos_CONTACTO__c, Telefono__c from Tasks), '
                                                                          + 'MX_WB_ph_Telefono1__c, MX_WB_ph_Telefono2__c, MX_WB_ph_Telefono3__c'
                                                                          + ' FROM Lead');
        final ID jobId = DataBase.executeBatch(bh1);
        Test.stopTest();
        System.assertNotEquals(null, jobId);
    }
}