/**-------------------------------------------------------------------------
* Nombre: rtwCliente
* @author Julio Medellin
* Proyecto: MW SB VTS - BBVA 
* Descripción : Clase control para componente rtwCliente

* --------------------------------------------------------------------------
*                         Fecha           Autor                   Descripción
* -------------------------------------------------------------------
* @version 1.0           02/05/2019      Julio Medellin            Encabezado
* @version 1.0           02/05/2019      Julio Medellin            Se adecua clase al nuevo modelo de datos
* @version 1.1           09/05/2019      Eduardo Hernández         BugFix para petición del servicio ASD Org Retail
* --------------------------------------------------------------------------*/
@RestResource(urlMapping='/Clientes/*')
global with sharing class rtwCliente
{
    /**
    * @desrciption: Objeto de Respuesta - Response
    */
    global class resSFDC
    {
        /*Variable de respuesta error alservicio REST */
        global String error {get; set;}
         /*Variable de respuesta message alservicio REST */
        global String message {get; set;}
         /*Variable de respuesta id alservicio REST */
        global String id {get; set;}
    }
    /**
    * @desrciption: Objeto de Entrada - Request
    */
    global class reqCliente
    {
         /*Variable de respuesta folioCliente alservicio REST */
        global String folioCliente {get; set;}
         /*Variable de respuesta nombre alservicio REST */
        global String nombre {get; set;}
         /*Variable de respuesta apellidoPaterno alservicio REST */
        global String apellidoPaterno {get; set;}
         /*Variable de respuesta apellidoMaterno alservicio REST */
        global String apellidoMaterno {get; set;}
         /*Variable de respuesta correoElectronico alservicio REST */
        global String correoElectronico {get; set;}
         /*Variable de respuesta fechaNacimiento alservicio REST */
        global Date fechaNacimiento {get; set;}
         /*Variable de respuesta telefono alservicio REST */
        global String telefono {get; set;}
        /*Variable de respuesta telefonoCelular alservicio REST */
        global String telefonoCelular {get; set;}
         /*Variable de respuesta edad alservicio REST */
        global String edad {get; set;}
         /*Variable de respuesta sexoDelConductor alservicio REST */
        global String sexoDelConductor {get; set;}
         /*Variable de respuesta rfc alservicio REST */
        global String rfc {get; set;}
         /*Variable de respuesta nacionalidad alservicio REST */
        global String nacionalidad {get; set;}
         /*Variable de respuesta profesion alservicio REST */
        global String profesion {get; set;}
         /*Variable de respuesta colonia alservicio REST */
        global String colonia {get; set;}
         /*Variable de respuesta calleOAvenida alservicio REST */
        global String calleOAvenida {get; set;}
         /*Variable de respuesta codigoPostal alservicio REST */
        global String codigoPostal {get; set;}
         /*Variable de respuesta numeroExterior alservicio REST */
        global String numeroExterior {get; set;}
         /*Variable de respuesta numeroInterior alservicio REST */
        global String numeroInterior {get; set;}
         /*Variable de respuesta origen alservicio REST */
        global String origen {get; set;}
        /*Variable de respuesta ciudad alservicio REST */
        global String ciudad {get; set;}
         /*Variable de respuesta estado alservicio REST */
        global String estado {get; set;}
         /*Variable de respuesta pais alservicio REST */
        global String pais {get; set;}
         /*Variable de respuesta descripcion alservicio REST */
        global String descripcion {get; set;}
         /*Variable de respuesta motivoNoInteres alservicio REST */
        global String motivoNoInteres {get; set;}
         /*Variable de respuesta delegacion alservicio REST */
        global String delegacion {get; set;}
         /*Variable de respuesta motivoNoElegibilidad alservicio REST */
        global String motivoNoElegibilidad  {get; set;}
         /*Variable de respuesta gclid alservicio REST */
        global String gclid {get; set;}
         /*Variable de respuesta estatusCotizacion alservicio REST */
        global String estatusCotizacion {get; set;}
         /*Variable de respuesta folioDeCotizacion alservicio REST */
        global String folioDeCotizacion {get; set;}
         /*Variable de respuesta valorRealIntermediarioCot alservicio REST */
        global String valorRealIntermediarioCot {get; set;}
         /*Variable de respuesta descripcionIntermediarioCot alservicio REST */
        global String descripcionIntermediarioCot {get; set;}
         /*Variable de respuesta TipoDeRegistro alservicio REST */
        global String TipoDeRegistro {get; set;}
         /*Variable de respuesta productoPlan alservicio REST */
        global ProductoPlan productoPlan{get;set;}
    }   

    /**
    * @description: Se crea o actualiza un Cliente
    */
    @HttpPost
    //global static resSFDC upsertCliente(){
    global static resSFDC upsertCliente( reqCliente cliente ){
        String sMsn = '';
        Boolean blRta = true;
        resSFDC rSFDC = new resSFDC();
        rSFDC.error = 'Ejecución de Método: upsertcliente';
        
        try{

            //Request
            sMsn = 'Request: ' + cliente + '. ';
            String sId = null;
            Schema.DescribeSObjectResult oAccount = Account.sObjectType.getDescribe();
            Schema.DescribeSObjectResult oLead = Lead.sObjectType.getDescribe();
            String sIdFolioCliente = cliente.folioCliente;
            String sPrefAccount = oAccount.getKeyPrefix();
            String sPrefLead = oLead.getKeyPrefix();
            
            if (cliente.correoElectronico != 'development@developmentadesis.com' && cliente.correoElectronico != 'm@m.com'
                && cliente.correoElectronico != 'maria007@hotmail.com' && cliente.correoElectronico != 'sara@hotmail.com'
                && cliente.correoElectronico != 'luis.perez@hotmail.com' && cliente.correoElectronico != 'a@a.com'
                && cliente.correoElectronico != 'marcelamg@gsekura.com'){
                    sId = fnCrearCliente( cliente );
                    //Arma el mensaje de Retorno
                    if(sId.contains('-')){
                        rSFDC.message = 'Error al actualizar los datos del cliente: ' + sId.substring(sId.indexOf('-')+1, sId.length());
                        sMsn += rSFDC.message + ' : ' + sId.substring(sId.indexOf('-')+1, sId.length());
                        rSFDC.id = '';
                    }else{
                        
                        
                        rSFDC.message = 'Registro guardado exitosamente: ' + sId;
                        sMsn += rSFDC.message;
                        rSFDC.id = sId;
                    }
                    
                }else{//Fin si cliente.correoElectronico != 'development@developmentadesis.com'
                    //Arma el mensaje de Retorno
                    rSFDC.message = 'Registro que pertece a la lista negra: ' + cliente.correoElectronico;
                    sMsn += rSFDC.message;
                    rSFDC.id = '';
                    
                }
            
        }catch( DmlException ex ){
            rSFDC.message = 'El siguiente error DML a ocurrido: ' + ex.getMessage();
            sMsn += rSFDC.message;
            blRta = false;
            rSFDC.id = '';
        }catch(Exception ex){
            rSFDC.message = 'El siguiente error ha ocurrido: ' + ex.getMessage();
            sMsn += rSFDC.message;
            blRta = false;
            rSFDC.id = '';
        }
        
        //Crea el Log en SalesForce
        if (cliente.correoElectronico != 'development@developmentadesis.com')
            WB_CrearLog_cls.fnCrearLog( sMsn, 'rtwCliente', blRta );
        
        //Regresa rSFDC
        return rSFDC;
    }
    
    /**
* @description: Se crea o actualiza un Cliente
*/
    public static String fnCrearCliente( reqCliente cliente ){       
        String sIdAccount;
        Boolean bUpdateCte = true;
        String sCorreoElectronico = '';
        
        //Busca el cliente a traves del campo Correo_Electronico__c
        if (cliente.correoElectronico != null && cliente.correoElectronico != ''){
            sCorreoElectronico = String.valueOf(cliente.correoElectronico.toLowerCase('ES'));
            for (Account cte : [Select id, Correo_Electronico__c
                                , (Select id, Estatus__c
                                   From Opportunities Where Estatus__c = 'Emitida')
                                From Account
                                Where Correo_Electronico__c = : sCorreoElectronico]){
                                    sIdAccount = cte.id;
                                    //Ve si tiene Opp emitidas
                                    if (!cte.Opportunities.isEmpty())
                                        bUpdateCte = false;
                                }
        }
        
        //Busca el Tipo de Registro
        String sIdProspecto = [
            SELECT  Id
            FROM    RecordType
            Where   DeveloperName= 'Prospecto'
            and     IsPersonType = true
            and     SobjectType = 'Account'].Id;
        //Inicializa el objeto
        Account entCliente = new Account();
        //Mapeo de Campos
        if (sIdAccount != null)
            entCliente.Id = sIdAccount;
        if (bUpdateCte){
            
            entCliente.RecordTypeId = sIdProspecto;
            entCliente.AccountSource = cliente.origen;
            entcliente.BillingCity = cliente.ciudad;
            entCliente.BillingCountry = cliente.pais;
            entCliente.BillingPostalCode = cliente.codigoPostal;
            entCliente.BillingState = cliente.estado;
            entCliente.BillingStreet = cliente.calleOAvenida;
            entCliente.Colonia__c = cliente.colonia;
            entCliente.Description = cliente.descripcion;
            entCliente.Profesion__c = cliente.profesion;
            entCliente.RFC__c = cliente.rfc;
            entCliente.Genero__c = cliente.sexoDelConductor;
            entCliente.Nacionalidad__c = cliente.nacionalidad;
            entCliente.Numero_Exterior__c = (cliente.numeroExterior != '' ? cliente.numeroExterior : null );
            entCliente.Numero_Interior__c = ( cliente.numeroInterior != '' ? cliente.numeroInterior : null );
            entCliente.Delegacion__c = cliente.delegacion;
            entCliente.FirstName = cliente.nombre;
            entCliente.LastName = cliente.apellidoPaterno; 
            entCliente.ApellidoMaterno__c = cliente.apellidoMaterno;   
            entCliente.Motivonointeres__c = cliente.motivoNoInteres;
            entCliente.PersonBirthdate = cliente.FechaNacimiento;
            entcliente.Edad__c = ( cliente.edad != '' ? decimal.valueOf(cliente.edad) : null );
            entCliente.PersonEmail = sCorreoElectronico;
            entCliente.Correo_Electronico__c = sCorreoElectronico;
            entCliente.PersonHomePhone = cliente.telefono;
            entCliente.Phone = cliente.telefonoCelular;
            entCliente.Salutation = '';
            entCliente.Tipo_Persona__c = (cliente.rfc == '' ? 'Física' : 'Moral');
            entCliente.Motivo_de_no_elegibilidad__c = cliente.motivoNoElegibilidad ;
        }//Fin si bUpdateCte
        if (!bUpdateCte){
            entCliente.Phone = cliente.telefonoCelular;
            entCliente.PersonHomePhone = cliente.telefono;
        }//Fin si !bUpdateCte
        entCliente.codigoIntermediario__c = cliente.valorRealIntermediarioCot;
        entCliente.nombreIntermediario__c = cliente.descripcionIntermediarioCot;
        
        
        String sIdCte = '';
        try{
            //Valida los datos del cliente
            if (cliente.correoElectronico != '' && cliente.correoElectronico != null && cliente.telefonoCelular != '' && cliente.telefonoCelular != null){
               
                    upsert entCliente;
     
            }
            
           //Retorna el Id de la Oportunidad
            if (entCliente.Id != null)
                sIdCte = entCliente.Id;
            else
                
                sIdCte = '1';
        }catch(Exception ex){
            sIdCte = entCliente.id + '-' + ex.getMessage();
        }
        
        return sIdCte ;
        
    }
    
    /**
* @description: Se crea o actualiza un Lead
*/
    public static String fnCrearCandidato( reqCliente cliente ){
        
        String sCorreoElectronico = String.valueOf(cliente.correoElectronico.toLowerCase('ES'));
        
        //Inicializa el objeto
        Lead entCandidato = new Lead();
        //Mapeo de Campos
        entCandidato.Id = ( cliente.folioCliente != '' ? cliente.folioCliente : null );
        entCandidato.LeadSource = cliente.origen;
        entCandidato.City = cliente.ciudad;
        entCandidato.Country = cliente.pais;
        entCandidato.PostalCode = cliente.codigoPostal;
        entCandidato.State = cliente.estado;
        entCandidato.Street = cliente.calleOAvenida;
        entCandidato.Colonia__c = cliente.colonia;
        entCandidato.Description = cliente.descripcion;
        entCandidato.Profesion__c = cliente.profesion;
        entCandidato.RFC__c = cliente.rfc;
        entCandidato.Genero__c = cliente.sexoDelConductor;
        entCandidato.Nacionalidad__c = cliente.nacionalidad;
        entCandidato.Numero_Exterior__c = (cliente.numeroExterior != '' ? cliente.numeroExterior : null );
        entCandidato.Numero_Interior__c = ( cliente.numeroInterior != '' ? cliente.numeroInterior : null );
        entCandidato.Delegacion__c = cliente.delegacion;
        entCandidato.FirstName = cliente.nombre;
        entCandidato.LastName = cliente.apellidoPaterno;
        entCandidato.Apellido_Materno__c =  cliente.apellidoMaterno;
        entCandidato.Motivonointeres__c = cliente.motivoNoInteres;
        entCandidato.Fecha_Nacimiento__c = cliente.FechaNacimiento;
		entCandidato.Edad__c =  String.isNotEmpty(cliente.edad) ? decimal.valueOf(cliente.edad) : 0;
        entCandidato.Email = sCorreoElectronico;
        entCandidato.Phone = cliente.telefono;
        entCandidato.MobilePhone = cliente.telefonoCelular;
        entCandidato.Salutation = '';
        entCandidato.Tipo_Persona__c = cliente.rfc == '' ? 'Física' : 'Moral';
        entCandidato.Motivo_de_no_elegibilidad__c = cliente.motivoNoElegibilidad ;
        upsert entCandidato;
        //Retorna el Id
        return entCandidato.Id;
    }
        
    //Regresa un objeto del tipo PartyAddress si no hubo error a la hora de registrar el cliente
    public static reqCliente JSONParserreqCliente(String sJsonResp){
        
        reqCliente objAPPResp = new reqCliente();
        
        try{
            JSONParser parser = JSON.createParser(sJsonResp);
            //Ve si tiene algo el objeto de parser
            while (parser.nextToken() != null) {
                //Inicia el detalle del objeto: sNombreObj
                if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                    //Toma el contenido del Json y conviertelo a SignInCls.class
                    objAPPResp = (reqCliente)parser.readValueAs(reqCliente.class);
                }//Fin si parser.getCurrentToken() == JSONToken.START_OBJECT
            }//Fin mientras parser.nextToken() != null
        }catch(Exception ex){
            System.debug('ERROR EN JSONParserreqCliente: sJsonResp: ' + ex.getMessage());
        }
        
        //Regresa el objeto objSignInClsPaso
        return objAPPResp;
    }
    
}