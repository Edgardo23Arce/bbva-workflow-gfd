/*
----------------------------------------------------------
* Nombre: MX_SB_MLT_GoogleGeolocation_Test
* Isaías Velázquez Cortés
* Proyecto: Siniestros - BBVA Bancomer
* Descripción : Clase que prueba la clase MX_SB_MLT_GoogleGeolocation_Cls

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Desripción<p />
* --------------------------------------------------------------------------------
* 1.0           02/05/2019     Isaías Velázquez Cortés          Creación
* 1.1           14/05/2019     Daniel Goncalves Vivas           Cambios de Asserts con mensajes
* --------------------------------------------------------------------------------
*/

@isTest
global class MX_SB_MLT_GoogleGeolocation_Test {

    @isTest static void testCalloutAddress() {
        Test.setMock(HttpCalloutMock.class, new MX_SB_MLT_MockHttpResponseGenerator_Test());
        final HttpResponse res = MX_SB_MLT_GoogleGeolocation_Cls.getReverseGeocode('19.3028657','-99.1527164');
        final String actualValuea = res.getBody();
        final String expectedValuea = '"plus_code" : { "compound_code" : "8R3W+4W Mexico City, CDMX, Mexico","global_code" : "76F28R3W+4W" },"results" : [],"status" : "ZERO_RESULTS"}';
        System.assertEquals(expectedValuea, actualValuea, 'Prueba Callout Address');
    }

    @isTest static void testCalloutMethodLocation() {
        Test.setMock(HttpCalloutMock.class, new MX_SB_MLT_MockHttpResponseGenerator_Test());
        final HttpResponse res = MX_SB_MLT_GoogleGeolocation_Cls.getLocationGeocode('Estadio+Azteca');
        final String actualValueb = res.getBody();
        final String expectedValueb = '"plus_code" : { "compound_code" : "8R3W+4W Mexico City, CDMX, Mexico","global_code" : "76F28R3W+4W" },"results" : [],"status" : "ZERO_RESULTS"}';
        System.assertEquals(expectedValueb, actualValueb, 'Prueba Callout Location');
    }

    @isTest static void testCalloutMethodAuraOk() {
        final String actualValuec = MX_SB_MLT_GoogleGeolocation_Cls.getLocationGeocodeString('isATest');
        final String expectedValuec = '37.4224764,-122.0842499';
        System.assertEquals(expectedValuec, actualValuec, 'Prueba Callout Aura OK');
    }
    
    @isTest static void testCalloutMethodAuraNull() {
        final String expectedValuee ='';        
        final String sNULL=null;
        final String actualValuee = '';
        try {
             actualValuee = MX_SB_MLT_GoogleGeolocation_Cls.getLocationGeocodeString(sNULL);
        } catch (Exception e) {
            System.debug('mensaje : '+e.getMessage());
            System.assertEquals(expectedValuee, actualValuee, 'Prueba Callout Aura Null');
        }
        
    }
}