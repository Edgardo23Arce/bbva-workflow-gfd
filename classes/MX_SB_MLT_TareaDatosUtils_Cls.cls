/*
----------------------------------------------------------
* Nombre: MX_SB_MLT_TareaDatosUtils_Cls
* Autor Oscar Martínez
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Clase con metódos reutilizables para el flujos de la creación de tarea de datos
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   			Desripción<p />
* --------------------------------------------------------------------------------
* 1.0           25/04/2019     Oscar Martínez           		   	Creación
* 1.1           07/05/2019     Oscar Martínez           		   	Se agrega Method que valida cadenas vacías
* 1.2           14/05/2019     Oscar Martínez           		   	Se modifica Method de obtieneCase, para evitar injection soql
* 1.3           15/05/2019     Daniel Goncalves Vivas               Corrección de acentos
* --------------------------------------------------------------------------------
*/

public with sharing class MX_SB_MLT_TareaDatosUtils_Cls {
    private MX_SB_MLT_TareaDatosUtils_Cls() {}
    /*
    * @description Method que devuelve una Lista de casos.
    * @param String idCase
	* @return List<sObject>
    */
    public static List<Case> obtieneCase(String idCase) {
        final List<case>lstCase = [Select Id,Status,Origin,SiniestroAbierto__c,MX_SB_MLT_URLLocation__c,MX_SB_MLT_SegmentClient__c,TipoSiniestro__c,RecordType.Name From Case where Id =: idCase];
        if (lstCase.isEmpty()) {
            throw new CustomException('sin resultados','','',1);
        }
        return lstCase;
     }

    /*
    * @description Method que valida que no este vacío el elemnto.
    * @param String obj
	* @return String strResponse
    */
    public static String validaDatoVacio(String obj) {
    	return String.isEmpty(obj) ? '' : obj;
    }
}