/**-------------------------------------------------------------------------
* Nombre: WB_ProcNotif_cls
* @author Karen Sanchez (KB)
* Proyecto: MW WB Tlmkt - BBVA Bancomer
* Descripción : Clase que se ocupa para enviar datos a CTI y regresar una consulta al componente MX_WB_CampaniasCTI_cmp

* --------------------------------------------------------------------------
*                         Fecha           Autor                   Desripción
* -------------------------------------------------------------------
* @version 1.0           13/01/2019      Karen Sanchez            Creación
*          1.1           17/01/2019      Arsenio Perez            Modificacion de busqueda Ln 30-38
* --------------------------------------------------------------------------*/
public without sharing class MX_WB_CampaniasCTI_cls {

    static final List<String> AllApiName = new List<String>();
    static final List<String> searchFilter = new List<String>();
    static String searchKey, cadena1;
    static final List<MX_SB_VTS_Generica__c> Generica = new List<MX_SB_VTS_Generica__c>();
    static final Map<String,String> mapGenerica = new Map<String, String>();
    static {
        searchKey = '';
        cadena1 = '';
        for(MX_SB_VTS_Generica__c gentemp: MX_SB_VTS_Generica__c.getall().values()){
            if('CP2'.equals(gentemp.MX_SB_VTS_Type__c)){
                Generica.add(gentemp);
                mapGenerica.put(gentemp.MX_SB_VTS_FiltroCampanaLabel__c,gentemp.MX_SB_VTS_FiltroCampana__c);
            }
        }
    }

    /*
    *Método: Regresa la consulta de los miembros de campañas de la campaña activa
    *13-01-2019
    *Karen Belem Sanchez Ruiz*/
    @AuraEnabled
    public static List < CampaignMember > lstCampaignMember(String searchKeyWord, String cadena) {
        try{
        searchKey = searchKeyWord;
        List<CampaignMember> lstOfCampaign = new List<CampaignMember>();
            final String USerID =UserInfo.getUserId();
            final String tempProvedor= [Select MX_SB_VTS_ProveedorCTI__c from user where id =:USerID].MX_SB_VTS_ProveedorCTI__c;
        String sQuery = 'SELECT LeadId, Lead.MX_WB_EnvioCTI__c, Lead.FirstName, Lead.LastName, Lead.Apellido_Materno__c, Lead.Fecha_Nacimiento__c,';
               sQuery +=' Lead.MX_WB_txt_Prefijo_1__c, Lead.MX_WB_ph_Telefono1__c, Lead.MX_WB_ph_Telefono2__c, Lead.MX_WB_ph_Telefono3__c, Lead.Folio_Cotizacion__c,';
               sQuery +=' Lead.Producto_Interes__c, Lead.OwnerId, Lead.EnviarCTI__c, Lead.MX_WB_TM_Congelado__c, campaign.MX_SB_VTS_Segmentada__c  FROM CampaignMember WHERE campaign.ID =: searchKey';
               sQuery +=' AND lead.MX_WB_Convertido__c = false AND lead.MX_WB_TM_Congelado__c = false AND LeadId != null AND campaign.IsActive = true';
               sQuery +=' AND Campaign.MX_SB_VTS_FamiliaProducto_Proveedor__r.MX_SB_VTS_Identificador_Proveedor__c=: tempProvedor';
        if(String.isNotBlank(cadena)){
            sQuery = sQuery + cadena;
        }
        List < CampaignMember > lstOfRecords = Database.query(sQuery);//NOSONAR 
        for (CampaignMember obj: lstOfRecords) {
            lstOfCampaign.add(obj);
        }
        return lstOfCampaign;
        } catch(QueryException err){
            throw new AuraHandledException(Label.MX_WB_ERROR + '\nMensaje: ' + err.getMessage() + '\nLinea: ' + err.getLineNumber() + err);
        }
    }

    /*
    *Método: Regresa la consulta de las etiquetas de un customsetting que sirve para el filtrado de los miembros de campañas
    *13-01-2019
    *Karen Belem Sanchez Ruiz*/
    @AuraEnabled
    public static  List<String> getListSettings(String Segmentado){
    try{
        final Boolean Segmentado_tem = Boolean.valueOf(Segmentado);
        List<String> AllLabels = new List<String>();
        if(Segmentado_tem){
            for(MX_SB_VTS_Generica__c gemtemp: Generica){
                 AllLabels.add(gemtemp.MX_SB_VTS_FiltroCampanaLabel__c);
            }
        }else{
        String type = Label.MX_WB_camposFiltrados;
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(type);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
         List<SelectOption> fieldNames = new List<SelectOption>();
        for (String fieldName: fieldMap.keySet()) {
            if(fieldMap.get(fieldName).getDescribe().isCustom()){
                 AllLabels.add(fieldMap.get(fieldName).getDescribe().getLabel());
            }
        }
        }
        return AllLabels;
        } catch(QueryException qError){
            throw new AuraHandledException(Label.MX_WB_ERROR + '\nMensaje: ' + qError.getMessage() + '\nLinea: ' + qError.getLineNumber() + qError);
        }
    }

    /*
    *Método: Regresa la cadena que se selecciona para filtrar los miembros que se enviarán a CTI
    *13-01-2019
    *Karen Belem Sanchez Ruiz*/
    @AuraEnabled
    public static String getListFilter(String field, String selectedOperator, String addValue, String Segmentado){
    try{
        final Boolean Segmentadotem = Boolean.valueOf(Segmentado);
        Boolean limitado = false;
        if(Segmentadotem) {
           limitado=segmentadotemtrue(selectedOperator,  addValue,field);
        } else {
          limitado= segmentadotemfalse(selectedOperator,  addValue,field);
        }
        if(limitado == true) {
            cadena1 = cadena1 + ' LIMIT '+ addValue;
        }
        return cadena1;
        } catch(QueryException qError){
            throw new AuraHandledException(Label.MX_WB_ERROR + '\nMensaje: ' + qError.getMessage() + '\nLinea: ' + qError.getLineNumber() + qError);
        }
    }

        /**
        *@Method: Regresa el armado del Query.
        */
    public static String Cadenresp(String cadena1, String selectedOperator, String sKeyApiValue, String addValue){
         String cadena1temp;
         switch on selectedOperator {
                    when 'CONTAINS' {
                        cadena1temp = cadena1 + ' AND Lead.'+sKeyApiValue+' LIKE \'%'+addValue+'%\'';
                    }
                    when 'NOT(CONTAINS)' {
                        cadena1temp = cadena1 + ' AND (NOT Lead.'+sKeyApiValue+' LIKE \'%'+addValue+'%\')';
                    }
                    when else{
                        cadena1temp = cadena1 + ' AND Lead.'+sKeyApiValue+' '+selectedOperator+' \''+addValue+'\'';
                    }
                }
                searchFilter.add(cadena1temp);
                return cadena1temp;
    }
    /**
    *@method: Valida la consulta cuando el segmentado es verdadero.      
    */
    public static Boolean segmentadotemtrue(String selectedOperator, String addValue,string field) {
        String sKeyApiValue;
        Boolean limitado = false;
         for(String fieldName: mapGenerica.keySet()) {
                if(field==fieldName) {
                    sKeyApiValue=mapGenerica.get(field);
                    if('limite__c'.equals(sKeyApiValue)) {
                        limitado = true;
                        break;
                    }
                    cadena1= Cadenresp(cadena1,selectedOperator,sKeyApiValue,addValue);
                }
            }
            return limitado;
    }
    /**
    * @method: valida la base de busqueda cuando el segmentado es false.        
    */
    public static Boolean segmentadotemfalse(String selectedOperator, String addValue, String field) {
        String sKeyApiValue;
        Set<String> setKeyApi = new Set<String>();
        Boolean limitado = false;
        Map<String, String> mapApiLabel= new Map<String, String>();
        String type = String.valueOf(label.MX_WB_CamposFiltrados);
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(type);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
         for(String fieldName: fieldMap.keySet()) {
                if(field == fieldMap.get(fieldName).getDescribe().getLabel() ) {
                    mapApiLabel.put(fieldName, fieldMap.get(fieldName).getDescribe().getLabel());
                    setKeyApi = mapApiLabel.keySet();
                    for(String sKeyApi:setKeyApi) {
                        sKeyApiValue = sKeyApi;
                    }
                    if('limite__c'.equals(sKeyApiValue)) {
                        limitado = true;
                        break;
                    }
                cadena1= Cadenresp(cadena1,selectedOperator,sKeyApiValue,addValue);
                }
            }
            return limitado;
    }
    /*
    *Método: Método que sirve par hacer el envio al método a futuro ftProcesaSol
    *13-01-2019
    *Karen Belem Sanchez Ruiz*/
    @AuraEnabled
    public static Boolean lstLeadEnviarCTI(List<CampaignMember> searchResult) {
        try{
        Lead objLead = new Lead();
        String sObjecto = 'Lead';
        Integer tipo = 0;
        Date fechaEnvio = Date.today();
        for(CampaignMember camp : searchResult ) {

            objLead.Id = camp.LeadId;
            objLead.Folio_Cotizacion__c = String.ISBLANK(camp.Lead.Folio_Cotizacion__c) ? '' : camp.Lead.Folio_Cotizacion__c;
            objLead.Producto_Interes__c = camp.Lead.Producto_Interes__c == null ? '' : camp.Lead.Producto_Interes__c;
            objLead.OwnerId = camp.Lead.OwnerId;
            objLead.FirstName = camp.Lead.FirstName;
            objLead.MX_WB_ph_Telefono1__c = camp.Lead.MX_WB_ph_Telefono1__c;
            objLead.MX_WB_ph_Telefono2__c = camp.Lead.MX_WB_ph_Telefono2__c;
            objLead.MX_WB_ph_Telefono3__c = camp.Lead.MX_WB_ph_Telefono3__c;

            MX_WB_TM_CTI_cls.ftProcesaSol(String.valueOf(objLead.Id),
                                          String.valueOf(objLead.Folio_Cotizacion__c),
                                          String.valueOf(objLead.Producto_Interes__c),
                                          String.valueOf(objLead.OwnerId),
                                          String.valueOf(objLead.FirstName),
                                          String.valueOf(objLead.MX_WB_ph_Telefono1__c), sObjecto, tipo ,
                                          String.valueOf(objLead.MX_WB_ph_Telefono2__c),String.valueOf(objLead.MX_WB_ph_Telefono3__c));
            objLead.MX_WB_EnvioCTI__c = fechaEnvio;
            objLead.MX_WB_TM_Congelado__c = true;
            update objLead;
        }
        return true;
        } catch(QueryException qError){
            throw new AuraHandledException(Label.MX_WB_ERROR + '\nMensaje: ' + qError.getMessage() + '\nLinea: ' + qError.getLineNumber() + qError);
        }
    }
     @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName) {
        String searchKey = searchKeyWord + '%';
        List < sObject > returnList = new List < sObject > ();
        final String USerID =UserInfo.getUserId();
        final String tempProvedor= [Select MX_SB_VTS_ProveedorCTI__c from user where id =:USerID].MX_SB_VTS_ProveedorCTI__c;
        String sQuery = 'SELECT name,campaign.Id, campaign.Name FROM CampaignMember WHERE campaign.Name LIKE: searchKey';
               sQuery +=' AND lead.MX_WB_Convertido__c = false AND lead.MX_WB_TM_Congelado__c = false AND LeadId != null AND campaign.IsActive = true';
               sQuery +=' AND Campaign.MX_SB_VTS_FamiliaProducto_Proveedor__r.MX_SB_VTS_Identificador_Proveedor__c=: tempProvedor order by createdDate DESC limit 5';
        List < CampaignMember > lstOfRecords = Database.query(sQuery);
        for (CampaignMember obj: lstOfRecords) {
            Campaign temcamp = new Campaign(Name=obj.campaign.Name,Id=obj.campaign.Id);
            returnList.add(temcamp);
        }
        return returnList;
    }
}