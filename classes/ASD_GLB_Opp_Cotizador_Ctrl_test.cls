/*
autor: VASS, Mario Rodriguez
fecha: 25/06/2018
descripción: Clase de prueba de la clase ASD_GLB_Opp_Cotizador_Ctrl
*/

@isTest
public class ASD_GLB_Opp_Cotizador_Ctrl_test {
    /** Valor de estatus para pruebas. */
    public static final String COTIZADA = 'Cotizada';
    /**Mensaje para pruebas. */
    public static final String ELCANDICONVER = 'El candidato ha sido convertido de manera correcta.';

    /**
    *
    *
    * @Descripción:  Test crear Url Seguro de auto
    */
    @isTest
    public static void testcrearURLSeguroDeAuto() {
        final User usrUser = MX_ASD_CargarDatosPrueba_cls.crearDatosUsuario();
        System.runAs(usrUser) {
            Lead testLead = new Lead();
            Test.startTest();
            MX_ASD_CargarDatosPrueba_cls.crearConstantesASD();
            testLead = Utilities.creaOportunidadComercial('Auto Seguro Dinámico');

            final Database.LeadConvert leadConvert = new Database.LeadConvert();
            leadConvert.setConvertedStatus(COTIZADA);
            leadConvert.setLeadId(testLead.Id);
            final Database.LeadConvertResult leadConvertResult = Database.convertLead(leadConvert);
            System.assert(leadConvertResult.isSuccess(),ELCANDICONVER);

            ASD_GLB_Opp_Cotizador_Ctrl.crearURL(leadConvertResult.getOpportunityId());
            Test.stopTest();
        }
    }

    /**
    *
    *
    * @Descripción:  Test crear Url Seguro de Moto
    */
    @isTest
    public static void testcrearURLSeguroDeMoto() {
        final User usrUser = MX_ASD_CargarDatosPrueba_cls.crearDatosUsuario();
        System.runAs(usrUser) {
            Lead testLead = new Lead();
            Test.startTest();
            MX_ASD_CargarDatosPrueba_cls.crearConstantesASD();
            testLead = Utilities.creaOportunidadComercial('Seguro de Moto Bancomer');

            final Database.LeadConvert leadConvert = new Database.LeadConvert();
            leadConvert.setConvertedStatus(COTIZADA);
            leadConvert.setLeadId(testLead.Id);
            final Database.LeadConvertResult leadConvertResult = Database.convertLead(leadConvert);
            System.assert(leadConvertResult.isSuccess(),ELCANDICONVER);

            ASD_GLB_Opp_Cotizador_Ctrl.crearURL(leadConvertResult.getOpportunityId());
            final Id oppId = leadConvertResult.getOpportunityId();
            final Id accountId = leadConvertResult.getAccountId();
            system.debug('###accountId'+accountId);
            final List<Opportunity> oppList = [select Id,accountId,LeadSource, CodigoPostal__c,nombreDelContratante__c,Telefono__c,Sexo_conductor__c,Fecha_Nacimiento_Contratante__c, CorreodelCliente__c from Opportunity where Id =: oppId];
            final String strDateMilsec = Utilities.fnGeneradorFechaMilis();
            ASD_GLB_Opp_Cotizador_Ctrl.crearURLSeguroMotoBancomer(oppList, strDateMilsec, usrUser.Username, '');
            Test.stopTest();
        }

    }

    /**
    *
    *
    * @Descripción:  Test crear Url Seguro Fronterizo
    */
    @isTest
    public static void testcrearURLSeguroFronterizo() {
        final User usrUser = MX_ASD_CargarDatosPrueba_cls.crearDatosUsuario();
        System.runAs(usrUser) {
            Lead testLead = new Lead();
            Test.startTest();
            MX_ASD_CargarDatosPrueba_cls.crearConstantesASD();
            testLead = Utilities.creaOportunidadComercial('Seguro Fronterizo');

            final Database.LeadConvert leadConvert = new Database.LeadConvert();
            leadConvert.setConvertedStatus(COTIZADA);
            leadConvert.setLeadId(testLead.Id);
            final Database.LeadConvertResult leadConvertResult = Database.convertLead(leadConvert);
            System.assert(leadConvertResult.isSuccess(),ELCANDICONVER);

            ASD_GLB_Opp_Cotizador_Ctrl.crearURL(leadConvertResult.getOpportunityId());
            final Id oppId = leadConvertResult.getOpportunityId();
            final List<Opportunity> oppList = [select Id,accountId,LeadSource,  CodigoPostal__c,nombreDelContratante__c,Telefono__c,Sexo_conductor__c,Fecha_Nacimiento_Contratante__c, CorreodelCliente__c from Opportunity where Id =: oppId];
            final String strDateMilsec = Utilities.fnGeneradorFechaMilis();
            ASD_GLB_Opp_Cotizador_Ctrl.crearURLSeguroFronterizo(oppList, strDateMilsec, usrUser.Username, '');
            Test.stopTest();
        }
    }

    /**
    *
    *
    * @Descripción:  Test crear Url Auto Seguro Dinamico
    */
    @isTest
    public static void testcrearURLAutoSeguroDinamico() {
        final User usrUser = MX_ASD_CargarDatosPrueba_cls.crearDatosUsuario();
        System.runAs(usrUser) {
            Lead testLead = new Lead();
            Test.startTest();
            MX_ASD_CargarDatosPrueba_cls.crearConstantesASD();
            testLead = Utilities.creaOportunidadComercial('Seguro Fronterizo');

            final Database.LeadConvert leadConvert = new Database.LeadConvert();
            leadConvert.setConvertedStatus(COTIZADA);
            leadConvert.setLeadId(testLead.Id);
            final Database.LeadConvertResult leadConvertResult = Database.convertLead(leadConvert);
            System.assert(leadConvertResult.isSuccess(),ELCANDICONVER);

            ASD_GLB_Opp_Cotizador_Ctrl.crearURL(leadConvertResult.getOpportunityId());
            final Id oppId = leadConvertResult.getOpportunityId();
            final List<Opportunity> oppList = [select Id,accountId, LeadSource, CodigoPostal__c,nombreDelContratante__c,Telefono__c,Sexo_conductor__c,Fecha_Nacimiento_Contratante__c, CorreodelCliente__c  from Opportunity where Id =: oppId];
            final String strDateMilsec = Utilities.fnGeneradorFechaMilis();
            ASD_GLB_Opp_Cotizador_Ctrl.crearURLAutoSeguroDinamico(oppList, strDateMilsec, usrUser.Username, '');
            Test.stopTest();
        }
    }


}