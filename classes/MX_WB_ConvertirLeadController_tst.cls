/**-------------------------------------------------------------------------
* Nombre: MX_WB_ConverLeadController
* Autor Alexis Pérez
* Proyecto: MW WB Tlmkt - BBVA Bancomer
* Descripción : Test class to ConvertirLeadController

* --------------------------------------------------------------------------
* Versión       Fecha           Autor                   Desripción
* --------------------------------------------------------------------------
* 1.0           15/01/2019      Alexis Pérez		   	Creación
* --------------------------------------------------------------------------
* 1.1          20/02/2019      Alexis Pérez		   	Modificación al método testError se atrapa la excepción de System.AuraHandledException
* 1.2          21/02/2019      Oscar Martínez		Modificación al método testIf se agrega el campo Status con valor de Cotizada
* 1.3			05/03/2019	   Alexis Pérez			Se modifica la clase de prueba para ajustarla a conversión estandar de Cuenta personal
* --------------------------------------------------------------------------
*/

@IsTest
public class MX_WB_ConvertirLeadController_tst {

    @TestSetup
    static void makeData(){
        final User uTelemarketing = MX_WB_TestData_cls.crearUsuario('Telemarketing1', System.Label.MX_SB_VTS_ProfileAdmin);
        insert uTelemarketing;

        final MX_WB_FamiliaProducto__c objFamProd = new MX_WB_FamiliaProducto__c();
        objFamProd.Name = 'ASD';
        insert objFamProd;

        final Product2 objProducto = new Product2();
        objProducto.Name = 'Auto Seguro Dinámico';
        objProducto.IsActive = true;
        objProducto.MX_WB_FamiliaProductos__c = objFamProd.Id;
        insert objProducto;

        final Scripts_Stage_Product__c objScript = new Scripts_Stage_Product__c();
        objScript.MX_WB_Etapa__c = 'Contacto Efectivo';
        objScript.MX_WB_FamiliaProductos__c = objFamProd.Id;
        objScript.MX_WB_Script__c = 'MinionTestScript001';
        insert objScript;
        final Date objDTHoy = Date.today();
        final Campaign objCampana = new Campaign();
        objCampana.Name = 'TestMinion001';
        objCampana.IsActive = true;
        objCampana.StartDate = objDThoy;
        objCampana.EndDate = objDTHoy.addDays(30);
        objCampana.Type = 'Outbound';
        objCampana.MX_WB_FamiliaProductos__c = objFamProd.Id;
        insert objCampana;
    }

     /**
     * Ideal test.
     */
    @IsTest
	public static void convertirLeadASDExcep() {
        final Boolean bExistKey = true;
        Map<String,String> mpEstado = null;
        final Lead objLead = creaLead();
        objLead.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_RecordTypeASD).getRecordTypeId();
        objLead.LeadSource = System.Label.MX_SB_VTS_OrigenCallMeBack;
        objLead.MobilePhone = '5561839485';
        insert objLead;
        final Campaign objCampana = [Select Id from Campaign];
        final CampaignMember objCamMem = new CampaignMember();
        objCamMem.CampaignId = objCampana.Id;
        objCamMem.LeadId = objLead.Id;
        insert objCamMem;
        objLead.MX_WB_lst_EstatusPrimerContacto__c = 'Contacto Efectivo';
        objLead.Status = 'Cotizada';
        update objLead;
        Test.startTest();
        try {
            MX_WB_ConvertirLeadController.convertirLead(objLead.Id);    
        } catch (AuraHandledException aEx) {
            aEx.setMessage('error');
            System.assertEquals(aEx.getMessage(),'error', 'Error al convertir');    
        }
        Test.stopTest();
        
    }

    /**
     * Ideal test.
     */
    @IsTest
	public static void convertirLeadOutBound() {
        final Boolean bExistKey = true;
        Map<String,String> mpEstado = null;
        final Lead objLead = creaLead();
        objLead.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_RecordTypeOut).getRecordTypeId();
        objLead.LeadSource = System.Label.MX_WB_leadSource;
        objLead.MobilePhone = '5561839485';
        insert objLead;
        final Campaign objCampana = [Select Id from Campaign];
        final CampaignMember objCamMem = new CampaignMember();
        objCamMem.CampaignId = objCampana.Id;
        objCamMem.LeadId = objLead.Id;
        insert objCamMem;
        objLead.MX_WB_lst_EstatusPrimerContacto__c = 'Contacto Efectivo';
        objLead.Status = 'Cotizada';
        update objLead;
        Test.startTest();
        mpEstado = MX_WB_ConvertirLeadController.convertirLead(objLead.Id);
        Test.stopTest();
        System.assertEquals(bExistKey, mpEstado.containsKey('OK'), 'Id de la oportunidad esperado.');
    }

    /**
     * Else test.
     */
    @IsTest
	public static void testElse() {
        final Boolean bExistKey = true;
        Map<String,String> mpEstado = null;
        Lead objLead = creaLead();
        objLead.MX_WB_lst_EstatusPrimerContacto__c = 'Contacto Efectivo';
        objLead.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_RecordTypeASD).getRecordTypeId();
        objLead.LeadSource = System.Label.MX_SB_VTS_OrigenCallMeBack;
        objLead.MobilePhone = '5561839485';
        insert objLead;
        final Campaign objCampana = [Select Id from Campaign];
        final CampaignMember objCamMem = new CampaignMember();
        objCamMem.CampaignId = objCampana.Id;
        objCamMem.LeadId = objLead.Id;
        insert objCamMem;
        Test.startTest();
        mpEstado = MX_WB_ConvertirLeadController.convertirLead(objLead.Id);
        Test.stopTest();
        System.assertEquals(bExistKey, mpEstado.containsKey('ERROR'), 'Error esperado.');
    }

    /**
     * Alternative Case test.
     */
    @IsTest
	public static void testAlternativeCase() {
        String strError = null;
        try {
            Test.startTest();
            MX_WB_ConvertirLeadController.convertirLead(null);
            Test.stopTest();
        } catch(System.AuraHandledException ex) {
            strError = 'Script-thrown exception';
            System.assertEquals(strError, ex.getMessage(), 'Error atrapado en la excepción');
        }
    }

    
    /**
     * Create a Lead object.
     */
    public static Lead creaLead() {
        final Lead objLead = new Lead();
        objLead.Email = 'minion001@gmail.com';
        objLead.LastName = '001';
        objLead.FirstName = 'Minion';
        objLead.Apellido_Materno__c = 'Sanchez';
        objLead.Phone = '5598745630';
        objLead.MX_WB_ph_Telefono1__c = '5598745631';
        objLead.MX_WB_ph_Telefono2__c = '5598745632';
        objLead.MX_WB_ph_Telefono3__c = '5598745633';
        objLead.MX_WB_TipoTelefono1__c = 'F';
        objLead.MX_WB_TipoTelefono2__c = 'F';
        objLead.MX_WB_TipoTelefono3__c = 'F';
        objLead.MX_WB_txt_Extension1__c = '5591';
        objLead.MX_WB_txt_Extension2__c = '5592';
        objLead.MX_WB_txt_Extension3_del__c = '5593';
        objLead.MX_WB_int_TerminacionTarjeta__c = '8796';
        objLead.MX_WB_txt_NumClienteEnmascarado__c = '12345678';
        objLead.MX_WB_Cliente_Unico_BBVA__c = 'qwedrftgyhujiklopzxcvbnm123456789741021645987';
        objLead.MX_WB_Cliente_Unico_Seguros__c = 'qwedrftgyhujiklopzxcvbnm123456789741021645987';
        objLead.MX_WB_No_envios_CTI__c = 0;
        objLead.MX_WB_txt_BCOM__c = 'BCO';
        objLead.MX_WB_txt_BMOV__c = 'CMO';
        objLead.MX_WB_txt_Clave_Texto__c = 'poiquefgd';
        objLead.MX_WB_txt_Prefijo_1__c = 'qwe';
        objLead.MX_WB_txt_Prefijo_2__c = 'qwe';
        objLead.MX_WB_txt_Prefijo_3__c = 'wer';
        objLead.Producto_Interes__c = 'Auto Seguro Dinámico';
        return objLead;
    }

    public static Account createAccount() {
        final Long noCliente = (Math.random()*10000000).round();
        final Account objAcc = new Account();
        objAcc.FirstName = 'Minion';
        objAcc.LastName = '001';
        objAcc.ApellidoMaterno__c = 'BancomerTest';
        objAcc.AccountNumber = String.valueOf(noCliente);
        objAcc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('MX_WB_rt_PAcc_Telemarketing').getRecordTypeId();
        objAcc.Correo_Electronico__c = 'minion001@gmail.com';
        insert objAcc;
        return objAcc;
    }
}