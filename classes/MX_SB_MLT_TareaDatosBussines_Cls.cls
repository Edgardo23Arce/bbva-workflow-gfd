/*
----------------------------------------------------------
* Nombre: MX_SB_MLT_TareaDatosBussines_Cls
* Autor Oscar Martínez / Saúl Gonzáles
* Proyecto: Siniestros - BBVA Bancomer
* Descripción : Clase que tiene la lógica de negocio para parsear los objeots json de claime center
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   			Desripción<p />
* --------------------------------------------------------------------------------
* 1.0           15/04/2019     Oscar Martínez / Saúl Gonzáles		   	Creación
* 1.1           26/04/2019     Oscar Martínez 						   	Se inserta el caso y se asigna a una cola
* 1.2           07/05/2019     Oscar Martínez 						   	Se refactoriza la clase para obtener los parámetros del Json mediante un mapa
* --------------------------------------------------------------------------------
*/

public without sharing class MX_SB_MLT_TareaDatosBussines_Cls {
    
    /*
* @description parsea el objeto json y almacena los datos en objetos custom Salesforce.
* @param String json
* @return String strResponse
*/
    public Map<String,String> creaObjetoTareaDatos (String strJson) {
        final Map<String, Object> mapJson = (Map<String, Object>) JSON.deserializeUntyped(strJson);
        final Case casoTareaDatos = new Case();
        casoTareaDatos.MX_SB_MLT_Area__c =  MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('area'));
        casoTareaDatos.MX_SB_MLT_BranchKey__c =  MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('branchKey'));
        casoTareaDatos.MX_SB_MLT_BranchName__c =  MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('branchName'));		
        casoTareaDatos.MX_SB_MLT_Company__c =  MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('company'));
        casoTareaDatos.MX_SB_MLT_Email__c =  MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('email'));
        casoTareaDatos.MX_SB_MLT_Event__c =  MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('event'));
        casoTareaDatos.MX_SB_MLT_Glass__c =  MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('glass'));
        casoTareaDatos.MX_SB_MLT_GlassInicidentDate__c =  MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('glassInicidentDate'));
        casoTareaDatos.MX_SB_MLT_GlassInicidentService__c =  MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('glassInicidentService'));
        casoTareaDatos.MX_SB_MLT_GlassInicidentTime__c =  MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('glassInicidentTime'));
        casoTareaDatos.MX_SB_MLT_GlasswareKey__c =  MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('glasswareKey'));
        casoTareaDatos.MX_SB_MLT_GlasswareName__c =  MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('glasswareName'));
        casoTareaDatos.MX_SB_MLT_External_Id__c =  MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('id'));
        casoTareaDatos.MX_SB_MLT_InsuredLastName__c =  MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('insuredLastName'));
        casoTareaDatos.MX_SB_MLT_InsuredLastName2__c =  MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('insuredLastName2'));
        casoTareaDatos.MX_SB_MLT_InsuredName__c =  MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('insuredName'));
        casoTareaDatos.MX_SB_MLT_IsVIP__c =  (Boolean) (mapJson.get('isVIP') == null ? false : mapJson.get('isVIP'));		
        casoTareaDatos.MX_SB_MLT_Phone__c =  MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('phone'));			
        casoTareaDatos.MX_SB_MLT_UrlLocation__c =  String.isEmpty((String)mapJson.get('urlLocation')) ? Label.MX_SB_MLT_LATLONG : (String)mapJson.get('urlLocation');
        casoTareaDatos.RecordTypeId = recuperaTipoRegistro('Siniestro');
        casoTareaDatos.MX_SB_MLT_Subtype__c = MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('subtype'));
        casoTareaDatos.Origin ='Web';
        final String [] latLong =  casoTareaDatos.MX_SB_MLT_UrlLocation__c.split(',');
        if (!latLong.isEmpty() && !Test.isRunningTest()) {
            casoTareaDatos.MX_SB_MLT_Address__c = obtieneDireccionMapa(MX_SB_MLT_GoogleGeolocation_Cls.getReverseGeocode(latLong[0],latLong[1]));
        }
        final Map<String,String> mapResponse =  new  Map<String,String>();
        AssignmentRule assignRule = new AssignmentRule(); 
        assignRule = [select id from AssignmentRule where SobjectType = 'Case' and Name ='Siniestros' and Active = true limit 1]; 			
        final Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId= assignRule.id;
        casoTareaDatos.setOptions(dmlOpts);
        insert casoTareaDatos ;
        final String strNumeroPoliza = (String)mapJson.get('policyNumber');
        final List<Contract> contratos = [Select Id,MX_WB_noPoliza__c From Contract where MX_WB_noPoliza__c =:strNumeroPoliza];
        if (! contratos.isEmpty()) {
            update MX_SB_MLT_TareaDatosBussines_Cls.mapeaContrato(contratos.get(0),mapJson);
            casoTareaDatos.MX_SB_SAC_Contrato__c  = contratos.get(0).Id;
            update casoTareaDatos;
        }
        mapResponse.put('respuesta', Label.MX_SB_MLT_ExitoTDT);
        mapResponse.put('error','');
        mapResponse.put('idSalesforce',casoTareaDatos.Id);
        return mapResponse;
    }
    /*
    * @description mapea datos en el contrato.
    * @param Contract nuevoContrato
    * @param Map<String, Object> mapJson
    * @return Contract contratoTDT
    */
    private static Contract mapeaContrato (Contract nuevoContrato, Map<String, Object> mapJson ) {
        Contract contratoTDT =  new Contract();
        contratoTDT = nuevoContrato;
        contratoTDT.MX_WB_noPoliza__c =  MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('policyNumber'));
        contratoTDT.MX_WB_Tipo_de_auto__c =  MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('vehicleDescription'));
        contratoTDT.MX_SB_MLT_ColorVehiculo__c =   MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('vehicleColor'));
        contratoTDT.MX_SB_MLT_ClauseNumber__c =  (Integer)mapJson.get('clauseNumber');
        contratoTDT.MX_SB_MLT_DescripcionVehiculo__c =   MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('vehicleDescription'));
        contratoTDT.MX_WB_Marca__c =   MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('make'));
        contratoTDT.MX_WB_Modelo__c =   MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('model'));
        contratoTDT.MX_WB_placas__c =   MX_SB_MLT_TareaDatosUtils_Cls.validaDatoVacio((String)mapJson.get('licensePlate'));
        return contratoTDT;
    }
    /*
    * @description Method privado que recupera el tpo de registro adecuado para la tarea de datos
    * @param String strDevName
    * @return String strTipoRegistro
    */
    private static String recuperaTipoRegistro(String strDevName) {
        String strTipoRegistro = '';
        final RecordType recordT =[SELECT Id FROM RecordType WHERE SobjectType = 'Case' And DeveloperName =:strDevName];
        strTipoRegistro = recordT.Id;
        return strTipoRegistro;
    }    
    /*
    * @description Method privado que recupera la dirección del mapa de google.
    * @param HttpResponse res
    * @return String strDireccion
    */
    private static String obtieneDireccionMapa(HttpResponse res) {
        String strDireccion = '';
        final Map<String,Object> mapGr = (Map<String,Object>)JSON.deserializeUntyped(res.getBody());
        final List<Object> results = (List<Object>)mapGr.get('results');
        if(!results.isEmpty()) {
            final Map<String,Object> firstResult = (Map<String,Object>)results[0];
            strDireccion = (string)firstResult.get('formatted_address');
        }
        return strDireccion ;
    }
}