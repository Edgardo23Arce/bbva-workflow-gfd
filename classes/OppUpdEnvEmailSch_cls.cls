global with sharing class OppUpdEnvEmailSch_cls implements Schedulable{

	global String sQuery {get;set;}
	global String sMinuto {get;set;}
	global string minutoCreada {get;set;}
	global string minutoFormalizada {get;set;}
	global string minutoTarificada {get;set;}
    public Boolean isCupon {get;set;}

    global void execute(SchedulableContext ctx){
        minutoCreada = label.CotizacionCreada;
        minutoTarificada = label.CotizacionTarificada;
        minutoFormalizada = label.CotizacionFormalizada;

        OppUpdEnvEmailBch_cls oppACls = new OppUpdEnvEmailBch_cls(sQuery, sMinuto, minutoCreada, minutoFormalizada, minutoTarificada, isCupon);
        Id batchInstanceId = Database.executeBatch(oppACls, 1);
    }
}