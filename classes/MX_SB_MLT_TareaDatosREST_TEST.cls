/*
----------------------------------------------------------
* Nombre: MX_SB_MLT_TareaDatosREST_TEST
* Autor Oscar Martínez / Saúl Gonzáles
* Proyecto: Siniestros - BBVA Bancomer
* Descripción : Clase que prueba los Methods de la clase MX_SB_TareaDatosREST_Wsr

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Desripción<p />
* --------------------------------------------------------------------------------
* 1.0           15/04/2019     Oscar Martínez / Saúl Gonzáles           Creación
* 1.1           15/04/2019     Daniel Goncalves Vivas                   Cambio en condición assert
* 1.2           15/04/2019     Oscar Martínez                           Profile a CustomLabel
* --------------------------------------------------------------------------------
*/

@isTest
public class MX_SB_MLT_TareaDatosREST_TEST {

    @testSetup static void setup() {
        final User testUser = MX_WB_TestData_cls.crearUsuario ( 'TestLastName', Label.MX_SB_VTS_ProfileAdmin  );
        insert testUser;
        final Account accRec = MX_WB_TestData_cls.crearCuenta ( 'LastName', 'MX_WB_rt_PAcc_Telemarketing' );
        accRec.OwnerId = testUser.Id;
        accRec.Correo_Electronico__c = 'prueba@wibe.com';
        insert accRec;
        final Opportunity oppRec = MX_WB_TestData_cls.crearOportunidad ( 'Test', accRec.Id, testUser.Id, 'MX_WB_RT_Telemarketing' );
        oppRec.FolioCotizacion__c = null;
        oppRec.TelefonoCliente__c = '1234567890';
        oppRec.NumerodePoliza__c='000101';
        insert oppRec;
        final Contract contrato = new Contract();
        contrato.MX_WB_noPoliza__c = oppRec.NumerodePoliza__c;
        contrato.MX_WB_Oportunidad__c = oppRec.Id;
        contrato.AccountId = accRec.Id;
        insert contrato;
    }

    static testMethod void guardaTareaDatos() {
        final String strJson = '{ "address": "valor", "area": "valor","branchKey": "valor","branchName": "valor","clauseNumber": 10,"company": "valor",   "email": "valor","event": "valor","glass": "valor","glassInicidentDate": "valor","glassInicidentService": "valor","glassInicidentTime": "valor","glasswareKey": "valor","glasswareName": "valor","id": "valor","insuredLastName": "valor","insuredLastName2": "valor","insuredName": "valor","isVIP ": 1,"licensePlate": "valor","make": "valor","model": "valor","phone": "valor","policyNumber": "000101","urlLocation": "99.3124214,-19.123124124","vehicleColor": "valor","vehicleDescription": "valor"}'; 
        final RestRequest req = new RestRequest();
        final RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/MX_SB_TareaDatosREST';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(strJson);
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        final MX_SB_MLT_TareaDatosREST_Wsr.Response  resultado = MX_SB_MLT_TareaDatosREST_Wsr.guardaTareaDatos();
        Test.stopTest();
        System.assert(resultado.toString().contains(Label.MX_SB_MLT_ExitoTDT),'Exito');        
    }

    static testMethod void guardaTareaDatosError() {
        final String strJson = '{ "addres+"valor",    "area": "valor","branchKey": "valor","branchName": "valor","clauseNumber": "tastte","company": "valor", "email": "valor","event": "valor","glass": "valor","glassInicidentDate": "valor","glassInicidentService": "valor","glassInicidentTime": "valor","glasswareKey": "valor","glasswareName": "valor","id": "valor","insuredLastName": "valor","insuredLastName2": "valor","insuredName": "valor","isVIP ": "test","licensePlate": "valor","make": "valor","model": "valor","phone": "valor","policyNumber": "valor","urlLocation": "99.3124214,-19.123124124","vehicleColor": "valor","vehicleDescription": "valor"}'; 
        final RestRequest req = new RestRequest();
        final RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/MX_SB_TareaDatosREST';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(strJson);
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        final MX_SB_MLT_TareaDatosREST_Wsr.Response  resultado = MX_SB_MLT_TareaDatosREST_Wsr.guardaTareaDatos();
        Test.stopTest();
        System.assert(resultado.toString().contains(Label.MX_SB_MLT_ErrorTDT),'Error en el servicio');
    }

    static testMethod void guardaTareaDatosErrorDML() {
        final String strJson = '{ "address": "valor", "area": "valor1234567890987654321","branchKey": "valor","branchName": "valor","clauseNumber": 10,"company": "valor",    "email": "valor","event": "valor","glass": "valor","glassInicidentDate": "valor","glassInicidentService": "valor","glassInicidentTime": "valor","glasswareKey": "valor","glasswareName": "valor","id": "valor","insuredLastName": "valor","insuredLastName2": "valor","insuredName": "valor","isVIP ": 1,"licensePlate": "valor","make": "valor","model": "valor","phone": "valor","policyNumber": "000101","urlLocation": "99.3124214,-19.123124124","vehicleColor": "valor","vehicleDescription": "valor"}'; 
        final RestRequest req = new RestRequest();
        final RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/MX_SB_TareaDatosREST';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(strJson);
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        final MX_SB_MLT_TareaDatosREST_Wsr.Response resultado = MX_SB_MLT_TareaDatosREST_Wsr.guardaTareaDatos();
        Test.stopTest(); 
        System.assert(resultado.toString().contains(Label.MX_SB_MLT_ErrorTDT),'Error en el servicio');      
    }

    static testMethod void enviaDatoCC() {
        final String strJson = '{ "calim": "valor",   "area": "valor","branchKey": "valor","branchName": "valor","clauseNumber": 10,"company": "valor",   "email": "valor","event": "valor","glass": "valor","glassInicidentDate": "valor","glassInicidentService": "valor","glassInicidentTime": "valor","glasswareKey": "valor","glasswareName": "valor","id": "valor","insuredLastName": "valor","insuredLastName2": "valor","insuredName": "valor","isVIP ": 1,"licensePlate": "valor","make": "valor","model": "valor","phone": "valor","policyNumber": "valor","urlLocation": "99.3124214,-19.123124124","vehicleColor": "valor","vehicleDescription": "valor"}'; 
        final String respuesta = MX_SB_MLT_TareaDatosREST_Wsr.enviaDatoCC(strJson);
        System.assert(String.isEmpty(respuesta),'Método vacío');
    }

    static testMethod void obtienDatosClipert() {
        final String strJson = '{ "calim": "valor",   "area": "valor","branchKey": "valor","branchName": "valor","clauseNumber": 10,"company": "valor",   "email": "valor","event": "valor","glass": "valor","glassInicidentDate": "valor","glassInicidentService": "valor","glassInicidentTime": "valor","glasswareKey": "valor","glasswareName": "valor","id": "valor","insuredLastName": "valor","insuredLastName2": "valor","insuredName": "valor","isVIP ": 1,"licensePlate": "valor","make": "valor","model": "valor","phone": "valor","policyNumber": "valor","urlLocation": "99.3124214,-19.123124124","vehicleColor": "valor","vehicleDescription": "valor"}'; 
        final String respuesta = MX_SB_MLT_TareaDatosREST_Wsr.obtienDatosClipert(strJson);
        System.assert(String.isEmpty(respuesta),'Método vacío');
     }
}