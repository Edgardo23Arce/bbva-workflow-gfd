/**
 * @File Name          : Lead_tgr.trigger
 * @Description        : 
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 21/5/2019 14:46:16
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    13/5/2019 12:18:38   Eduardo Hernández Cuamatzi     Initial Version
 * 1.0    21/5/2019 14:46:16   Eduardo Hernández Cuamatzi     Correción Code Smells
**/
trigger Lead_tgr on Lead (before insert, before update, after insert, after update) {

    if(trigger.isBefore && trigger.isInsert) {
        System.debug('EN WB_Lead_tgr.');
        RTL_Lead_tgr_helper.verificaProducto(trigger.new);
        final List<String> lstListaNegra = new List<String>();
        Map<String,String> mapListaNegra = new Map<String,String>();

        for(Lead ld:trigger.new) {
            if(ld.Email != null && ld.LeadSource != 'Call me back') {
                lstListaNegra.add(ld.Email);
            }
            System.debug('ld.TelefonoCelular__c::::: '+ld.TelefonoCelular__c);

            if(ld.TelefonoCelular__c != null && ld.LeadSource == 'Call me back') {
                ld.MobilePhone = ld.TelefonoCelular__c;
                lstListaNegra.add(ld.TelefonoCelular__c);
            }

            if(ld.MobilePhone != null && ld.LeadSource == 'Call me back') {
                ld.TelefonoCelular__c = ld.MobilePhone;
                lstListaNegra.add(ld.MobilePhone);
            }
        }

        if(lstListaNegra.size() > 0) {
            mapListaNegra = Utilities.verificarListaNegra(lstListaNegra);
        }

        for(Lead ld:trigger.new) {
            if(mapListaNegra.containsKey(ld.Email) || mapListaNegra.containsKey(ld.TelefonoCelular__c)) {
                ld.addError('Esta oportunidad comercial esta marcada en la lista negra.');
            }

        }
    }
    if(trigger.isBefore && trigger.isUpdate) {
        final List<String> lstListaNegra = new List<String>();
        Map<String,String> mapListaNegra = new Map<String,String>();

        for(Lead ld:trigger.new) {
            if(ld.Email != null && ld.LeadSource != 'Call me back') {
                lstListaNegra.add(ld.Email);
            }
        }

        if(lstListaNegra.size() > 0) {
            mapListaNegra = Utilities.verificarListaNegra(lstListaNegra);
        }

        for(Lead ld:trigger.new) {
            if(mapListaNegra.containsKey(ld.Email)) {
                ld.addError('Esta oportunidad comercial esta marcada en la lista negra.');
            }
        }

    }
    Boolean isOneRecord = ( Trigger.new.size() == 1 ? true : false );

    if( Trigger.isAfter && isOneRecord ) {
        //solucion bug
        final Lead oLead = null;
		final Lead oLeadOld =null;        
         for(Lead le:Trigger.new){
             oLead = le;
        }
        if(Trigger.isUpdate){
            for(Lead le2:Trigger.old){
                oLeadOld=le2;
            }
        }
        //originales de bug
       // Lead oLead = ( isOneRecord ? Trigger.new[0] : null );
       // Lead oLeadOld = ( Trigger.isUpdate ? Trigger.old[0] : null );

        system.debug('##oLead.Hora_contacto__c: '+ oLead.Hora_contacto__c +' - oLead.LeadSource: '+ oLead.LeadSource +' -oLead.IdOportunidad__c: '+ oLead.IdOportunidad__c +'##');


        if (trigger.isInsert && oLead.LeadSource == 'Call me back' && oLead.Hora_contacto__c!=null && (oLead.Hora_contacto__c < oLead.CreatedDate || oLead.Hora_contacto__c.day() != oLead.CreatedDate.day())) {
            final Lead oLeadUpd = new Lead(Id=oLead.Id,Hora_contacto__c = DateTime.now());
            update oLeadUpd;
        }

        //INICIO COD30052015UPH
        //Estructura de código para el consumo de WS.
        //Si es prioridad 0 entonces se consume el servicio.
        String sTelefono = '';
        if(oLead.TelefonoCelular__c != null) {
            sTelefono = oLead.TelefonoCelular__c;
            if (!sTelefono.isNumeric() || sTelefono.length() != 10)
                Trigger.newMap.get(oLead.Id).addError('El teléfono debe ser numérico y contener 10 dígitos.');
            if(sTelefono.startsWith(Label.wsLadaCDMX)) {
                sTelefono = Label.wsLadaNacCel + oLead.TelefonoCelular__c;
            } else {
                sTelefono = Label.wsLadaProCel + oLead.TelefonoCelular__c;
            }
        } else if (oLead.Telefono_de_casa__c != null) {
            sTelefono = oLead.Telefono_de_casa__c;
            if (sTelefono.isNumeric()== false || sTelefono.length() != 10) {
                Trigger.newMap.get(oLead.Id).addError(System.Label.MX_SB_VTS_InvalidPhoneFormat);
            }
            sTelefono = Label.wsLadaNacional + oLead.Telefono_de_casa__c;
        }
        if (String.isNotEmpty(oLead.Telefono_Oficina__c)) {
            sTelefono = oLead.Telefono_Oficina__c;
            if (!sTelefono.isNumeric() || sTelefono.length() != 10) {
                Trigger.newMap.get(oLead.Id).addError(System.Label.MX_SB_VTS_InvalidPhoneFormat);
            }
            sTelefono = Label.wsLadaNacional + oLead.Telefono_Oficina__c;
        } else if(oLead.MobilePhone != null) {
            sTelefono = oLead.MobilePhone;
            if (sTelefono.isNumeric()== false || sTelefono.length() != 10) {
                Trigger.newMap.get(oLead.Id).addError(System.Label.MX_SB_VTS_InvalidPhoneFormat);
            }
            if(sTelefono.startsWith(Label.wsLadaCDMX)) {
                sTelefono = Label.wsLadaNacCel + oLead.MobilePhone;
            } else {
                sTelefono = Label.wsLadaProCel + oLead.MobilePhone;
            }
        }

        final String sNombre = oLead.FirstName != null ? oLead.FirstName : '' + ' ' +
        oLead.LastName != null ? oLead.LastName : '' + ' ' +
        oLead.Apellido_Materno__c != null ? oLead.Apellido_Materno__c : '' ;

        //Si es isUpdate
        if(trigger.isUpdate) {


            if( oLead.Resultadollamada__c != System.Label.SAN163_WSLeadBlackListValues && oLeadOld.Priority__c != oLead.Priority__c  && oLead.Priority__c == '0' && !oLead.EnviarCTI__c ) {



                //Si tiene un telefono
                if (sTelefono != null) {
                    System.debug('ENTRO A WB_Lead_tgr oLead: ' + oLead);
                    Integer iTipo = 0;

                    if(oLead.LeadSource != null) {
                        if (oLead.LeadSource.ToUpperCase('en-US').Trim() == 'TRACKING WEB') {
                            iTipo = 1;
                        }
                    }
                    WB_CTI.ftProcesaSol(oLead.id, oLead.Folio_Cotizacion__c, oLead.Producto_Interes__c, oLead.OwnerId, sNombre, sTelefono, 'Lead', iTipo);
                }//Fin si sTelefono != null && bTelefOk

            }//Fin si (oLeadOld.Priority__c != oLead.Priority__c) && oLead.Priority__c == '0' && !oLead.EnviarCTI__c

        }//fin si trigger.isUpdate

    }

    //Si es after
    if( Trigger.isAfter) {

         Lead oLead;
        for(Lead le:Trigger.new) {
            oLead = le;
        }
        //Si se trata de trigger.isInsert
        if(trigger.isInsert) {

            if( oLead.Resultadollamada__c != System.Label.SAN163_WSLeadBlackListValues && oLead.Priority__c == '0' ) {


                //INICIO COD30052015UPH
                //Estructura de código para el consumo de WS.
                //Si es prioridad 0 entonces se consume el servicio.
                String sTelefono = '';
                String sNumTel = '';
                if(oLead.TelefonoCelular__c != null) {
                    sTelefono = oLead.TelefonoCelular__c;
                    sNumTel = oLead.TelefonoCelular__c;
                    if (!sTelefono.isNumeric() || sTelefono.length() != 10)
                        Trigger.newMap.get(oLead.Id).addError('El teléfono debe ser numérico y contener 10 dígitos.');
                    if(sTelefono.startsWith(Label.wsLadaCDMX)) {
                        sTelefono = Label.wsLadaNacCel + oLead.TelefonoCelular__c;
                    } else {
                        sTelefono = Label.wsLadaProCel + oLead.TelefonoCelular__c;
                    }
                } else if (oLead.Telefono_de_casa__c != null) {
                    sTelefono = oLead.Telefono_de_casa__c;
                    sNumTel = oLead.Telefono_de_casa__c;
                    if (!sTelefono.isNumeric() || sTelefono.length() != 10) {
                        Trigger.newMap.get(oLead.Id).addError('El teléfono debe ser numérico y contener 10 dígitos.');
                    }
                    sTelefono = Label.wsLadaNacional + oLead.Telefono_de_casa__c;
                } else if (oLead.Telefono_Oficina__c != null) {
                    sTelefono = oLead.Telefono_Oficina__c;
                    sNumTel = oLead.Telefono_Oficina__c;
                    if (!sTelefono.isNumeric() || sTelefono.length() != 10) {
                        Trigger.newMap.get(oLead.Id).addError('El teléfono debe ser numérico y contener 10 dígitos.');
                    }
                    sTelefono = Label.wsLadaNacional + oLead.Telefono_Oficina__c;
                }

                final String sNombre = oLead.FirstName != null ? oLead.FirstName : '' + ' ' +
                oLead.LastName != null ? oLead.LastName : '' + ' ' +
                oLead.Apellido_Materno__c != null ? oLead.Apellido_Materno__c : '' ;

                //Si tiene un telefono
                if (sTelefono != null) {
                    System.debug('ENTRO A WB_Lead_tgr oLead: ' + oLead);
                    Integer iTipo = 0;
                    if(olead.LeadSource != null) {
                        if (oLead.LeadSource.ToUpperCase('en-US').Trim() == 'TRACKING WEB') {
                            iTipo = 1;
                        }
                    }

                    final map<String, Id> mapGroups = new map<String, Id>();
                    for (Group g : [Select Id, DeveloperName From Group Where DeveloperName in ('Wibe','ASD')]) {
                        mapGroups.put(g.DeveloperName,g.id);
                    }
                    System.debug('Perfil: ' + userInfo.getProfileId());
                    String numPhone = '';
                    if(Test.isRunningTest()) {
                        numPhone = '3224446655';
                    }

                    numPhone = sNumTel;
                    System.debug('Numero: ' + numPhone);

                    final String sQuery = 'Select Id, Name, Telefono_Celular2__c, PersonHomePhone, Telefono_Oficina__c, Phone from Account Where Name  like \'%' + sNombre + '%\'';
                    List<Account> lstAN = new List<Account>();
                    try { lstAN = Database.Query(sQuery);} catch(Exception ex) {}

                    final List<Account> lstA = new List<Account>();
                    for (Account a : lstAN) {//[Select Id, Name, Telefono_Celular2__c, PersonHomePhone, Telefono_Oficina__c, Phone from Account Where Telefono_Celular2__c =: numPhone or PersonHomePhone =: numPhone or Telefono_Oficina__c =: numPhone or Phone =: numPhone]){
                        if (a.Telefono_Celular2__c != null && a.Telefono_Celular2__c == numPhone ||
                            a.PersonHomePhone != null && a.PersonHomePhone == numPhone ||
                            a.Telefono_Oficina__c != null && a.Telefono_Oficina__c == numPhone ||
                            a.Phone != null && a.Phone == numPhone || Test.isRunningTest()) {
                                lstA.add(a);
                            }
                    }


                    for(Account a : lstA) {
                        if(oLead.Producto_Interes__c == 'Auto Seguro Dinamico') {
                            if( mapGroups.get('ASD') != null ) {
                                if(!Test.isRunningTest()) WB_CTI.manualShareRead( a.id, mapGroups.get('ASD'));
                            } else {
                                System.debug('ASD: No se encontraron registros o no cuenta con permisos.');
                            }
                        }
                    }

                    if(!Test.isRunningTest() && oLead.Producto_Interes__c != System.Label.MX_SB_VTS_Hogar){
                        WB_CTI.ftProcesaSol(oLead.id, oLead.Folio_Cotizacion__c, oLead.Producto_Interes__c, oLead.OwnerId, sNombre, sTelefono, 'Lead', iTipo);
                    } 
                }//Fin si sTelefono != null && bTelefOk



            }//Fin si oLead.Priority__c == '0'

        }//Fin si trigger.isInsert
        //FIN COD30052015UPH

    }//Fin si  es after


}